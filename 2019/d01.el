;;; d01 --- Advent of code d01

;;; Commentary:

;;; Code:

(defun read-lines (filePath)
  "Return a list of lines of a file at FILEPATH."
  (with-temp-buffer
    (insert-file-contents filePath)
    (split-string (buffer-string) "\n" t)))

(defun fuel-required (mass)
  "Compute fuel required for a module of MASS."
  (- (floor mass 3) 2))

(defun fuel-required-rec (mass &optional accu)
  "Compute fuel required by fuel for MASS, accumulate in ACCU."
  (let ((current (fuel-required mass))
	(current-accu (or accu 0)))
    (if (<= 0 current)
	(fuel-required-rec current (+ current-accu current))
      current-accu)))

(defun aoc/d01-part1 (file-path)
  "Advent of Code 2019 - d01 part1, read from file at FILE-PATH."
  (apply #'+ (mapcar (lambda (x) (fuel-required (string-to-number x)))
		     (read-lines file-path))))


(defun aoc/d01-part2 (file-path)
  "Advent of Code 2019 - d01 part2, read from file at FILE-PATH."
  (apply #'+ (mapcar (lambda (x) (fuel-required-rec (string-to-number x)))
		     (read-lines file-path))))

(aoc/d01-part1 "input-01.txt")
(aoc/d01-part2 "input-01.txt")

;;; d01-part1.el ends here
