;;; d02 -- Advent of code - d02

;;; Code:

(defun load-memory (file-path)
  "Load FILE-PATH to a vector of number."
  (with-temp-buffer
    (insert-file-contents file-path)
    (vconcat (mapcar #'string-to-number (split-string (buffer-string) "," t)))))

(defun make-instruction (b1 b2 b3 b4)
  "Build an instruction from bytes B1 B2 B3 and B4."
  (cond ((eq b1 1)
	 (list :opcode "add"
	       :func #'+
	       :left b2
	       :right b3
	       :dest b4
	       :size 4))
	((eq b1 2)
	 (list :opcode "mul"
	       :func #'*
	       :left b2
	       :right b3
	       :dest b4
	       :size 4))
	((eq b1 99)
	 (list :opcode "quit"
	       :size 1))))

(defun aref-or-nil (array idx)
  "Get the value from ARRAY at IDX or nil if out of bound."
  (when (< idx (length array))
    (aref array idx)))

(defun build-instruction (memory instr-pointer)
  ""
  (make-instruction (aref memory instr-pointer)
		    (aref memory (+ instr-pointer 1))
		    (aref memory (+ instr-pointer 2))
		    (aref memory (+ instr-pointer 3))))

(defun aoc/d02-run (memory &optional noun verb)
  "Run ship computer from MEMORY, with NOUN at pos 1 and VERB at pos 2."
  (when noun (aset memory 1 noun))
  (when verb (aset memory 2 verb))
  (aset memory 2 verb)
  (let ((instr-pointer 0)
	(halted nil))

    (while (and (not halted) (< instr-pointer (length memory)))
      ;; (message "PC:%d out of %d" instr-pointer (length memory))
      (let ((instr (build-instruction memory instr-pointer)))
	(if (string= (plist-get instr :opcode) "quit")
	    (setq halted t)
	  (progn
	    (aset memory (plist-get instr :dest)
		  (apply (plist-get instr :func)
			 (list (aref memory (plist-get instr :left))
			       (aref memory (plist-get instr :right)))))))
	(setq instr-pointer (+ instr-pointer (plist-get instr :size))))))
  memory)

(defun aoc/d02-01 (file-path)
  "Run ship computer loaded from FILE-PATH."
  (let ((memory (load-memory file-path)))
    (aref (aoc/d02-run memory 12 2) 0)))

(defun aoc/d02-02 (file-path target)
  "Run ship computer loaded from FILE-PATH.and brute-force noun and verb to reach TARGET."
  (dotimes (noun 100)
    (dotimes (verb 100)
      (let ((output (aref (aoc/d02-run (load-memory file-path) noun verb) 0)))
	(when (= output target)
	  (message "%02d %02d => %d" noun verb output))))))

;; (aoc/d02-01 "input-02.txt")
(aoc/d02-02 "input-02.txt" 19690720)

(defun aoc/d02-01-ic (file-path)
  ""
  (let ((memory (ic/load-memory file-path)))
    (aset memory 1 12)
    (aset memory 2 2)
    (ic/run-program memory)
    )
  )

(aoc/d02-01-ic "input-02.txt")


;;; d02.el ends here
