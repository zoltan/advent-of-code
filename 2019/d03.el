;;; d03 -- Advent of code - d03

;;; Commentary:

;;; Code:

;; Required for recursions
(setq max-specpdl-size (* max-specpdl-size 15))
(setq max-lisp-eval-depth (* max-lisp-eval-depth 30))

(defun aoc/read-input (file-path)
  "Read input data from FILE-PATH."
  (with-temp-buffer
    (insert-file-contents file-path)
    (mapcar (lambda (x) (split-string x ","))
	    (split-string (buffer-string) "\n"))))

(defun aoc/build-segment (current directions steps)
  ""
  (let ((prefix (aref (car directions) 0))
	(offset (string-to-number (substring (car directions) 1)))
	(next-point current)
	(cell))
    (cond
     ((= prefix ?U)
      (setq next-point (cons (car current)
			     (+ (cdr current) offset))
	    cell (list :orientation "vertical"
		       :direction "up"
		       :steps steps
		       :range (cons (cdr current) (+ (cdr current) offset))
		       :fixed (car current))))
     ((= prefix ?D)
      (setq next-point (cons (car current)
			     (- (cdr current) offset))
	    cell (list :orientation "vertical"
		       :direction "down"
		       :steps steps
		       :range (cons (- (cdr current) offset) (cdr current))
		       :fixed (car current))))
     ((= prefix ?L)
      (setq next-point (cons (- (car current) offset)
			     (cdr current))
	    cell (list :orientation "horizontal"
		       :direction "left"
		       :steps steps
		       :range (cons (- (car current) offset) (car current))
		       :fixed (cdr current))))
     ((= prefix ?R)
      (setq next-point (cons (+ (car current) offset)
			     (cdr current))
	    cell (list :orientation "horizontal"
		       :direction "right"
		       :steps steps
		       :range (cons (car current) (+ (car current) offset))
		       :fixed (cdr current)))))

    (message "%S %s => %S [%S]"
	     current
	     (car directions)
	     next-point
	     cell)
    (if (cdr directions)
	(cons cell (aoc/build-segment next-point (cdr directions) (+ steps offset)))
      (list cell))))


;; (aoc/build-segment (cons 0 0) (list "D5" "R2"))
;; (aoc/build-segment (cons 0 0) (list "R75" "D30" "R83" "U83" "L12" "D49" "R71" "U7" "L72"))

(defun aoc/is-vertical (segment)
  ""
  (string= (plist-get segment :orientation) "vertical"))

(defun aoc/is-horizontal (segment)
  ""
  (string= (plist-get segment :orientation) "horizontal"))

(defun aoc/in-range (value boundaries)
  ""
  (and (<= (car boundaries) value) (<= value (cdr boundaries))))

(defun aoc/segment-intersection (seg1 seg2)
  ""
  (when (and
	 (or (and (aoc/is-vertical seg1) (aoc/is-horizontal seg2))
	     (and (aoc/is-horizontal seg1) (aoc/is-vertical seg2)))
	 (and (aoc/in-range (plist-get seg1 :fixed) (plist-get seg2 :range))
	      (aoc/in-range (plist-get seg2 :fixed) (plist-get seg1 :range))))
    (cons (plist-get seg1 :fixed) (plist-get seg2 :fixed))))

(defun aoc/segment-steps (segment pos)
  ""

  (let ((direction (plist-get segment :direction))
	(steps (plist-get segment :steps))
	(range (plist-get segment :range)))
    (if (or (string= direction "right")
	    (string= direction "up"))
	(+ steps (- pos (car range)))
      (+ steps (abs (- pos (cdr range)))))))

;; (aoc/segment-steps (list :direction "right" :steps 20 :range (cons 5 25)) 15)

(defun aoc/segment-intersection-steps (seg1 seg2)
  ""

  (let ((steps (+ (aoc/segment-steps seg1 (plist-get seg2 :fixed))
		  (aoc/segment-steps seg2 (plist-get seg1 :fixed)))))
    (when (and
	   (or (and (aoc/is-vertical seg1) (aoc/is-horizontal seg2))
	       (and (aoc/is-horizontal seg1) (aoc/is-vertical seg2)))
	   (and (aoc/in-range (plist-get seg1 :fixed) (plist-get seg2 :range))
		(aoc/in-range (plist-get seg2 :fixed) (plist-get seg1 :range)))
	   (/= steps 0))
      (message "%S - %S => %d" seg1 seg2 (+ (aoc/segment-steps seg1 (plist-get seg2 :fixed))
					    (aoc/segment-steps seg2 (plist-get seg1 :fixed))))
      steps)))

(defun aoc/find-intersections (blue-wire red-wire intersect-f)
  ""
  (let ((intersections))
    (dolist (blue-segment blue-wire intersections)
      (dolist (red-segment red-wire)
	(let ((intersec (apply intersect-f (list blue-segment red-segment))))
	  (when intersec
	    (setq intersections (cons intersec intersections))))))))

(defun aoc/make-distances (points)
  ""
(let ((distances))
    (dolist (pt points distances)
      (setq distances (cons (+ (abs (car pt)) (abs (cdr pt))) distances)))))

(defun aoc/d03-01 (file-path)
  "Day 3 part 1, read input from FILE-PATH."
  (let ((raw-src (aoc/read-input file-path)))
    (message "%S" raw-src)
    (let ((blue-wire (aoc/build-segment (cons 0 0) (car raw-src) 0))
	  (red-wire (aoc/build-segment (cons 0 0) (nth 1 raw-src) 0)))
      (apply #'min
	     (aoc/make-distances (aoc/find-intersections blue-wire red-wire #'aoc/segment-intersection))))))

(defun aoc/d03-02 (file-path)
  "Day 3 part 2, read input from FILE-PATH."
  (let ((raw-src (aoc/read-input file-path)))
    (message "%S" raw-src)
    (let ((blue-wire (aoc/build-segment (cons 0 0) (car raw-src) 0))
	  (red-wire (aoc/build-segment (cons 0 0) (nth 1 raw-src) 0)))
      (apply #'min
	     (aoc/find-intersections blue-wire red-wire #'aoc/segment-intersection-steps)))))

;; (aoc/d03-01 "./input-03.txt")
(aoc/d03-02 "./input-03.txt")
;; (aoc/d03-02 "./input-03-example1.txt")

;;; d03.el ends here
