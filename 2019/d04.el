;;; d04 -- Advent of code - d04

;;; Commentary:

;;; Code:

(defun aoc/dont-decrease-p (password)
  ""
  (if (cdr password)
      (and
       (<= (car password) (car (cdr password)))
       (aoc/dont-decrease-p (cdr password)))
    t))

(defun aoc/valid-password-p (password)
  ""
  (and (string-match-p "\\([0-9]\\)\\1" password)
       (aoc/dont-decrease-p (append password nil))))

(defun aoc/valid-password-strict-p (password)
  ""
  (and (string-match-p "\\([0-9]\\)\\1" (replace-regexp-in-string "\\([0-9]\\)\\1\\1+" "A" password))
       (aoc/dont-decrease-p (append password nil))))

(defun aoc/d04-01 (low high)
  ""
  (apply #'+ (mapcar (lambda (x) (if (aoc/valid-password-p (number-to-string x)) 1 0)) (number-sequence  low high))))

(defun aoc/d04-02 (low high)
  ""
  (apply #'+ (mapcar (lambda (x) (if (aoc/valid-password-strict-p (number-to-string x)) 1 0)) (number-sequence  low high))))

;; Input is 168630-718098 but these values are not valid,
;; range can be reduced to: 168888 - 699999
(aoc/d04-01 168888 699999)
(aoc/d04-02 168888 699999)



;;; d04.el ends here
