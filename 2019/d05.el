;;; d05 -- Advent of code - d05

;;; Commentary:

;;; Code:

(load-file "intcode.el")

;; (let ((memory (vector 1002 2 4 0 99)))
;;   (ic/run-program memory)
;;   memory)
(let ((memory (vector 1 0 0 0 99)))
  (ic/run-program memory))

(ic/run-program (vector 1 0 0 0 99))	; 2 0 0 0 99
(ic/run-program (vector 2 3 0 3 99))	; 2 3 0 6 99
(ic/run-program (vector 2 4 4 5 99 0))	; 2 4 4 5 99 9801

(ic/run-program (vector 1101 100 -1 4 0))


;; Using position mode, consider whether the input is equal to 8; output
;; 1 (if it is) or 0 (if it is not).
(ic/run-program (vector 3 9 8 9 10 9 4 9 99 -1 8))
;; Using position mode, consider whether the input is less than 8; output
;; 1 (if it is) or 0 (if it is not).
(ic/run-program (vector 3 9 7 9 10 9 4 9 99 -1 8))
;; Using immediate mode, consider whether the input is equal to 8; output
;; 1 (if it is) or 0 (if it is not).
(ic/run-program (vector 3 3 1108 -1 8 3 4 3 99))
;; Using immediate mode, consider whether the input is less than 8; output
;; 1 (if it is) or 0 (if it is not).
(ic/run-program (vector 3 3 1107 -1 8 3 4 3 99))

;; Non zero input
(ic/run-program (vector 3 12 6 12 15 1 13 14 13 4 13 99 -1 0 1 9))
(ic/run-program (vector 3 3 1105 -1 9 1101 0 0 12 4 12 99 1))


(ic/run-program (ic/load-memory "./input-05.txt"))



;;; d05.el ends here
