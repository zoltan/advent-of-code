;;; d06 -- Advent of code d06

;;; Commentary:

;;; Code:

(defun aoc/load-map (file-path)
  ""
  (with-temp-buffer
    (insert-file-contents file-path)
    (mapcar (lambda (x) (split-string x ")"))
	    (split-string (buffer-string) "\n" t))))

(defun aoc/make-orbits-hash (pairs-list)
  ""

  (let ((orbits (make-hash-table :test #'equal)))
    (mapc (lambda (pair)
	    (let ((key (car pair))
		  (value (cdr pair)))
	      (puthash key (cons value (gethash key orbits ())) orbits)))
	  pairs-list)
    (message "%S" orbits)))

(message "%S" (aoc/make-orbits-hash '(("aaa" "bbb")
				      ("aaa" "eee")
				      ("bbb" "ccc")
				      ("ccc" "ddd"))))



;;; d06.el ends here
