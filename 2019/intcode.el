;;; intcode --- Intcode compute for https://adventofcode.com/2019/

;;; Commentary:

;; Implements the Intcode computer defined by the puzzles from Advent of Code 2019.

;;; Code:

(defconst ic/opcode-table-alist
  '((1 . ic/add)
    (2 . ic/mul)
    (3 . ic/input)
    (4 . ic/output)
    (5 . ic/jump-if-true)
    (6 . ic/jump-if-false)
    (7 . ic/less-than)
    (8 . ic/equals)
    (99 . ic/halt))
  "Mapping of all opcodes and code.")

(defmacro ic/get-moded-value (pos mode memory)
  "Read the value at POS according to MODE in MEMORY."
  `(let ((direct-value (aref ,memory ,pos)))
    (cond
     ((= ,mode 0) (aref ,memory direct-value))
     ((= ,mode 1) direct-value))))

(defmacro ic/reset-flags (flags)
  "Set init values of FLAGS."
  `(progn
     (plist-put ,flags :halt 0)
     (plist-put ,flags :pc 0)))

(defmacro ic/pc ()
  "Set init values of FLAGS."
  `(plist-get flags :pc))

(defun ic/load-memory (file-path)
  "Load FILE-PATH to a vector of number."
  (with-temp-buffer
    (insert-file-contents file-path)
    (vconcat (mapcar #'string-to-number (split-string (buffer-string) "," t)))))

(defun ic/modes-to-list (modes)
  "Build a list from MODES."
  (let ((current (% modes 10))
	(rest (/ modes 10)))
    (if (> rest 0)
	(cons current (ic/modes-to-list rest))
      (list current))))

(defun ic/make-opcode-mode (value)
  "Convert VALUE to a pair of opcode and mode."
  (list :opcode (% value 100)
	:modes (ic/modes-to-list (/ value 100))))

(defun ic/add (memory op-mode flags)
  "Run operation `mul` on MEMORY at :pc from FLAGS based on OP-MODE."
  (let ((modes (plist-get op-mode :modes)))
    (let ((left (ic/get-moded-value (+ (ic/pc) 1) (or (nth 0 modes) 0) memory))
	  (right (ic/get-moded-value (+ (ic/pc) 2) (or (nth 1 modes) 0) memory))
	  (dest (aref memory (+ 3 (plist-get flags :pc)))))
      (message "ADD >> %d + %d => %d" left right dest)
      (aset memory dest (+ left right))))
  (+ (ic/pc) 4))

(defun ic/mul (memory op-mode flags)
  "Run operation `mul` on MEMORY at :pc from FLAGS based on OP-MODE."
  (let ((modes (plist-get op-mode :modes)))
    (let ((left (ic/get-moded-value (+ (ic/pc) 1) (or (nth 0 modes) 0) memory))
	  (right (ic/get-moded-value (+ (ic/pc) 2) (or (nth 1 modes) 0) memory))
	  (dest (aref memory (+ 3 (plist-get flags :pc)))))
      (message "MUL >> %d * %d => %d" left right dest)
      (aset memory dest (* left right))))
  (+ (ic/pc) 4))

(defun ic/input (memory op-mode flags)
  "Run operation `input`."
  (let ((dest (aref memory (1+ (ic/pc)))))
    (aset memory dest (string-to-number (read-from-minibuffer "Input:"))))
  (+ (ic/pc) 2))

(defun ic/output (memory op-mode flags)
  "Run operation `output`."
  (let ((modes (plist-get op-mode :modes)))
    (message "OUTPUT: %d" (ic/get-moded-value (+ (ic/pc) 1) (or (nth 0 modes) 0) memory)))
  (+ (ic/pc) 2))

(defun ic/jump-if-true (memory op-mode flags)
  "Run operator `jump-if-true`."
  (let ((modes (plist-get op-mode :modes)))
    (if (/= (ic/get-moded-value (+ (ic/pc) 1) (or (nth 0 modes) 0) memory) 0)
	(ic/get-moded-value (+ (ic/pc) 2) (or (nth 1 modes) 0) memory)
      (+ (ic/pc) 3))))

(defun ic/jump-if-false (memory op-mode flags)
  "Run operator `jump-if-false`."
  (let ((modes (plist-get op-mode :modes)))
    (if (= (ic/get-moded-value (+ (ic/pc) 1) (or (nth 0 modes) 0) memory) 0)
	(ic/get-moded-value (+ (ic/pc) 2) (or (nth 1 modes) 0) memory)
      (+ (ic/pc) 3))))

(defun ic/less-than (memory op-mode flags)
  "Run operator `less-than`."
  (let ((modes (plist-get op-mode :modes)))
    (let ((left (ic/get-moded-value (+ (ic/pc) 1) (or (nth 0 modes) 0) memory))
	  (right (ic/get-moded-value (+ (ic/pc) 2) (or (nth 1 modes) 0) memory))
	  (dest (aref memory (+ 3 (ic/pc)))))
      (aset memory dest (if (< left right) 1 0)))
    (+ (ic/pc) 4)))

(defun ic/equals (memory op-mode flags)
  "Run operator `equals`."
  (let ((modes (plist-get op-mode :modes)))
    (let ((left (ic/get-moded-value (+ (ic/pc) 1) (or (nth 0 modes) 0) memory))
	  (right (ic/get-moded-value (+ (ic/pc) 2) (or (nth 1 modes) 0) memory))
	  (dest (aref memory (+ 3 (ic/pc)))))
      (aset memory dest (if (= left right) 1 0)))
    (+ (ic/pc) 4)))

(defun ic/halt (memory op-mode flags)
  "Halt the program."
  (plist-put flags :halt 1)
  (+ (ic/pc) 1))

(defun ic/run-program (memory)
  "Run the program loaded into MEMORY."
  (let ((flags '(:halt 0 :pc 0)))
    (ic/reset-flags flags)
    (while (= (plist-get flags :halt) 0)
      (let ((instruction (ic/make-opcode-mode (aref memory (ic/pc)))))
	(message "%#04x[%S] >> %S" (ic/pc) flags instruction)
	(let ((code-f (cdr (assoc (plist-get instruction :opcode) ic/opcode-table-alist))))
	  (plist-put flags :pc (funcall code-f memory instruction flags))))))
  (message "--------------------------------------------------------------------------------")
  memory)

(provide 'intcode)

;;; intcode.el ends here
