(defsystem "cl-2015"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on ("md5" "cl-ppcre" "str")
  :components ((:module "src"
                :components
                ((:file "main")
                 (:file "d01")
                 (:file "d02")
                 (:file "d03")
                 (:file "d04")
                 (:file "d05")
                 (:file "d06")
                 (:file "d07"))))
  :description ""
  :in-order-to ((test-op (test-op "cl-2015/tests"))))

(defsystem "cl-2015/tests"
  :author ""
  :license ""
  :depends-on ("cl-2015"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for cl-2015"
  :perform (test-op (op c) (symbol-call :rove :run c)))
