(in-package :cl-2015)

(defun as-relative-floor (char)
  (cond ((eql char #\() 1)
        ((eql char #\)) -1)
        (t (error "invalid char"))))

(defun d01-1 (infile)
  (with-open-file (stream infile)
    (loop for c across (read-line stream)
          sum (as-relative-floor c))))

(defun d01-2 (infile)
  (with-open-file (stream infile)
    (loop for pos from 1
          for c across (read-line stream)
          sum (as-relative-floor c) into floor
          while (>= floor 0)
          finally (return pos))))
