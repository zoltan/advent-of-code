(in-package :cl-2015)

(defun d02-read-size (value)
  (let ((left-sep (position #\x value))
        (right-sep (position #\x value :from-end t)))
    (list
     (parse-integer (subseq value 0 left-sep))
     (parse-integer (subseq value (1+ left-sep) right-sep))
     (parse-integer (subseq value (1+ right-sep))))))

(defun d02-wrap-size (l w h)
  (let ((lw (* l w))
        (wh (* w h))
        (hl (* h l)))
    (+ (* 2 lw)
       (* 2 wh)
       (* 2 hl)
       (min lw wh hl))))

(defun d02-ribbon-length (l w h)
  (+ (* l w h)
     (min (+ l l w w)
          (+ w w h h)
          (+ h h l l))))

(defun d02-1 (infile)
  (with-open-file (stream infile)
    (loop for line = (read-line stream nil)
          while line
          for (l w h) of-type (integer integer integer) = (d02-read-size line)
          ;; 2*l*w + 2*w*h + 2*h*l
          sum (d02-wrap-size l w h))))

(defun d02-2 (infile)
  (with-open-file (stream infile)
    (loop for line = (read-line stream nil)
          while line
          for (l w h) of-type (integer integer integer) = (d02-read-size line)
          ;; 2*l*w + 2*w*h + 2*h*l
          sum (d02-ribbon-length l w h))))
