(in-package :cl-2015)


(defun d03-next-coords (dir current)
  (cond ((char= dir #\^) (cons (car current)
                               (1+ (cdr current))))
        ((char= dir #\v) (cons (car current)
                               (1- (cdr current))))
        ((char= dir #\<) (cons (1- (car current))
                               (cdr current)))
        ((char= dir #\>) (cons (1+ (car current))
                               (cdr current)))))


(defun d03-1-in (line)
  (let ((coords (make-hash-table :test #'equalp)))
    (setf (gethash '(0 . 0) coords) 1)
    (loop
      for dir across line
      for current = (d03-next-coords dir (or current '(0 . 0)))
      do
         (unless (gethash current coords)
           (setf (gethash current coords) 1)))
    (hash-table-count coords)))

(defun d03-2-in (line)
  (let ((coords (make-hash-table :test #'equalp)))
    (setf (gethash '(0 . 0) coords) 2)
    (loop
      for turn from 0
      and dir across line
      for pos = (if (evenp turn)
                    (cons (d03-next-coords dir (or (and pos (car pos)) '(0 . 0)))
                          (cdr pos))
                    (cons (car pos)
                          (d03-next-coords dir (or (and pos (cdr pos)) '(0 . 0)))))
      for current = (if (evenp turn)
                        (car pos)
                        (cdr pos))
      do
         (unless (gethash current coords)
           (setf (gethash current coords) 1)))
    (hash-table-count coords)))

(defun d03-1 (infile)
  (with-open-file (stream infile)
    (d03-1-in (read-line stream))))

(defun d03-2 (infile)
  (with-open-file (stream infile)
    (d03-2-in (read-line stream))))
