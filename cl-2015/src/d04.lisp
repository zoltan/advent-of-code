(in-package :cl-2015)

(defun d04-1-valid-md5 (raw)
  (let ((regs (md5:md5sum-string raw)))
    (and (eql (elt regs 0) 0)
         (eql (elt regs 1) 0)
         (< (elt regs 2) 16))))

(defun d04-2-valid-md5 (raw)
  (let ((regs (md5:md5sum-string raw)))
    (and (eql (elt regs 0) 0)
         (eql (elt regs 1) 0)
         (eql (elt regs 2) 0))))

(defun d04-1 (prefix)
  (loop for i from 0
        when (d04-1-valid-md5 (format nil "~a~a" prefix i))
          return i))

(defun d04-2 (prefix)
  (loop for i from 0
        when (d04-2-valid-md5 (format nil "~a~a" prefix i))
          return i))
