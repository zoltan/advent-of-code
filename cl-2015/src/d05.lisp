(in-package :cl-2015)


(defmacro voyelp (char)
  `(find ,char '(#\a #\e #\i #\o #\u)))

(defun d05-1-nicep (str)
  (loop with prev = nil
        for c across str
        when (voyelp c)
          count c into voyels
        when (eql prev c)
          count c into doubles
        when (or (and (eql prev #\a)
                      (eql c #\b))
                 (and (eql prev #\c)
                      (eql c #\d))
                 (and (eql prev #\p)
                      (eql c #\q))
                 (and (eql prev #\x)
                      (eql c #\y)))
          return nil
        do
             (setf prev c)
        finally (return (and
                         (>= voyels 3)
                         (>= doubles 1)))))


(defun d05-1 (infile)
  (with-open-file (stream infile)
    (loop for line = (read-line stream nil)
          while line
          when (d05-1-nicep line)
            count 1)))

(defun d05-2-nicep (str)
  (and (cl-ppcre:scan "(..).*\\1" str)
       (cl-ppcre:scan "(.).\\1" str)))

(defun d05-2 (infile)
  (with-open-file (stream infile)
    (loop for line = (read-line stream nil)
          while line
          when (d05-2-nicep line)
            count 1)))
