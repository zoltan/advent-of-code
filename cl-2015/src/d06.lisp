(in-package :cl-2015)

(defun d06-make-grid (size)
  (let ((grid (make-hash-table :test #'equalp)))
    (loop named outer for x to size do
      (loop for y to size do
        (setf (gethash (cons x y) grid) 0)))
    grid))

(defun d06-parse-line (line)
  (let ((comps (cl-ppcre:register-groups-bind (action x1 y1 x2 y2)
                   ("(turn on|turn off|toggle) ([0-9]{1,3}),([0-9]{1,3}) through ([0-9]{1,3}),([0-9]{1,3})" line :sharedp t)
                 (list (cond ((string= action "turn on") :on)
                             ((string= action "turn off") :off)
                             ((string= action "toggle") :toggle))
                       x1 y1 x2 y2))))
    (list (first comps)
          (cons (parse-integer (elt comps 1))
                (parse-integer (elt comps 2)))
          (cons (parse-integer (elt comps 3))
                (parse-integer (elt comps 4))))))


(defmacro d06-1-litp (grid coord)
  `(> (gethash ,coord ,grid) 1))

(defmacro d06-1-unlitp (grid coord)
  `(= (gethash ,coord ,grid) 0))

(defun d06-1-on (grid coord)
  (setf (gethash coord grid) 1))

(defun d06-1-off (grid coord)
  (setf (gethash coord grid) 0))

(defun d06-1-toggle (grid coord)
  (if (d06-1-unlitp grid coord)
      (d06-1-on grid coord)
      (d06-1-off grid coord)))

(defun d06-2-on (grid coord)
  (let ((current (gethash coord grid)))
    (setf (gethash coord grid) (1+ current))))

(defun d06-2-toggle (grid coord)
  (let ((current (gethash coord grid)))
    (setf (gethash coord grid) (+ 2 current))))

(defun d06-2-off (grid coord)
  (let ((current (gethash coord grid)))
    (setf (gethash coord grid) (max 0 (1- current)))))

(defun d06-1-apply-instruction (grid instr)
  (let ((name (first instr))
        (start (second instr))
        (end (third instr)))
    (loop for x from (car start) to (car end) do
      (loop for y from (cdr start) to (cdr end) do
        (cond ((eql :on name) (d06-1-on grid (cons x y)))
              ((eql :off name) (d06-1-off grid (cons x y)))
              ((eql :toggle name) (d06-1-toggle grid (cons x y))))))))

(defun d06-2-apply-instruction (grid instr)
  (let ((name (first instr))
        (start (second instr))
        (end (third instr)))
    (loop for x from (car start) to (car end) do
      (loop for y from (cdr start) to (cdr end) do
        (cond ((eql :on name) (d06-2-on grid (cons x y)))
              ((eql :off name) (d06-2-off grid (cons x y)))
              ((eql :toggle name) (d06-2-toggle grid (cons x y))))))))

(defun d06-get-value (grid coord)
  (print (format nil "~s: ~s" coord (gethash coord grid)))
  (gethash coord grid))

(defun d06-count-lit (grid size)
  (loop named outer for x to size
        sum
        (loop for y to size
              sum
              (gethash (cons x y) grid))))

(defun d06-1 (infile)
  (with-open-file (stream infile)
    (let ((grid (d06-make-grid 999)))
      (loop for line = (read-line stream nil nil)
            while line
            for instr = (d06-parse-line line)
            do
               (d06-1-apply-instruction grid instr))
      (format t "~a" (d06-count-lit grid 999)))))

(defun d06-2 (infile)
  (with-open-file (stream infile)
    (let ((grid (d06-make-grid 999)))
      (loop for line = (read-line stream nil nil)
            while line
            for instr = (d06-parse-line line)
            do
               (d06-2-apply-instruction grid instr))
      (format t "~a" (d06-count-lit grid 999)))))
