(in-package :cl-2015)

(defmacro --d07-infix-cond (name symbol words)
  `((equalp (second ,words) ,name)
    (,symbol (car (last words)))))

(defun d07-parse-line (line)
  (let ((words (str:split " " line)))
    (cond ((equalp (first words) "NOT")
           `(:not ,(car (last words)) ,(second words)))
          (--d07-infix-cond "AND" :and words)
          )))

(defun d07-1 (infile)
  (with-open-file (stream infile)
    ))

(defun d07-2 (infile)
  (with-open-file (stream infile)
    (loop for line = (read-line stream nil nil)
          collect (d07-parse-line line))))
