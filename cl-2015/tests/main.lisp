(defpackage cl-2015/tests/main
  (:use :cl
        :cl-2015
        :rove))
(in-package :cl-2015/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :cl-2015)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))

(deftest test-d02-read-size
  (testing "should read-size get three integers"
    (ok (equal (cl-2015::d02-read-size "2x34x789") '(2 34 789)))))

(deftest test-d02-wrap-size
  (testing "wrap area"
    (ok (eql (cl-2015::d02-wrap-size 2 3 4) 58))
    (ok (eql (cl-2015::d02-wrap-size 1 1 10) 43))))

(deftest test-d02-ribbon-length
  (testing "ribbon length"
    (ok (eql (cl-2015::d02-ribbon-length 2 3 4) 34))
    (ok (eql (cl-2015::d02-ribbon-length 1 1 10) 14))))

(deftest test-d03-1
  (testing "d03 1"
    (ok (eql (cl-2015::d03-1-in ">") 2))
    (ok (eql (cl-2015::d03-1-in "^>v<") 4))
    (ok (eql (cl-2015::d03-1-in "^v^v^v^v^v") 2))))

(deftest test-d03-2
  (testing "d03 2"
    (ok (eql (cl-2015::d03-2-in "^v") 3))
    (ok (eql (cl-2015::d03-2-in "^>v<") 3))
    (ok (eql (cl-2015::d03-2-in "^v^v^v^v^v") 11))))

(deftest test-d05-1
  (testing "nice strings"
    (ok (cl-2015::d05-1-nicep "ugknbfddgicrmopn"))
    (ok (cl-2015::d05-1-nicep "aaa")))
  (testing "naughty strings"
    (ng (cl-2015::d05-1-nicep "jchzalrnumimnmhp"))
    (ng (cl-2015::d05-1-nicep "haegwjzuvuyypxyuh"))
    (ng (cl-2015::d05-1-nicep "dvszwmarrgswjxmb"))))

(deftest test-d05-2
  (testing "nice strings"
    (ok (cl-2015::d05-2-nicep "qjhvhtzxzqqjkmpb"))
    (ok (cl-2015::d05-2-nicep "xxyxx")))
  (testing "naughty strings"
    (ng (cl-2015::d05-2-nicep "uurcxstgmygtbstg"))
    (ng (cl-2015::d05-2-nicep "ieodomkazucvgmuy"))))

(deftest test-d06-parse-line
  (testing "turn on"
    (ok (equalp (cl-2015::d06-parse-line "turn on 519,391 through 605,718")
                '(:on (519 . 391) (605 . 718)))))
  (testing "turn off"
    (ok (equalp (cl-2015::d06-parse-line "turn off 524,349 through 694,791")
                '(:off (524 . 349) (694 . 791)))))
  (testing "toggle"
    (ok (equalp (cl-2015::d06-parse-line "toggle 521,303 through 617,366")
                '(:toggle (521 . 303) (617 . 366))))))

(deftest test-d07-parse-line
    (testing "value"
             (ok (equalp
                  (cl-2015::d07-parse-line "123 -> x")
                  '(:assign x 123))))
  (testing "not"
           (ok (equalp
                (cl-2015::d07-parse-line "NOT y -> h")
                '(:not y h))))
  (testing "and"
           (ok (equalp
                (cl-2015::d07-parse-line "a AND b -> c")
                '(:and c a b))))
  (testing "or"
           (ok (equalp
                (cl-2015::d07-parse-line "d OR e -> f")
                '(:or f d e))))
  (testing "lshift"
           (ok (equalp
                (cl-2015::d07-parse-line "g LSHIFT 2 -> h")
                '(:lshift h g 2))))
  (testing "rshift"
           (ok (equalp
                (cl-2015::d07-parse-line "i RSHIFT 2 -> j")
                '(:rshift j i 2)))))
