(defsystem "aoc22"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "d01")
                 (:file "d02")
                 (:file "d03")
                 (:file "d04")
                 (:file "d06")
                 (:file "d09")
                 (:file "d10")
                 (:file "d13"))))
  :description ""
  :in-order-to ((test-op (test-op "aoc22/tests"))))

(defsystem "aoc22/tests"
  :author ""
  :license ""
  :depends-on ("aoc22"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for aoc22"
  :perform (test-op (op c) (symbol-call :rove :run c)))
