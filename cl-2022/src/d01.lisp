(defpackage aoc22-01
  (:use #:cl)
  (:export :d01-1
           :d01-2))

(in-package :aoc22-01)

(defun parse-line (line)
  (unless (equalp line "")
    (parse-integer line)))

(defun read-input (infile)
  (let (calories
        (current 0))
    (with-open-file
        (stream infile)
      (loop for line = (read-line stream nil :eof)
            while (not (eq line :eof))
            for value = (parse-line line)
            if value
              do (incf current value)
            else
              do (setf calories (cons current calories)
                       current 0)))
    calories))

(defun d01-1 (infile)
  (let ((calories (read-input infile)))
    (reduce #'max calories)))

(defun d01-2 (infile)
  (let ((calories (sort (read-input infile) #'>)))
    (apply #'+ (subseq calories 0 3))))
