(defpackage aoc22-03
  (:use #:cl)
  (:export :d03-1
           :d03-2))

(in-package :aoc22-03)

(defun badge-value (char)
  (cond ((and (char<= #\a char)
              (char<= char #\z))
         (1+
            (- (char-code char)
               (char-code #\a))))
        ((and (char<= #\A char)
              (char<= char #\Z))
         (+ 27
            (- (char-code char)
               (char-code #\A))))))

(defun find-badge-1 (line)
  (let ((middle (/ (length line) 2)))
    (first (intersection
            (coerce (subseq line 0 middle) 'list)
            (coerce (subseq line middle) 'list)))))

(defun d03-1 (infile)
  (with-open-file (stream infile)
    (loop for line = (read-line stream nil)
          until (null line)
          sum (badge-value (find-badge-1 line)))))

(defun find-badge-2 (top middle bottom)
  (first (intersection
          (intersection (coerce top 'list)
                        (coerce middle 'list))
          (coerce bottom 'list))))

(defun d03-2 (infile)
  (with-open-file (stream infile)
    (loop for top = (read-line stream nil)
          until (null top)
          for middle = (read-line stream nil)
          for bottom = (read-line stream nil)
          sum (badge-value (find-badge-2 top middle bottom)))))
