(defpackage aoc22-04
  (:use #:cl)
  (:export :d04-1
           :d04-2))

(in-package :aoc22-04)


(defun parse-range (instr)
  (let ((sep (position #\- instr)))
    (list
     (parse-integer (subseq instr 0 sep))
     (parse-integer (subseq instr (1+ sep))))))


(defun parse-line (line)
  (let* ((coma-pos (position #\, line))
         (left (subseq line 0 coma-pos))
         (right (subseq line (1+ coma-pos))))
    (list
     (parse-range left)
     (parse-range right))))

(defun contains-1 (left right)
  (or
   (and (<= (first left) (first right))
        (>= (second left) (second right)))
   (and (<= (first right) (first left))
        (>= (second right) (second left)))))

(defun contains-2 (left right)
  (and (>= (second left) (first right))
       (>= (second right) (first left))))

(defun d04-1 (infile)
  (with-open-file (stream infile)
    (loop for line = (read-line stream nil)
          until (null line)
          count (apply #'contains-1 (parse-line line)))))

(defun d04-2 (infile)
  (with-open-file (stream infile)
    (loop for line = (read-line stream nil)
          until (null line)
          count (apply #'contains-2 (parse-line line)))))
