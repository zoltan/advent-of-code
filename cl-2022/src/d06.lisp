(defpackage aoc22-06
  (:use #:cl)
  (:export :d06-1
           :d06-2))

(in-package :aoc22-06)


(defun d06 (input window-size)
  (loop for pos from 0 to (- (length input) window-size)
        when (= window-size
                (length
                 (remove-duplicates
                  (subseq input pos (+ pos window-size))
                  :test #'equalp)))
          return (+ pos window-size)))

(defun d06-1 (infile)
  (with-open-file (stream infile)
    (d06 (read-line stream nil) 4)))

(defun d06-2 (infile)
  (with-open-file (stream infile)
    (d06 (read-line stream nil) 14)))
