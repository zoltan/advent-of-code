(defpackage aoc22-09
  (:use #:cl)
  (:export :d09-1
           :d09-2))

(in-package :aoc22-09)

(defvar head '(0 . 0))
(defvar tail '(0 . 0))
(defvar *tail-coords* (make-hash-table :test #'equalp))

(defun read-move (value)
  (cons
   (case (elt value 0)
     (#\U 'up)
     (#\D 'down)
     (#\L 'left)
     (#\R 'right))
   (parse-integer value :start 2)))

;; If the head is ever two steps directly up, down, left, or right from the
;; tail, the tail must also move one step in that direction so it remains close
;; enough:
;;
;; Otherwise, if the head and tail aren't touching and aren't in the same row or
;; column, the tail always moves one step diagonally to keep up:

(defun distance-2 (x1 y1 x2 y2)
  (+
   (expt (- y2 y1) 2)
   (expt (- x2 x1) 2)))

(defun get-tail-position (head tail direction)
  (let ((x-head (car head))
        (y-head (cdr head))
        (x-tail (car tail))
        (y-tail (cdr tail)))

    (if (> (distance-2 x-head y-head x-tail y-tail) 2)
        ;; tail must move closer
        (case direction
          (up (cons x-head (1- y-head)))
          (down (cons x-head (1+ y-head)))
          (left (cons (1+ x-head) y-head))
          (right (cons (1- x-head) y-head)))
        ;; head and tail are next to each other, no move needed
        tail)))

(defun move-parts (move)
  (let ((direction (car move))
        (distance (cdr move)))
    (loop for i from 0 below distance
          do
             (setq head
                   (case direction
                     (up (cons (car head) (1+ (cdr head))))
                     (down (cons (car head) (1- (cdr head))))
                     (left (cons (1- (car head)) (cdr head)))
                     (right (cons (1+ (car head)) (cdr head)))))
             (setq tail (get-tail-position head tail direction))
             (setf (gethash tail *tail-coords*) (1+ (gethash tail *tail-coords* 0)))
             (format t "    ~a / ~a~%" head tail))))

(defun d09-1 (infile)
  (setq head '(0 . 0)
        tail '(0 . 0)
        *tail-coords* (make-hash-table :test #'equalp))
  (with-open-file (stream infile)
    (loop for line = (read-line stream nil)
          until (null line)
          for move = (read-move line)
          do
             (format t "head: ~a / tail: ~a -> ~a~%" head tail move)
             (move-parts move)
             (format t "  .. -> ~a / ~a~%" head tail))
    (read-moves stream)))

(defun d09-2 (infile)
  )
