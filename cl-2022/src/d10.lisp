(defpackage aoc22-10
  (:use #:cl)
  (:export :d10-1
           :d10-2))

(in-package :aoc22-10)

(defun collect-cycle-p (low high)
  (loop for x from low below high
        when (equalp (mod (+ x 20) 40) 0)
          return t))

(defun part1 (stream)
  (loop with reg-x = 1
        with cycle = 1
        for line = (read-line stream nil)
        until (null line)

        for cycle-incr = 1 and value = 0
        unless (equalp line "noop")
          do (setq cycle-incr 2
                   value (parse-integer (subseq line 5)))
        end

        when (collect-cycle-p cycle (+ cycle cycle-incr))
          do
             (format t "cycle: ~4a, reg: ~4a: ~a~%" cycle reg-x line)
          and collect reg-x

        do
           ;; (format t "cycle: ~4a, reg: ~4a: ~a~%" cycle reg-x line)
           (setq reg-x (+ reg-x value)
                 cycle (+ cycle cycle-incr))))

(defun d10-1 (infile)
  (with-open-file (stream infile)
    (loop for cycle in '(20 60 100 140 180 220 260)
          for reg in (part1 stream)
          do (format t "~a ~a -> ~a~%" cycle reg (* cycle reg))
          sum (* cycle reg))))

(defun part2 (stream)
  (loop with reg-x = 1
        with cycle = 0
        for line = (read-line stream nil)
        until (null line)

        for cycle-incr = 1 and value = 0
        unless (equalp line "noop")
          do (setq cycle-incr 2
                   value (parse-integer (subseq line 5)))
        end

        nconcing (loop for c from cycle repeat cycle-incr
                       for offset = (mod c 40)
                       ;; do (format t "DRAW: ~3a(~2a) reg: ~2a ~a~%" c offset reg-x (<= (1- reg-x) offset (1+ reg-x)))
                       if (<= (1- reg-x) offset (1+ reg-x))
                         collect #\#
                       else
                         collect #\.)

        do
           ;; (format t "cycle: ~4a, reg: ~4a: ~a~%" cycle reg-x line)
           (setq reg-x (+ reg-x value)
                 cycle (+ cycle cycle-incr))))

(defun d10-2 (infile)
  (with-open-file (stream infile)
    (loop with oneline = (coerce (part2 stream) 'string)
          for start from 0 below (* 40 6) by 40
          for end from 40 by 40
          do
             (format t "~a~%" (subseq oneline start end)))))
