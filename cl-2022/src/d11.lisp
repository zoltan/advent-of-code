(defpackage aoc22-11
  (:use #:cl)
  (:export :d11-1
           :d11-2))

(in-package :aoc22-11)

(defclass monkey ()
  ((number
    :initarg :number
    :initform (error "required"))
   (items
    :initarg :items
    :initform (error "required"))
   (operation
    :initarg :operation
    :initform (error "required"))
   (test
    :initarg :test
    :initform (error "required"))
   (inspections
    :initform 0)))

(defmethod print-object ((obj monkey) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots ((num number)
                 (items items)
                 (inspections inspections))
        obj
      (format stream "num:~a, inspections: ~4a, items: ~a" num inspections items))))

(defmethod monkey-receive-item ((obj monkey) item)
  (with-slots ((items items))
      obj
    (setf items (append items (list item)))))

(defmethod monkey-drop-item ((obj monkey))
  (with-slots ((items items))
      obj
    (setf items (cdr items))))

(defmethod monkey-inspect-item ((obj monkey))
  (with-slots ((items items)
               (inspections inspections))
      obj
    (let ((head (first items)))
      (when head
        (incf inspections))
      head)))

(defun parse-list (str start)
  (when (< start (length str))
    (multiple-value-bind (num offset)
        (parse-integer str :start start :junk-allowed t)
      (when num
        (cons num (parse-list str (+ offset 2)))))))

(defun read-monkey (stream)
  (flet
      ((get-num (line)
         (parse-integer line :start 7 :junk-allowed t))

       (read-operation (line)
         (let ((op (case (elt line 23)
                     (#\+ #'+)
                     (#\* #'*)
                     (otherwise (error "unknown op ~a" (elt line 23)))))
               (right (parse-integer line :start 24 :junk-allowed t)))

           (cond
             ((null right) (lambda (x) (apply op (list x x))))
             (t (lambda (x) (apply op (list right x)))))))

       (read-test (l1 l2 l3)
         (let ((threshold (parse-integer l1 :start 21))
               (on-true (parse-integer l2 :start 29))
               (on-false (parse-integer l3 :start 30)))
           (lambda (x)
             (if (= (mod x threshold) 0)
                 on-true
                 on-false)))))

    (make-instance 'monkey
                   :number (get-num (read-line stream))
                   :items (parse-list (read-line stream) 18)
                   :operation (read-operation (read-line stream))
                   :test (apply #'read-test (loop repeat 3 collect (read-line stream))))))

(defun read-monkeys (stream)
  (let ((monkey (read-monkey stream)))
    (if (read-line stream nil nil)
        (cons monkey (read-monkeys stream))
        (list monkey))))

(defun run-round-1 (monkeys)
  (loop for monkey across monkeys
        do
           ;; (format t "  Monkey ~a~%" monkey)
           (loop for item = (monkey-inspect-item monkey)
                 while item
                 for old-worry = (apply (slot-value monkey 'operation) (list item))
                 for new-worry = (floor old-worry 3)
                 for next-monkey = (apply (slot-value monkey 'test) (list new-worry))
                 do
                    ;; (format t "    item: ~a -> ~a -> ~a -- sent to ~a~%" item old-worry new-worry next-monkey)
                    (monkey-receive-item (elt monkeys next-monkey) new-worry)
                    (monkey-drop-item monkey))))

(defun d11-1 (infile &key (rounds 20))
  (with-open-file (stream infile)
    (let ((monkeys (coerce (read-monkeys stream) 'vector)))
      (loop for r from 1 to rounds
            do
               (format t "ROUND ~a:~%" r)
               (run-round-1 monkeys)
               (format t "~{  ~a~%~}" (coerce monkeys 'list)))

      (loop for monkey across monkeys
            collect (slot-value monkey 'inspections) into ret
            finally
               (let ((activities (sort ret #'>)))
                 (return (* (first activities) (second activities))))))))

(defun run-round-2 (monkeys)
  (loop for monkey across monkeys
        do
           (format t "  Monkey ~a~%" monkey)
           (loop for item = (monkey-inspect-item monkey)
                 while item
                 for worry = (apply (slot-value monkey 'operation) (list item))
                 for next-monkey = (apply (slot-value monkey 'test) (list worry))
                 do
                    (format t "    item: ~a -> ~a -- sent to ~a~%" item worry next-monkey)
                    (monkey-receive-item (elt monkeys next-monkey) worry)
                    (monkey-drop-item monkey))))

(defun d11-2 (infile &key (rounds 10000))
  (with-open-file (stream infile)
    (let ((monkeys (coerce (read-monkeys stream) 'vector)))
      (loop for r from 1 to rounds
            do
               (format t "ROUND ~a:~%" r)
               (run-round-2 monkeys)
               ;; (format t "~{  ~a~%~}" (coerce monkeys 'list))
            )

      (loop for monkey across monkeys
            collect (slot-value monkey 'inspections) into ret
            finally
               (let ((activities (sort ret #'>)))
                 (return (* (first activities) (second activities))))))))
