(defpackage aoc22-13
  (:use #:cl)
  (:export :d13-1
           :d13-2))

(in-package :aoc22-13)

(defun read-list (line)
  (flet
      ((clean-string (s)
         (loop for c across s

               if (equalp c #\[)
                 collect #\( into ret
               else if (equalp c #\])
                   collect #\) into ret
               else if (equalp c #\,)
                   collect #\  into ret
               else
                 collect c into ret
               finally
                  (return (coerce ret 'string)))))

    (with-input-from-string (s (clean-string line))
      (read s))))


(defun cmp-list-1 (left right &key (offset 0))
  ;; (format t "~vt- Compare ~a vs. ~a~%" offset left right)
  (cond
    ((null left)
     (if (null right)
         'same
         'good))
    ((null right) 'bad)
    ((and (numberp left)
          (numberp right))
     (cond
       ((< left right) 'good)
       ((< right left) 'bad)
       (t 'same)))
    ((numberp left)
     (cmp-list-1 (list left) right))
    ((numberp right)
     (cmp-list-1 left (list right)))

    (t
     (let ((head (cmp-list-1 (first left) (first right) :offset (+ offset 4))))
       (if (equalp head 'same)
           (cmp-list-1 (cdr left) (cdr right))
           head)))))

(defun d13-1 (infile)
  (with-open-file (stream infile)
    (loop
      for n from 1
      for lline = (read-line stream nil)
      while lline
      for left = (read-list lline)
      for right = (read-list (read-line stream nil))
      do
         (read-line stream nil)
         (format t "---~%~a >> ~a <<>> ~a~%" n left right)
      when (equalp (cmp-list-1 left right) 'good)
        sum n
        and do (format t "GOOD[~a]" n)

      )))

(defun packet< (left right)
  (case (cmp-list-1 left right)
    ('good t)
    (otherwise nil)))

(defun d13-2 (infile)
  (with-open-file (stream infile)
    (let ((packets (append
                    '(((2)) ((6)))
                    (loop for line = (read-line stream nil)
                          while line
                          unless (equalp line "")
                            collect (read-list line)))))
      (loop for n from 1
            for packet in (sort packets #'packet<)
            when (or (equalp packet '((2)))
                     (equalp packet '((6))))
              collect n into ret
            finally (return (apply #'* ret))))))
