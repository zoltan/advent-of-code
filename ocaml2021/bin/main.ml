open Aoc2021

exception NoSolution of int * int

let usage = "aoc2021 [-v] -d day -p part INFILE"

let verbose = ref false
let day = ref 1
let part = ref 1
let infile = ref ""

let speclist =
  [("-v", Arg.Set verbose, "Output debug information");
   ("-d", Arg.Set_int day, "Puzzle day");
   ("-p", Arg.Set_int part, "Puzzle part");]

let anon_func filename =
  infile := filename

let rec read_input chan =
  try
    let line = input_line chan in
    line :: read_input chan
  with End_of_file -> []

let () =
  Arg.parse speclist anon_func usage;;
  Printf.printf "Running %d:%d with input from %s\n" !day !part !infile;;

  let inchan =
    if
      !infile = ""
    then
      Stdlib.stdin
    else
      open_in !infile
  in
  let input = read_input inchan
  in
  let answer =
    match (!day, !part) with
    | 1, 1 -> D01.part1 input
    | 1, 2 -> D01.part2 input
    | 2, 1 -> D02.part1 input
    | 2, 2 -> D02.part2 input
    | 3, 1 -> D03.part1 input
    | 3, 2 -> D03.part2 input
    | 4, 1 -> D04.part1 input
    | 4, 2 -> D04.part2 input
    | _ -> raise(NoSolution(!day, !part))
  in
  print_endline(string_of_int(answer))
