let rec count_increases acc = function
  | [] -> acc
  | _ :: [] -> acc
  | x :: y :: t -> count_increases (if x < y then acc + 1 else acc) (y :: t)

let part1 input =
  let values = List.map int_of_string input in
  count_increases 0 values

let part2 input =
  let values = List.map int_of_string input
  in
  let rec triples_sum = function
    | [] | _ :: [] | _ :: _ :: [] -> []
    | x :: y :: z :: t -> (x + y + z) :: triples_sum (y :: z :: t)
  in
  let l = triples_sum values
  in count_increases 0 l
