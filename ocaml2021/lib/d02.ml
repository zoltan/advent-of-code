type direction =
  | Forward of int
  | Dive of int

exception BadDirection of string list

let parse_direction value =
  match String.split_on_char ' ' value with
  | "forward" :: x :: [] -> Forward(int_of_string x)
  | "down" :: x :: [] -> Dive(int_of_string x)
  | "up" :: x :: [] -> Dive(int_of_string(x) * -1)
  | err -> raise(BadDirection(err))

let part1 input =
  let directions = List.map parse_direction input in
  let folder (forward, dive) = function
    | Forward(x) -> (forward + x, dive)
    | Dive(x) -> (forward, dive + x)
  in
  let (x, y) =  List.fold_left folder (0, 0) directions in
  x * y

let part2 input =
  let directions = List.map parse_direction input
  in
  let folder (forward, dive, aim) = function
    | Forward(x) -> (forward + x, dive + (aim * x), aim)
    | Dive(x) -> (forward, dive, aim + x)
  in
  let (x, y, _) =  List.fold_left folder (0, 0, 0) directions
  in
  x * y
