let parse_line line =
  let chars = List.of_seq (String.to_seq line) in
  let bits = List.map (fun c -> if c = '1' then 1 else 0) chars in
  bits

let bit_sum report =
  List.fold_left
    (fun acc elem -> List.map2 (fun x y -> x + y) acc elem)
    (List.map (fun _ -> 0) (List.hd report))
    report

let part1 input =
  let report = List.map parse_line input in
  let report_len = List.length input in
  let bit_sums = bit_sum report in
  let folder total (gamma, epsilon) elem =
    if elem > (total - elem)
    then
      ((gamma lsl 1) + 1, epsilon lsl 1)
    else
      (gamma lsl 1, (epsilon lsl 1) + 1)
  in
  let (gamma, epsilon) = List.fold_left (folder report_len) (0, 0) bit_sums in
  gamma * epsilon



(* let filter_report report pos value =
 *   List.filter (fun x -> List.nth x pos = value) report *)

let part2 _input =
  (* let report = List.map parse_line input in
   * let report_len = List.length input in
   * let bit_sums = bit_sum report in
   * let oxygen = *)
  1
