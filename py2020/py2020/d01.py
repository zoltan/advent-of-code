from itertools import combinations
import logging
from math import prod

from py2020.cli import cli


def finder(numbers, size, target):
    combs = combinations(numbers, size)
    predicate = lambda x: sum(x) == target
    return filter(predicate, combs)

def main(size):
    args = cli()
    raw_nums = [int(x) for x in args.infile]
    for item in finder(raw_nums, size, 2020):
        logging.info("prod: %s => %d", item, prod(item))

def part1():
    main(2)

def part2():
    main(3)
