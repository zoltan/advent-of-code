from collections import Counter
import logging
import re

from py2020.cli import cli


PATTERN = re.compile(r"^([0-9]+)-([0-9]+) (.): (.+)$")

def is_valid_p1(line):
    ret = PATTERN.match(line)
    min_num, max_num, char, password = ret.groups()
    occurs = Counter(password)
    return int(min_num) <= occurs[char] <= int(max_num)

def is_valid_p2(line):
    ret = PATTERN.match(line)
    pos1, pos2, char, password = ret.groups()
    # rely on bool.__xor__
    return (password[int(pos1) - 1] == char) ^ (password[int(pos2) - 1] == char)

def main(validator):
    args = cli()

    counter =  Counter(map(lambda x: validator(x.rstrip()), args.infile))
    logging.info("Count: %s", counter)
    logging.info("Valid passwords: %d", counter[True])

def part1():
    main(is_valid_p1)

def part2():
    main(is_valid_p2)
