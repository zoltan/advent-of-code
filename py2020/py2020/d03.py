from collections import Counter
from enum import auto, IntFlag
import logging

from py2020.cli import cli


class Cell(IntFlag):
    OPEN = auto()
    TREE = auto()

    @staticmethod
    def from_char(char):
        mapping = {
            ".": Cell.OPEN,
            "#": Cell.TREE,
        }
        return mapping.get(char)

    def __str__(self):
        mapping = {
            Cell.OPEN: ".",
            Cell.TREE: "#",
        }
        return mapping.get(self.value)


def path_generator(max_width, right, down):
    x_pos = 0
    for y_pos in  range(0, max_width, down):
        yield (x_pos, y_pos)
        x_pos += right


class Map:

    def __init__(self, map_2d):
        self.map_2d = map_2d
        self.height = len(self.map_2d)
        self.width = len(self.map_2d[0])

    @staticmethod
    def from_file(infile):
        rows = []
        for line in infile:
            rows.append([Cell.from_char(cell) for cell in line.strip()])
        return Map(rows)

    def __str__(self) -> str:
        ret = [f"Size: {self.height} x {self.width}", "Map:"]
        for row in self.map_2d:
            ret.append("".join(map(str, row)))
        return "\n".join(ret)

    def at(self, x_coord: int, y_coord: int) -> Cell:
        return self.map_2d[y_coord][x_coord % self.width]


def part1():
    args = cli()

    results = Counter()
    tree_map = Map.from_file(args.infile)
    results = Counter((tree_map.at(*x) for x in path_generator(tree_map.height, 3, 1)))
    logging.info("Results: %s", results)
    logging.info("Trees: %s", results[Cell.TREE])

def part2():
    args = cli()

    steps = (
        (1, 1,),
        (3, 1,),
        (5, 1,),
        (7, 1,),
        (1, 2,),
    )

    tree_map = Map.from_file(args.infile)

    result = 1
    for right, down in steps:
        ret = Counter((tree_map.at(*x) for x in path_generator(tree_map.height, right, down)))
        result = result * ret[Cell.TREE]
        logging.info("(%d, %d) => %s", right, down, ret)

    logging.info("Trees: %s", result)
