from collections import Counter
import logging
import re

from py2020.cli import cli

HEX_COLOR = re.compile(r"^#[0-9a-f]{6}$")
PID_PATTERN = re.compile(r"^[0-9]{9}$")

def batch_splitter(infile):
    current = []
    for line in infile:
        if line != "\n":
            current.append(line.strip())
        else:
            if current:
                yield current
            current = []
    if current:
        yield current

def read_document(raw):
    words = " ".join(raw).split()
    return dict(x.split(":") for x in words)

def valid_passport_p1(doc):
    required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
    for key in required:
        if key not in doc:
            return False
    return True

def is_valid_int_range(value, min_val, max_val):
    try:
        return min_val <= int(value) <= max_val
    except ValueError:
        return False

def valid_passport_p2(doc):
    # byr (Birth Year) - four digits; at least 1920 and at most 2002.
    # iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    # eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    # hgt (Height) - a number followed by either cm or in:
    #     If cm, the number must be at least 150 and at most 193.
    #     If in, the number must be at least 59 and at most 76.
    # hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    # ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    # pid (Passport ID) - a nine-digit number, including leading zeroes.
    # cid (Country ID) - ignored, missing or not.

    validators = (
        ("byr", lambda x: is_valid_int_range(x, 1920, 2002)),
        ("eyr", lambda x: is_valid_int_range(x, 2020, 2030)),
        ("iyr", lambda x: is_valid_int_range(x, 2010, 2020)),
        ("hgt", _is_valid_height),
        ("hcl", _is_valid_hair_color),
        ("ecl", _is_valid_eye_color),
        ("pid", _is_valid_pid),
    )
    for key, func in validators:
        if key not in doc:
            logging.debug("Key %s is missing from document %s", key, doc)
            return False
        if not func(doc[key]):
            logging.debug("Key %s (value %s) is invalid in document %s", key, doc[key], doc)
            return False
    return True

def _is_valid_height(value):
    if value.endswith("cm"):
        return is_valid_int_range(value[:-2], 150, 193)
    elif value.endswith("in"):
        return is_valid_int_range(value[:-2], 59, 76)
    return False

def _is_valid_hair_color(value):
    return HEX_COLOR.match(value) is not None

def _is_valid_eye_color(value):
    return value in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

def _is_valid_pid(value):
    return PID_PATTERN.match(value) is not None

def main(validator):
    args = cli()
    docs = map(read_document, batch_splitter(args.infile))
    results = Counter(map(validator, docs))
    logging.info(results)
    logging.info("Valid passports: %d", results[True])


def part1():
    main(valid_passport_p1)

def part2():
    main(valid_passport_p2)
