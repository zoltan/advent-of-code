import logging

from py2020.cli import cli


def read_seat_id(instr):
    row = int(instr[:7].translate({ord("F"): "0", ord("B"): "1"}), 2)
    col = int(instr[-3:].translate({ord("L"): "0", ord("R"): "1"}), 2)
    logging.debug("%s means row %d col %d", instr, row, col)
    return (row, col, row * 8 + col)

def part1():
    args = cli()
    logging.info("Highest seat ID: %d", max(read_seat_id(x.strip())[2] for x in args.infile))

def part2():
    args = cli()
    seat_ids = [read_seat_id(x.strip())[2] for x in args.infile]
    expected = sum(range(min(seat_ids), max(seat_ids) + 1))
    total = sum(seat_ids)
    logging.info("Diff: %d", expected - total)
