from collections import Counter
import logging

from py2020.cli import cli

def read_groups(infile):
    current = []
    for line in infile:
        line = line.strip()
        if line:
            current.append(line)
        else:
            yield current
            current = []
    if current:
        yield current

def part1():

    args = cli()

    total = 0
    for group in read_groups(args.infile):
        count = Counter()
        for person in group:
            count.update(person)
        total += len(count.keys())

    logging.info("Total: %d", total)

def part2():

    args = cli()

    total = 0
    for group in read_groups(args.infile):
        count = Counter()
        for person in group:
            count.update(person)
        group_size = len(group)
        total += sum(x == group_size for x in count.values())
    logging.info("Total: %d", total)
