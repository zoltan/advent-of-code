from collections import Counter
import logging
import re

from py2020.cli import cli


BAG_PATTERN = re.compile(r"^(\d+) (.+) bags?$")


def parse_rule(rule):
    container, contained = rule.split("bags contain ")

    container = container.strip()
    if contained == "no other bags.":
        logging.debug("Parsing '%s' => %s", rule, (container, []))
        return (container, [])

    bags = []
    for bag in contained.rstrip(".").split(", "):
        ret = BAG_PATTERN.match(bag)
        bags.append((int(ret.group(1)), ret.group(2)))
    logging.debug("Parsing '%s' => %s", rule, (container, bags))
    return (container, bags)

def read_rules(infile):
    return dict([parse_rule(x.strip()) for x in infile])

def bag_contains(rules, src, dest):
    right = rules[src]
    for _, bag in right:
        if bag == dest:
            return True
        elif bag_contains(rules, bag, dest):
            return True
    return False

def bags_inside(rules, src):
    total = 0
    for qty, bag in rules[src]:
        total += qty * bags_inside(rules, bag) + qty
    logging.debug("Count [%s] => %d bags", src, total)
    return total

def part1():

    args = cli()

    rules = read_rules(args.infile)
    logging.info("Loaded %d rules.", len(rules))
    total = sum(bag_contains(rules, x, "shiny gold") for x in rules.keys())
    logging.info("Total: %d", total)

def part2():

    args = cli()

    rules = read_rules(args.infile)
    logging.info("Loaded %d rules.", len(rules))
    logging.info("Total: %d", bags_inside(rules, "shiny gold"))
