from copy import copy
from enum import auto, IntFlag
import logging

from py2020.cli import cli


class Operation(IntFlag):
    NOP = auto()
    JMP = auto()
    ACC = auto()

    @staticmethod
    def from_code(char):
        mapping = {
            "nop": Operation.NOP,
            "jmp": Operation.JMP,
            "acc": Operation.ACC,
        }
        return mapping.get(char)

def run_no_repeat(code):
    code_len = len(code)
    visited = [0, ] * code_len
    accumulator = 0
    current_pc = 0
    exit_code = 0

    while current_pc < code_len:
        ope, arg = code[current_pc]
        logging.debug("PC: %04d - ACC: % 4d - Ope: %s", current_pc, accumulator, (ope, arg))
        if visited[current_pc] > 0:
            logging.debug("Instruction at %04d already executed.", current_pc)
            exit_code = 1
            break
        visited[current_pc] += 1
        if ope == Operation.ACC:
            accumulator += arg
            current_pc += 1
        elif ope == Operation.JMP:
            current_pc += arg
        else:
            current_pc += 1

    logging.info("Execution finished. Final PC: %d, acc: %d", current_pc, accumulator)
    return (exit_code, accumulator)

def fix_code(code):

    for idx, (ope, arg) in enumerate(code):
        if ope == Operation.ACC:
            continue

        logging.info("Replacing operation at address %04d: %s", idx, ope)
        dup = copy(code)
        if ope == Operation.JMP:
            dup[idx] = (Operation.NOP, arg)
        else:
            dup[idx] = (Operation.JMP, arg)
        yield dup

def read_source(infile):
    src = []
    for line in infile:
        ope, arg = line.strip().split()
        src.append((Operation.from_code(ope), int(arg)))
    return src

def part1():
    args = cli()
    src = read_source(args.infile)
    _, acc = run_no_repeat(src)
    logging.info("Final accumulator: %d", acc)

def part2():
    args = cli()
    src = read_source(args.infile)
    for code in fix_code(src):
        exit_status, acc = run_no_repeat(code)
        if exit_status == 0:
            logging.info("Final accumulator: %d", acc)
            break
