from collections import deque
from itertools import combinations
import logging

from py2020.cli import cli


def read_file(infile, preamble_len):
    queue = deque(maxlen=preamble_len)
    for _ in range(preamble_len):
        queue.append(int(infile.readline()))

    for line in infile:
        current = int(line)
        yield (current, queue)
        queue.append(current)

def part1():
    args = cli([
        (("--size", "-s"), {"type": int, "default": 25}),
    ])
    preambule_size = args.size
    for num, previous in read_file(args.infile, preambule_size):
        logging.debug("Testing number %d with numbers %s", num, previous)

        combs = combinations(previous, 2)
        predicate = lambda x: sum(x) == num
        if not any(map(predicate, combs)):
            logging.info("%d is not a sum of any of two of previous %d numbers", num, preambule_size)

def reduce_queue(target, queue):
    total = sum(queue)
    while total > target:
        queue.popleft()
        total = sum(queue)
    return (total == target, queue)

def part2():
    args = cli([
        (("--target", "-t"), {"type": int}),
    ])

    queue = deque()

    for line in args.infile:
        queue.append(int(line))
        has_target, queue = reduce_queue(args.target, queue)
        if has_target:
            break

    logging.debug("Final list: %s", queue)
    results = min(queue), max(queue)
    logging.info("Min/Max: %s => sum: %d", results, sum(results))
