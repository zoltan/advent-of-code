from collections import Counter
from itertools import chain
import logging
from math import prod

from py2020.cli import cli



def pair_generator(ite):
    current = ite[0]
    for item in ite[1:]:
        yield current, item
        current = item

def part1():
    args = cli()
    adapters = sorted([int(x) for x in args.infile])
    logging.info("Found %d adapters", len(adapters))

    adapters = [0, ] + adapters + [adapters[-1] + 3]
    counter = Counter(y - x for x, y in pair_generator(adapters))
    logging.info("Counter: %s", counter)
    logging.info("Result: %d", counter[1] * counter[3])


def part2():
    args = cli()
    adapters = sorted([int(x) for x in args.infile])
    logging.info("Found %d adapters", len(adapters))

    adapters = [0, ] + adapters + [adapters[-1] + 3]
    steps = [y - x for x, y in pair_generator(adapters)]
    mapping = {
        "": 1,
        "1": 1,
        "11": 2,
        "111": 4,
        "1111": 7,
    }
    logging.info("result: %d", prod([mapping[x] for x in "".join(str(x) for x in steps).split("3")]))
