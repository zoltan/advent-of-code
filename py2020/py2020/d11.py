from collections import Counter
from copy import deepcopy
from itertools import product, repeat
import logging

from py2020.cli import cli


def valid_position(pos, size):
    return (0 <= pos[0] < size[0]) and (0 <= pos[1] < size[1])

def get_neighbours(pos, lobby, size):
    offsets = [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]
    pos_x, pos_y = pos

    neighbours = []
    for off_x, off_y in offsets:
        if valid_position((pos_x + off_x, pos_y + off_y), size):
            neighbours.append(lobby[pos_x + off_x][pos_y + off_y])
    return neighbours

def new_state_p1(lobby, size, x, y):
    surroundings = Counter(get_neighbours((x, y), lobby, size))
    current = lobby[x][y]

    if current == "L" and surroundings["#"] == 0:
        return "#"
    elif current == "#" and surroundings["#"] >= 4:
        return "L"
    return current

def lookup(lobby, size, pos, steps):
    neighbours = []

    if steps[0] > 0:
        x_range = range(pos[0] + 1, size[0], 1)
    elif steps[0] == 0:
        x_range = repeat(pos[0], size[0])
    else:
        x_range = range(pos[0] - 1, -1, -1)

    if steps[1] > 0:
        y_range = range(pos[1] + 1, size[1], 1)
    elif steps[1] == 0:
        y_range = repeat(pos[1], size[1])
    else:
        y_range = range(pos[1] - 1, -1, -1)

    for x, y in zip(x_range, y_range):
        if lobby[x][y] in "#L":
            return lobby[x][y]
    return "."

def get_sight_neighbours(lobby, size, pos):
    steps = [
        (-1, -1),
        (1, 1),
        (-1, 1),
        (1, -1),
        (0, -1),
        (-1, 0),
        (0, 1),
        (1, 0),
    ]
    return [lookup(lobby, size, pos, item) for item in steps]


def new_state_p2 (lobby, size, x, y):
    surroundings = Counter(get_sight_neighbours(lobby, size, (x, y)))
    current = lobby[x][y]

    if current == "L" and surroundings["#"] == 0:
        return "#"
    elif current == "#" and surroundings["#"] >= 5:
        return "L"
    return current


def run_round(lobby, size, finder):
    new_lobby = deepcopy(lobby)
    changes = 0

    for x, y in product(range(size[0]), range(size[1])):
        if lobby[x][y] == ".":
            continue
        new_state = finder(lobby, size, x, y)
        new_lobby[x][y] = new_state
        changes += lobby[x][y] != new_state

    return changes, new_lobby

def show_lobby(lobby):
    print("X" * 80)
    print("\n".join("".join(x) for x in lobby))
    print("X" * 80)


def main(finder):
    args = cli()
    lobby = [list(x.strip()) for x in args.infile]
    size = (len(lobby), len(lobby[0]))
    logging.info("Lobby size: %s", size)

    changes = 1
    round = 0
    while changes > 0:
        changes, lobby = run_round(lobby, size, finder)
        logging.info("Round %d => %d changes", round, changes)
        round += 1

    states = Counter()
    for line in lobby:
        states += Counter(line)
    logging.info("Final states: %s", states)

def part1():
    main(new_state_p1)

def part2():
    main(new_state_p2)
