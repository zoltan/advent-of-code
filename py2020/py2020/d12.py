from enum import auto, IntFlag
import logging

from py2020.cli import cli


class ShipOne:

    def __init__(self):
        self.pos_x = 0
        self.pos_y = 0
        self.direction = 90

    def move(self, instruction):
        direction = instruction[0]
        value = int(instruction[1:])

        if direction == "L":
            self.direction = (self.direction - value) % 360
        elif direction == "R":
            self.direction = (self.direction + value) % 360
        elif direction == "F":
            direction = ["N", "E", "S", "W"][self.direction // 90]

        if direction == "W":
            self.pos_x -= value
        elif direction == "E":
            self.pos_x += value
        elif direction == "N":
            self.pos_y -= value
        elif direction == "S":
            self.pos_y += value

    def __str__(self):
        return f"ship at {self.pos_x: 6}:{self.pos_y:6}, oriented at {self.direction}"

class Waypoint:

    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude

    def move(self, instruction, value):

        if instruction == "W":
            self.longitude -= value
        elif instruction == "E":
            self.longitude += value
        elif instruction == "N":
            self.latitude += value
        elif instruction == "S":
            self.latitude -= value
        elif instruction == "R":
            self.rotate(value)
        else:
            self.rotate(value * -1)

    def rotate(self, value):
        cos = [1, 0, -1, 0]
        sin = [0, 1, 0, -1]

        value = (value % 360) // 90

        new_lon = cos[value] * self.longitude + sin[value] * self.latitude
        new_lat = -1 * sin[value] * self.longitude + cos[value] * self.latitude

        self.longitude = new_lon
        self.latitude = new_lat

    def __repr__(self):
        return f"Waypoint({self.latitude}, {self.longitude})"

def part1():
    args = cli()
    ship = ShipOne()
    for line in args.infile:
        line = line.strip()
        ship.move(line)
        logging.debug("Line '% 6s' => %s", line, ship)
    logging.info("Final postion: %s", ship)
    logging.info("Manhattan distance: %d", abs(ship.pos_x) + abs(ship.pos_y))

def part2():
    args = cli()
    waypoint = Waypoint(1, 10)
    ship = (0, 0)
    for line in args.infile:
        line = line.strip()
        instruction = line[0]
        value = int(line[1:])

        if instruction == "F":
            ship = (ship[0] + value * waypoint.latitude, ship[1] + value * waypoint.longitude)
        else:
            waypoint.move(instruction, value)

        logging.debug(">> line: [%s] => ship: %s => waypoint: %s", line, ship, waypoint)

    logging.info("Ship is at %s", ship)
    logging.info("Manhattan distance: %d", abs(ship[0]) + abs(ship[1]))
