import logging

from py2020.cli import cli


def part1():
    args = cli()
    timestamp = int(args.infile.readline())
    buses = (int(x) for x in args.infile.readline().split(",") if x != "x")
    next_bus = min(map(lambda x: (x - timestamp % x, x), buses))
    logging.info("Earliest bus %d in %d minutes. %d * %d = %d",
                 next_bus[0], next_bus[1], next_bus[0], next_bus[1], next_bus[0] *next_bus[1], )

def find_timestamp(buses):

    step = 1
    buses = list(sorted(buses, reverse=True))
    timestamp = 0

    for num, offset in buses:
        while timestamp < num or (timestamp + offset) % num != 0:
            timestamp += step
        step *= num
    return timestamp

def read_buses(line):
    return [(int(x), pos) for pos, x in enumerate(line.split(",")) if x != "x"]

def part2():
    args = cli()
    # skip timestamp
    args.infile.readline()
    buses = read_buses(args.infile.readline())
    logging.info("Result: %d", find_timestamp(buses))
