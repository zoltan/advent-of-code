from collections import defaultdict
import logging
import re

from py2020.cli import cli


class Machine:

    def __init__(self):
        self.memory = defaultdict(int)
        self._reset_mask()

    def _reset_mask(self):
        self.mask = [
            2 ** 36 - 1,
            0,
        ]

    def _set_mask(self, value):
        self._reset_mask()

        for idx, bit in enumerate(value[::-1]):
            if bit == "0":
                self.mask[0] = self.mask[0] & ((2 **  36 - 1) ^ 2 ** idx)
            elif bit == "1":
                self.mask[1] = self.mask[1] ^ (2 ** idx)

    def _set_addr(self, addr, value):

        value = (value & self.mask[0]) | self.mask[1]
        self.memory[addr] = value


    def run(self, infile):
        mask_pattern = re.compile(r"^mask = ([X01]{36})$")
        addr_pattern = re.compile(r"^mem\[([0-9]+)\] = ([0-9]+)")

        for line in infile:
            line = line.strip()

            ret = mask_pattern.match(line)
            if ret is not None:
                logging.info("NEW MASK from line [%s] => %s", line, ret.groups())
                self._set_mask(ret.group(1))
                continue

            ret = addr_pattern.match(line)
            if ret is not None:
                logging.info("SET ADDR from line [%s] => %s", line, ret.groups())
                self._set_addr(int(ret.group(1)), int(ret.group(2)))

    def memsum(self):
        return sum(self.memory.values())


def build_masks(floating_mask, prefix=""):
    if not floating_mask:
        return [prefix,]

    bit = floating_mask[0]
    if bit == "X":
        return (build_masks(floating_mask[1:], prefix + "0")
                + build_masks(floating_mask[1:], prefix + "1"))
    else:
        return build_masks(floating_mask[1:], prefix + bit)


class FloatingMachine:

    def __init__(self):
        self.memory = defaultdict(int)
        self.mask = None

    def _apply_floating_mask(self, value):
        return "".join(map(lambda x: x[0] if x[0] in "1X" else x[1], zip(self.mask, f"{value:036b}")))

    def _set_addr(self, addr, value):
        floating_addr = self._apply_floating_mask(addr)

        logging.info(f"Writting at address {floating_addr:36} => {value}")
        for addr in build_masks(floating_addr):
            logging.debug(f"  {addr:36} ({int(addr, 2)}) => {value}")
            self.memory[int(addr)] = value

    def run(self, infile):
        mask_pattern = re.compile(r"^mask = ([X01]{36})$")
        addr_pattern = re.compile(r"^mem\[([0-9]+)\] = ([0-9]+)")

        for line in infile:
            line = line.strip()

            ret = mask_pattern.match(line)
            if ret is not None:
                logging.info("NEW MASK from line [%s] => %s", line, ret.groups())
                self.mask = ret.group(1)
                continue

            ret = addr_pattern.match(line)
            if ret is not None:
                logging.info("SET ADDR from line [%s] => %s", line, ret.groups())
                self._set_addr(int(ret.group(1)), int(ret.group(2)))

    def memsum(self):
        return sum(self.memory.values())


def part1():
    args = cli()

    machine = Machine()
    machine.run(args.infile)
    logging.info("Memory sum: %d", machine.memsum())

def part2():
    args = cli()

    machine = FloatingMachine()
    machine.run(args.infile)
    logging.info("Memory sum: %d", machine.memsum())
