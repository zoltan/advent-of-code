import logging

from py2020.cli import cli


def part1():
    args = cli(extra_args=[
        (
            ("--limit", "-l"),
            {"type": int, "default": 2020},
        ),
    ])

    spoken = [int(x) for x in args.infile.readline().split(",")]
    spoken.reverse()
    print(spoken)

    for turn in range(len(spoken), args.limit):
        try:
            # logging.debug(spoken)
            idx = spoken[1:].index(spoken[0])
            spoken.insert(0, idx + 1)
            logging.debug("[turn %04d] Found %4d at position %4d\t=> %s", turn, spoken[1], idx + 1, spoken)
        except ValueError:
            spoken.insert(0, 0)
            logging.debug("[turn %04d] Could not find %4d\t\t=> %s", turn, spoken[1], spoken)
    spoken.reverse()
    logging.info("List has %d numbers, the last one is %d", len(spoken), spoken[-1])


def part2():
    args = cli(extra_args=[
        (
            ("--limit", "-l"),
            {"type": int, "default": 2020},
        ),
    ])

    numbers = [int(x) for x in args.infile.readline().split(",")]

    ages = {num: idx + 1 for idx, num in enumerate(numbers[:-1])}

    previous_turn = numbers[-1]
    for turn in range(len(numbers) + 1, args.limit + 1):
        if turn % 100000 == 0:
            logging.info("Reached turn %d", turn)
        last_seen = ages.get(previous_turn, 0)
        if last_seen == 0:
            spoken = 0
        else:
            spoken = turn - 1 - last_seen

        logging.debug("turn %04d => last: %4d -> spoken %04d", turn, previous_turn, spoken)
        ages[previous_turn] = turn - 1
        previous_turn = spoken

    logging.info("The last spoken number was %d on turn %d", previous_turn, turn)
