from collections import Counter
import logging
from math import prod
import re

from py2020.cli import cli


def make_cmp(v1, v2, v3, v4):
    def cmp(x):
        return (v1 <= x <= v2) or (v3 <= x <= v4)
    return cmp

def parse_ranges(raw):
    ranges = {}
    pattern = re.compile(r"^(.+): ([0-9]+)-([0-9]+) or ([0-9]+)-([0-9]+)")

    for line in raw.split("\n"):
        ret = pattern.match(line)
        ranges[ret.group(1)] = ((int(ret.group(2)), int(ret.group(3))), (int(ret.group(4)), int(ret.group(5))),
                                make_cmp(int(ret.group(2)), int(ret.group(3)), int(ret.group(4)), int(ret.group(5))))
    return ranges

def read_tickets(raw):
    tickets = []
    for line in raw.strip().split("\n")[1:]:
        tickets.append([int(x) for x in line.split(",")])
    return tickets

def invalid_sum(ticket, ranges):
    invalid = 0
    for num in ticket:
        if not any(r[2](num) for r in ranges.values()):
            invalid += num
    return invalid

def parse_input(infile):
    indata = infile.read()
    groups = indata.split("\n\n")

    ranges = parse_ranges(groups[0])
    ticket = read_tickets(groups[1])[0]
    other_tickets = read_tickets(groups[2])

    return (ranges, ticket, other_tickets)

def part1():
    args = cli()
    ranges, ticket, other_tickets = parse_input(args.infile)
    result = sum(map(lambda x: invalid_sum(x, ranges), other_tickets))
    logging.info(result)

def find_ticket_options(ticket, ranges):

    options = []

    for num in ticket:
        options.append(set(name
                           for name, values in ranges.items()
                           if values[2](num)))
    return options

def find_sole_option(possibles):

    for idx, names in enumerate(possibles):
        if len(names) == 1:
            return idx, names.pop()
    raise ValueError

def deduct_positions(possibles):
    positions = []

    while len(positions) < len(possibles):

        pos, name = find_sole_option(possibles)
        positions.append((pos, name))

        for options in possibles:
            options.discard(name)

    return [x for _, x in sorted(positions)]

def part2():
    args = cli()
    ranges, my_ticket, other_tickets = parse_input(args.infile)

    possibles = [set(ranges.keys()) for _ in range(len(my_ticket))]

    print(possibles)
    for ticket in other_tickets:
        if invalid_sum(ticket, ranges) != 0:
            continue

        possibles = [x & y for x, y in zip(possibles, find_ticket_options(ticket, ranges))]
        logging.debug("ticket %s => %s", ticket, possibles)

    positions = deduct_positions(possibles)
    logging.info("fields: %s", positions)
    result = prod(x for x, n in zip(my_ticket, positions) if n.startswith("departure"))
    logging.info("result: %d", result)
