from itertools import tee
import logging

from py2021.cli import cli

# from python doc, itertools.pairwise is only available in python 3.10, I'm
# currently running 3.9 :(
def pairwise(iterable):
    # pairwise('ABCDEFG') --> AB BC CD DE EF FG
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

def count_increases(values):
    return sum(int(left < right) for left, right in pairwise(values))

def make_slices(values):

    values = tee(values, 3)

    next(values[1])
    next(values[2])
    next(values[2])
    return map(sum, zip(*values))

def part1():
    args = cli()
    logging.info("Increasing steps: %d", count_increases(map(int, args.infile)))

def part2():
    args = cli()
    logging.info("Increasing steps: %d", count_increases(make_slices(map(int, args.infile))))
