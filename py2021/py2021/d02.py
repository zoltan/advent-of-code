from collections import Counter
import logging

from py2021.cli import cli


def part1():
    args = cli()
    directions = Counter()

    for entry in args.infile:
        direction, steps = entry.split()
        directions[direction] += int(steps)

    logging.info("Directions: %s => %d",
                 directions,
                 directions["forward"] * (directions["down"] - directions["up"]))

def part2():
    args = cli()

    aim = 0
    horizontal = 0
    depth = 0
    for entry in args.infile:
        direction, step = entry.split()
        step = int(step)
        if direction == "up":
            aim -= step
        if direction == "down":
            aim += step
        if direction == "forward":
            depth += step * aim
            horizontal += step
        logging.debug("%s/%d => {horizontal: %d, depth: %d, aim: %d}",
                      direction, step, horizontal, depth, aim)
    logging.info("Result: %d", horizontal * depth)
