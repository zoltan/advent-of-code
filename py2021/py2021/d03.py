from collections import Counter
from itertools import chain, count
import logging

from py2021.cli import cli


def part1():
    args = cli()

    bits_counter = Counter()
    for total, line in enumerate(args.infile):
        bits_counter.update(dict(zip(count(),
                                     map(int, line.rstrip()))))

    logging.debug("Total readings: %d - Final counter: %s", total, bits_counter)

    gamma = 0
    epsilon = 0
    total_keys = len(bits_counter.keys()) - 1
    for pos in sorted(bits_counter.keys()):
        if bits_counter[pos] > total - bits_counter[pos]:
            gamma += 1 << (total_keys - pos)
        else:
            epsilon += 1 << (total_keys - pos)
        logging.debug(f"Gamma {gamma:012b}, Epsilon: {epsilon:012b}")

    logging.info("Gamma: %d, Epsilon: %d, Result: %d", gamma, epsilon, gamma * epsilon)


def process_readings(in_values, offset, selector_f):

    if len(in_values) == 1:
        return in_values[0]
    logging.debug("Offset: %d -> values: %s", offset, in_values)

    mask = 1 << offset
    bits = Counter(map(lambda x: x & mask, in_values))
    most_common = selector_f(bits[0], bits[mask])

    return process_readings(
        list(filter(lambda x: bool(x & mask) == most_common, in_values)),
        offset - 1,
        selector_f,
    )


def part2():
    args = cli()

    first_row = args.infile.readline()
    start_offset = len(first_row.rstrip()) - 1

    in_values = [int(x, 2) for x in chain([first_row, ], args.infile)]

    oxygen = process_readings(in_values, start_offset,
                              lambda z, nz: int(nz >= z))
    co2 = process_readings(in_values, start_offset,
                           lambda z, nz: int(nz < z))
    logging.info("Oxygen: %d, CO2: %d, results: %d", oxygen, co2, oxygen * co2)
