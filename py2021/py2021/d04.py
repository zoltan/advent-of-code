from collections import Counter
from itertools import chain
import logging

from py2021.cli import cli



class Board:

    def __init__(self, values):
        assert len(values) == 5
        assert len(values[0]) == 5

        logging.debug("Building board with values: %s", values)

        self.is_complete = False
        self.values = tuple(chain.from_iterable(values))
        self.drawn = []
        self.v_crossed = Counter()
        self.h_crossed = Counter()


    def add_input(self, value):

        try:
            pos = self.values.index(value)
        except ValueError:
            return

        self.drawn.append(value)
        row = pos // 5
        col = pos % 5

        self.h_crossed[row] += 1
        self.v_crossed[col] += 1

        if self.h_crossed[row] == 5 or self.v_crossed[col] == 5:
            self.is_complete = True
        return self.is_complete

    def get_result(self):

        return sum(set(self.values) - set(self.drawn)) * self.drawn[-1]


def read_boards(infile):

    boards = []
    buffer = []

    for line in infile:
        line = line.rstrip()
        if line:
            buffer.append(tuple(map(int, line.split())))
        elif buffer:
            boards.append(Board(buffer))
            buffer = []

    if buffer:
        boards.append(Board(buffer))

    return boards

def part1():
    args = cli()

    draw_numbers = tuple(map(int, args.infile.readline().split(",")))
    # test if all draws numbers are unique, helps with board implementation
    assert len(draw_numbers) == len(set(draw_numbers))
    logging.debug("Numbers to draw: %s", draw_numbers)
    boards = read_boards(args.infile)

    for num in draw_numbers:
        for idx, board in enumerate(boards):
            complete = board.add_input(num)
            if complete:
                logging.info("Number %d completed board %d, result is %d", num, idx, board.get_result())
                return

def part2():
    args = cli()

    draw_numbers = tuple(map(int, args.infile.readline().split(",")))
    # test if all draws numbers are unique, helps with board implementation
    assert len(draw_numbers) == len(set(draw_numbers))
    logging.debug("Numbers to draw: %s", draw_numbers)
    boards = read_boards(args.infile)

    for num in draw_numbers:
        for idx, board in enumerate(boards):
            if board.is_complete:
                continue
            complete = board.add_input(num)
            if complete:
                logging.info("Number %d completed board %d, result is %d", num, idx, board.get_result())
