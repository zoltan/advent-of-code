from collections import Counter
import logging
import re

from py2021.cli import cli


class Segment:

    def __init__(self, coords):
        self.point_1 = coords[:2]
        self.point_2 = coords[2:]
        if self.point_2[0] != self.point_1[0]:
            self.coeff = (self.point_2[1] - self.point_1[1]) // (self.point_2[0] - self.point_1[0])
            self.offset = self.point_1[1] - self.coeff * self.point_1[0]
        else:
            self.coeff = self.point_1[0]
            self.offset = None
        self.x_minmax = (min(self.point_1[0], self.point_2[0]),
                         max(self.point_1[0], self.point_2[0]))
        self.y_minmax = (min(self.point_1[1], self.point_2[1]),
                         max(self.point_1[1], self.point_2[1]))

    def __str__(self):
        coords = f"{self.point_1} -> {self.point_2} #> "

        if self.offset:
            return f"[{coords} y = {self.coeff} * x + {self.offset}]"
        else:
            return f"[{coords} x = {self.coeff}]"

    def walk(self):
        if self.offset is not None:
            return ((x, self.coeff * x + self.offset)
                    for x in range(self.x_minmax[0], self.x_minmax[1] + 1))

        return ((self.coeff, y)
                for y in range(self.y_minmax[0], self.y_minmax[1] + 1))

def read_coords(infile):
    pattern = re.compile(r"^(\d+),(\d+) -> (\d+),(\d+)$")

    for line in infile:
        ret = pattern.match(line)
        if not ret:
            logging.error("Can't parse: %s", line.rstrip())
            raise ValueError(line)
        yield Segment(tuple(map(int, ret.groups())))

def part1():
    args = cli()

    coords = filter(lambda s: (s.point_1[0] == s.point_2[0]) or (s.point_1[1] == s.point_2[1]),
                    read_coords(args.infile))

    results = Counter()
    for segment in coords:
        results.update(Counter(segment.walk()))

    logging.info("Found %d intersections",
                 len(list(filter(lambda x: x > 1, results.values()))))

def part2():
    args = cli()

    coords = read_coords(args.infile)
    results = Counter()
    for segment in coords:
        results.update(Counter(segment.walk()))

    logging.info("Found %d intersections",
                 len(list(filter(lambda x: x > 1, results.values()))))
