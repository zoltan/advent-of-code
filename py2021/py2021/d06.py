from collections import Counter
from functools import lru_cache
import logging

from py2021.cli import cli


@lru_cache
def count_children(value, days_left):
    logging.debug("Counting from %d with %d days left", value, days_left)

    if days_left <= value:
        return 0

    children = 0
    for idx in range(0, days_left // 7 + 1):
        child_at = days_left - (7 * idx + value) - 1
        if child_at >= 0:
            logging.debug("New child with %d days left", child_at)
            children += 1 + count_children(6, child_at - 2)
    return children

def main(defaut_cycles):
    args = cli([
        (("--cycles", "-c"), {"type": int, "default": defaut_cycles}),
    ])

    in_values = Counter(map(int, next(args.infile).split(",")))
    logging.info("Result: %d",
                 sum(occurs * (1 + count_children(value, args.cycles))
                     for value, occurs in in_values.items()))

def part1():
    main(80)

def part2():
    main(256)
