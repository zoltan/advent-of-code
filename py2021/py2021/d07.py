from collections import Counter
from functools import reduce
from itertools import accumulate
import logging

from py2021.cli import cli


def gen_distances(coords, func):

    max_pos = max(coords)
    for pos, occurs in coords.items():
        logging.debug("Generating for pos %d, %d times", pos, occurs)
        yield [func(abs(idx - pos)) * occurs for idx in range(1, max_pos + 1)]

def dist_p1(value):
    return value

def dist_p2(max_val):
    costs = list(accumulate(range(max_val + 1)))
    def inner(value):
        return costs[value]
    return inner

def part1():
    args = cli()

    coords = Counter(map(int, next(args.infile).split(",")))
    max_pos = max(coords)
    logging.debug("Max coords: %d", max_pos)

    result = reduce(lambda x, y: map(sum, zip(x, y)),
                    gen_distances(coords, dist_p2(max_pos)))
    logging.info("Result: %d", min(result))

def part2():
    args = cli()

    coords = Counter(map(int, next(args.infile).split(",")))
    max_pos = max(coords)
    logging.debug("Max coords: %d", max_pos)

    result = reduce(lambda x, y: map(sum, zip(x, y)),
                    gen_distances(coords, dist_p2(max_pos)))
    logging.info("Result: %d", min(result))
