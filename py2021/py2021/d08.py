from collections import Counter
from itertools import chain
import logging

from py2021.cli import cli


def part1():
    args = cli()

    ret = list(filter(lambda x: x in (2, 3, 4, 7),
                     map(len, chain.from_iterable((x.split("|")[1].split() for x in args.infile)))))

    logging.info("Result: %d", len(ret))

def part2():
    args = cli()
