from collections import Counter
from itertools import product
import logging

from py2021.cli import cli


class HeightMap:

    def __init__(self, infile):

        self._map = [list(map(int, line.strip())) for line in infile]
        self.height = len(self._map)
        self.width = len(self._map[0])

    def get_surroundings_coords(self, coords):

        def is_valid(_coords):
            return (0 <= _coords[0] < self.width) and (0 <= _coords[1] < self.height)

        pos_x, pos_y = coords
        assert 0 <= pos_x < self.width
        assert 0 <= pos_y < self.height

        return filter(is_valid, (
            (pos_x - 1, pos_y),
            (pos_x + 1, pos_y),
            (pos_x, pos_y - 1),
            (pos_x, pos_y + 1),
        ))

    def __getitem__(self, coords):
        pos_x, pos_y = coords
        return self._map[pos_y][pos_x]

    def is_low_point(self, coords):
        return self[coords] < min(map(self.__getitem__,
                                      self.get_surroundings_coords(coords)))

    def get_bassin_members(self, coords, visited=None):

        if self[coords] == 9:
            return visited

        if visited is None:
            visited = {}

        visited[coords] = True
        for neighbour in self.get_surroundings_coords(coords):
            if neighbour not in visited:
                self.get_bassin_members(neighbour, visited)
        return visited

    def print_map(self):

        red = "\033[0;31m"
        cyan = "\033[0;36m"
        default = "\033[0m"

        low_point_coords = list(filter(self.is_low_point,
                                       product(range(self.width),
                                               range(self.height))))

        for y in range(self.height):
            line = []
            for x in range(self.width):
                val = self[(x, y)]
                if val == 9:
                    line.append(f"{red}9{default}")
                elif (x, y) in low_point_coords:
                    line.append(f"{cyan}{val}{default}")
                else:
                    line.append(" ")
            print("".join(line))


def part1():
    args = cli()

    height_map = HeightMap(args.infile)

    low_point_coords = filter(height_map.is_low_point,
                              product(range(height_map.width),
                                      range(height_map.height)))
    result = sum(map(lambda x: height_map[x] + 1, low_point_coords))

    logging.info("Result: %d", result)


def part2():
    args = cli()
    height_map = HeightMap(args.infile)
    height_map.print_map()

    low_point_coords = filter(height_map.is_low_point,
                              product(range(height_map.width),
                                      range(height_map.height)))


    sizes = [len(height_map.get_bassin_members(coords)) for coords in low_point_coords]
    sizes.sort(reverse=True)
    logging.info("Results: %d", sizes[0] * sizes[1] * sizes[2])
