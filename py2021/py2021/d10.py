from collections import Counter
from itertools import product
import logging

from py2021.cli import cli


def is_match(left, right):
    valid_pairs = (
        ("(", ")"),
        ("{", "}"),
        ("[", "]"),
        ("<", ">"),
    )
    logging.debug("Matching '%s' and '%s'", left, right)
    return (left, right) in valid_pairs

def line_score(line):

    points = {
        ")": 3,
        "]": 57,
        "}": 1197,
        ">": 25137,
    }


def part1():
    args = cli()

    print(line_score(['<', '(', ')', '{', '}', '>']))
    return

    # lines = (list(x.rstrip()) for x in args.infile)
    # for x in lines:
    #     print(x, line_score(x))

def part2():
    args = cli()
