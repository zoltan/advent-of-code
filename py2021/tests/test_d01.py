from py2021.d01 import count_increases, make_slices


def test_count_increases():

    in_values = iter([
        199,
        200,
        208,
        210,
        200,
        207,
        240,
        269,
        260,
        263,
    ])

    assert count_increases(in_values) == 7

def test_make_slices():
    in_values = iter([
        199,
        200,
        208,
        210,
        200,
        207,
        240,
        269,
        260,
        263,
    ])

    expected = [607, 618, 618, 617, 647, 716, 769, 792]

    assert list(make_slices(in_values)) == expected
