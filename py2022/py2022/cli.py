from argparse import ArgumentParser, FileType
from logging import basicConfig, info


def cli(extra_args=None):
    parser = ArgumentParser()
    parser.add_argument("--verbose", "-v", action="store_true")
    parser.add_argument("infile", type=FileType("r"))

    if extra_args is not None:
        for arg, kwarg in extra_args:
            parser.add_argument(*arg, **kwarg)

    args = parser.parse_args()

    if args.verbose:
        basicConfig(level="DEBUG")
    else:
        basicConfig(level="INFO")
    info("Reading file %s", args.infile.name)

    return args
