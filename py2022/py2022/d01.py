import logging
from io import TextIOWrapper
from itertools import islice

from py2022.cli import cli


def read_input(infile: TextIOWrapper) -> list[int]:
    cals = []
    current = 0
    for line in infile:
        line = line.strip()
        if not line:
            cals.append(current)
            current = 0
            continue
        current += int(line)
    if current:
        cals.append(current)
    return cals


def part1():
    args = cli()
    calories = read_input(args.infile)
    logging.info("Max calories %d", max(calories))


def part2():
    args = cli()
    calories = read_input(args.infile)
    logging.info(
        "3 biggest calories %d",
        sum(islice(sorted(calories, reverse=True), 3)),
    )
