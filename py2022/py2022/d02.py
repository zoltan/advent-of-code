import logging
from enum import IntEnum, auto

from py2022.cli import cli


class HandShape(IntEnum):
    ROCK = auto()
    PAPER = auto()
    SCISSORS = auto()


class Outcome(IntEnum):
    LOSS = 0
    DRAW = 1
    WIN = 2


def get_outcome(other: HandShape, me: HandShape) -> Outcome:

    if other == me:
        return Outcome.DRAW
    elif me.value == other.value + 1 or (
        me == HandShape.ROCK and other == HandShape.SCISSORS
    ):
        return Outcome.WIN
    else:
        return Outcome.LOSS


def get_handshape(shape: HandShape, outcome: Outcome):

    if outcome == Outcome.DRAW:
        return shape
    if outcome == Outcome.WIN:
        value = shape.value + 1
        if value > len(HandShape):
            value = 1
        return HandShape(value)
    if outcome == Outcome.LOSS:
        value = shape.value - 1
        if value == 0:
            value = len(HandShape)
        return HandShape(value)


def read_line_p1(line: str) -> tuple[HandShape, HandShape]:
    parts = line.split(" ")
    other = HandShape(ord(parts[0]) - ord("A") + 1)
    me = HandShape(ord(parts[1]) - ord("X") + 1)
    return (other, me)


def read_line_p2(line: str) -> tuple[HandShape, Outcome]:
    parts = line.split(" ")
    other = HandShape(ord(parts[0]) - ord("A") + 1)
    outcome = Outcome(ord(parts[1]) - ord("X"))
    return (other, outcome)


def get_score(outcome: Outcome, shape: HandShape) -> int:
    return outcome * 3 + shape


def part1():
    args = cli()
    score = 0
    for line in args.infile:
        plays = read_line_p1(line.strip())
        outcome = get_outcome(*plays)
        score += get_score(outcome, plays[1])
    logging.info("Score: %d", score)


def part2():
    args = cli()
    score = 0
    for line in args.infile:
        other, outcome = read_line_p2(line.strip())
        me = get_handshape(other, outcome)
        score += get_score(outcome, me)
    logging.info("Score: %d", score)
