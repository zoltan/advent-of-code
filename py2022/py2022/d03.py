from collections.abc import Generator
from io import TextIOWrapper

from py2022.cli import cli


def get_common_1(items: str) -> "str":
    middle = len(items) // 2
    left = set(items[:middle])
    right = set(items[middle:])
    inter = left.intersection(right)
    assert inter
    return inter.pop()


def get_priority(item: str) -> int:
    if "a" <= item <= "z":
        return ord(item) - ord("a") + 1
    return ord(item) - ord("A") + 27


def readline1(line: str) -> int:
    common = get_common_1(line.strip())
    prio = get_priority(common)
    return prio


def readfile2(infile: TextIOWrapper) -> Generator[tuple[str, str, str], None, None]:
    try:
        while True:
            yield (
                next(infile).strip(),
                next(infile).strip(),
                next(infile).strip(),
            )
    except StopIteration:
        return


def get_common_2(bags: tuple[str, str, str]) -> str:
    return set(bags[0]).intersection(set(bags[1])).intersection(bags[2]).pop()


def part1():
    args = cli()
    print(sum(map(readline1, args.infile)))


def part2():
    args = cli()
    print(sum(get_priority(get_common_2(x)) for x in readfile2(args.infile)))
