from py2022.cli import cli


class Range:

    def __init__(self, instr):
        x = instr.split("-")
        self.low = int(x[0])
        self.high = int(x[1])

    def __contains__(self, other: "Range") -> bool:
        return self.low <= other.low and self.high >= other.high


class Range2(Range):
    def __contains__(self, other: "Range") -> bool:
        return self.high >= other.low


def parse_line1(line: str) -> tuple[Range, Range]:
    left, right = line.strip().split(",")
    return (
        Range(left),
        Range(right)
    )


def parse_line2(line: str) -> tuple[Range, Range]:
    left, right = line.strip().split(",")
    return (
        Range2(left),
        Range2(right)
    )


def part1():
    args = cli()
    print(sum(1 for x, y in map(parse_line1, args.infile) if x in y or y in x))


def part2():
    args = cli()
    print(sum(1 for x, y in map(parse_line2, args.infile) if x in y and y in x))
