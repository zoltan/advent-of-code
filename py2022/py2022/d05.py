from collections.abc import Generator
from io import TextIOWrapper
import logging
import re

from py2022.cli import cli

Stack = list[str]
Move = tuple[int, int, int]

move_re = re.compile(r"move (\d+) from (\d+) to (\d+)")


def read_stacks(infile: TextIOWrapper) -> list[Stack]:
    line = next(infile)
    if line.startswith(" 1 "):
        nb_stack = int(line.split()[-1])
        return [[] for _ in range(nb_stack)]
    stacks = read_stacks(infile)
    for pos, stack in enumerate(stacks):
        offset = 4 * pos + 1
        char = None
        if len(line) > offset:
            char = line[offset]
        if char is not None and char != " ":
            stack.append(char)
    return stacks


def read_moves(infile: TextIOWrapper) -> Generator[Move, None, None]:
    for line in infile:
        groups = move_re.match(line)
        assert groups is not None
        yield (
            int(groups.group(1)),
            int(groups.group(2)),
            int(groups.group(3)),
        )


def move_p1(stacks: list[Stack], move: Move) -> None:
    amount, src, dst = move
    for _ in range(amount):
        stacks[dst - 1].append(stacks[src - 1].pop())


def move_p2(stacks: list[Stack], move: Move) -> None:
    amount, src, dst = move
    logging.debug("Move: %s", move)
    stacks[dst - 1].extend(stacks[src - 1][-amount:])
    del stacks[src - 1][-amount:]
    logging.debug("Stacks out: %s", stacks)


def part1():
    args = cli()
    stacks = read_stacks(args.infile)
    next(args.infile)
    [move_p1(stacks, x) for x in read_moves(args.infile)]
    logging.info("Result: %s", "".join(x[-1] for x in stacks if x))


def part2():
    args = cli()
    stacks = read_stacks(args.infile)
    next(args.infile)
    [move_p2(stacks, x) for x in read_moves(args.infile)]
    logging.info("Result: %s", "".join(x[-1] for x in stacks if x))
