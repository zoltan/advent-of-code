import logging
from collections.abc import Generator
from io import TextIOWrapper

from py2022.cli import cli


def read_dirsizes(infile: TextIOWrapper, current_path: str) -> list[tuple[str, int]]:
    logging.debug("reading folder %s", current_path)
    size = 0
    results = []
    for line in infile:
        line = line.strip()
        if line.startswith("$ cd"):
            if line == "$ cd ..":
                break
            target = line.split()[-1]
            dirsize = read_dirsizes(infile, f"{current_path}/{target}")
            size += dirsize[-1][1]
            results.extend(dirsize)
        elif line == "$ ls" or line.startswith("dir "):
            continue
        else:
            file_size, _ = line.split()
            size += int(file_size)
    results.append((current_path, size))
    return results


def part1():
    args = cli()
    next(args.infile)
    dirsizes = read_dirsizes(args.infile, "/")
    logging.info(
        "result: %d", sum(filter(lambda x: x <= 100_000, map(lambda x: x[1], dirsizes)))
    )


def part2():
    args = cli()
    next(args.infile)
    dirsizes = read_dirsizes(args.infile, "/")
    disk_size = 70_000_000
    freespace = disk_size - dirsizes[-1][1]
    missing = 30_000_000 - freespace
    logging.debug("Freespace: %d", freespace)
    logging.debug("Missing: %d", missing)
    logging.info("Result: %d", min(filter(lambda x: x >= missing, map(lambda x: x[1], dirsizes))))
