import logging
from io import TextIOWrapper
from itertools import product, takewhile

from py2022.cli import cli

Coords = tuple[int, int]
Map = dict[Coords, int]


def make_map(infile: TextIOWrapper) -> tuple[int, Map]:
    treemap = {}
    for nrow, line in enumerate(infile):
        line = line.strip()
        for nchar, char in enumerate(line):
            treemap[(nchar, nrow)] = int(char)
    return nrow + 1, treemap


def is_visible(pos: Coords, treemap: Map, size: int) -> bool:
    tree = treemap[pos]
    left = max(treemap[(x, pos[1])] for x in range(0, pos[0]))
    right = max(treemap[(x, pos[1])] for x in range(pos[0] + 1, size))

    top = max(treemap[(pos[0], x)] for x in range(0, pos[1]))
    bottom = max(treemap[(pos[0], x)] for x in range(pos[1] + 1, size))

    visible = tree > left or tree > right or tree > top or tree > bottom
    logging.debug(
        "Tree at %s %d -> l:%d t:%d r:%d b:%d -> %s",
        pos,
        tree,
        left,
        top,
        right,
        bottom,
        visible,
    )
    return visible


def view_score(pos: Coords, treemap: Map, size: int) -> int:
    tree = treemap[pos]

    # I don't like black...
    left = min(pos[0],
               1 + sum(1 for _ in takewhile(
                   lambda v: v < tree,
                   (treemap[(x, pos[1])] for x in range(pos[0] - 1, -1, -1)),
               )))

    right = min(
        size - 1 - pos[0],
        1
        + sum(
            1
            for _ in takewhile(
                lambda v: v < tree,
                (treemap[(x, pos[1])] for x in range(pos[0] + 1, size)),
            )
        ),
    )

    top = min(
        pos[1],
        1
        + sum(
            1
            for _ in takewhile(
                lambda v: v < tree,
                (treemap[(pos[0], x)] for x in range(pos[1] - 1, -1, -1)),
            )
        ),
    )
    bottom = min(
        size - 1 - pos[1],
        1
        + sum(
            1
            for _ in takewhile(
                lambda v: v < tree,
                (treemap[(pos[0], x)] for x in range(pos[1] + 1, size)),
            )
        ),
    )

    score = left * right * top * bottom

    logging.debug(
        "Tree at %s %d -> l:%d t:%d r:%d b:%d -> %d",
        pos,
        tree,
        left,
        top,
        right,
        bottom,
        score,
    )
    return score


def part1():
    args = cli()
    size, treemap = make_map(args.infile)

    visibles = sum(
        1
        for y, x in product(range(1, size - 1), range(1, size - 1))
        if is_visible((x, y), treemap, size)
    )

    visibles += (size - 1) * 4
    logging.info("Visible trees: %d", visibles)


def part2():
    args = cli()
    size, treemap = make_map(args.infile)
    logging.info(
        "Best viewing score: %d",
        max(
            view_score((x, y), treemap, size)
            for y, x in product(range(1, size - 1), range(1, size - 1))
        ),
    )
