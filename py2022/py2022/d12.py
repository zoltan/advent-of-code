import logging
from collections.abc import Generator
from io import TextIOWrapper

from py2022.cli import cli

Coords = tuple[int, int]
Map = dict[Coords, int]


def read_map(infile: TextIOWrapper) -> tuple[Coords, Coords, Map]:
    start = None
    end = None
    height_map = {}
    for y, line in enumerate(infile):
        line = line.strip()
        for x, value in enumerate(line):
            if value == "S":
                start = (x, y)
                height_map[start] = 0
            elif value == "E":
                end = (x, y)
                height_map[end] = ord("z") - ord("a")
            else:
                height_map[(x, y)] = ord(value) - ord("a")

    assert start is not None
    assert end is not None

    return start, end, height_map


def neighbours(points: set[Coords], hmap: Map) -> Generator[Coords, None, None]:
    for x, y in points:
        logging.debug("Looking for neighbours of %s", (x, y))
        current = hmap[(x, y)]

        right = hmap.get((x + 1, y))
        if right is not None and right <= current + 1:
            yield (x + 1, y)
        left = hmap.get((x - 1, y))
        if left is not None and left <= current + 1:
            yield (x - 1, y)

        up = hmap.get((x, y - 1))
        if up is not None and up <= current + 1:
            yield (x, y - 1)
        down = hmap.get((x, y + 1))
        if down is not None and down <= current + 1:
            yield (x, y + 1)


def search1(start: Coords, end: Coords, hmap: Map) -> int:

    known: dict[Coords, int] = {start: 0}
    pending = set((start, ))
    score = 1

    while True:
        next_pending: set[Coords] = set()

        logging.debug("Computing score %d", score)
        for point in neighbours(pending, hmap):
            logging.debug("inspecting %s", point)

            if point == end:
                return score
            if point in known:
                logging.debug(
                    "Point %s is already known with a score of %d (current score is %d)",
                    point, known[point], score
                )
                continue
            logging.debug("Saving point %s with score %d", point, score)
            known[point] = score
            next_pending.add(point)

        score += 1
        pending = next_pending
        if not pending:
            return -1


def part1():
    args = cli()
    start, end, hmap = read_map(args.infile)
    logging.info("Going from %s to %s", start, end)
    score = search1(start, end, hmap)
    logging.info("Score is %d", score)


def part2():
    args = cli()
    _, end, hmap = read_map(args.infile)

    score = min(
        filter(
            lambda x: x > 0,
            map(lambda x: search1(x, end, hmap), (x for x, y in hmap.items() if y == 0))
        )
    )
    logging.info("Score is %d", score)
