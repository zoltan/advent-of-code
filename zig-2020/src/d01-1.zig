const std = @import("std");

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    var infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();
    var buffer: [16:0]u8 = undefined;

    var fbs = std.io.fixedBufferStream(&buffer);
    var nums = std.ArrayList(u64).init(allocator);

    loop: while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break :loop;
        };
        var num = try std.fmt.parseInt(u64, fbs.getWritten(), 10);
        try nums.append(num);
        fbs.reset();
    }

    loop: for (nums.items, 0..) |left, i| {
        for (nums.items[(i + 1)..]) |right| {
            if (left + right == 2020) {
                std.debug.print(">> {} - {} => sum: {}, mul: {}\n", .{ left, right, left + right, left * right });
                break :loop;
            }
        }
    }
}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
