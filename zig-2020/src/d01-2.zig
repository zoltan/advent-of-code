const std = @import("std");

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    var infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();
    var buffer: [16:0]u8 = undefined;

    var fbs = std.io.fixedBufferStream(&buffer);
    var nums = std.ArrayList(u64).init(allocator);

    loop: while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break :loop;
        };
        var num = try std.fmt.parseInt(u64, fbs.getWritten(), 10);
        try nums.append(num);
        fbs.reset();
    }

    loop: for (nums.items, 0..) |left, i| {
        for (nums.items[(i + 1)..], i..) |middle, j| {
            for (nums.items[(j + 1)..]) |right| {
                if (left + middle + right == 2020) {
                    std.debug.print(">> {} - {} - {} => sum: {}, mul: {}\n", .{ left, middle, right, left + middle + right, left * middle * right });
                    break :loop;
                }
            }
        }
    }
}
