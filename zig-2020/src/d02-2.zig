const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
    WrongLine,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    var infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn isValidPassword(low: u8, high: u8, char: u8, password: []const u8) bool {
    return (@intFromBool(password[low - 1] == char) ^ @intFromBool(password[high - 1] == char)) != 0;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();
    _ = allocator;

    var reader = try getReader();
    var buffer: [256:0]u8 = undefined;

    var fbs = std.io.fixedBufferStream(&buffer);
    var counter: u16 = 0;

    loop: while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break :loop;
        };
        var line = fbs.getWritten();

        var sep1 = std.mem.indexOf(u8, line, "-") orelse {
            return AOCError.WrongLine;
        };
        var sep2 = std.mem.indexOf(u8, line[sep1..], " ") orelse {
            return AOCError.WrongLine;
        };
        var left = try std.fmt.parseInt(u8, line[0..sep1], 10);
        var right = try std.fmt.parseInt(u8, line[(sep1 + 1)..(sep1 + sep2)], 10);
        var char = line[sep1 + sep2 + 1];
        var password = line[(sep1 + sep2 + 4)..];

        // std.debug.print("[{s}] => {d} {d} {c} [{s}]\n", .{ line, left, right, char, password });

        if (isValidPassword(left, right, char, password)) {
            counter += 1;
        }

        fbs.reset();
    }

    std.debug.print("Valid passwords: {d}\n", .{counter});
}

test "example" {
    try expect(isValidPassword(1, 3, 'a', "abcde") == true);
    try expect(isValidPassword(1, 3, 'b', "cdefg") == false);
    try expect(isValidPassword(2, 9, 'c', "ccccccccc") == false);
}
