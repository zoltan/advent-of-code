const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn readNumPart1(str: []const u8) u8 {
    var left: u8 = undefined;
    var right: u8 = undefined;
    var i: usize = 0;

    while (i < str.len) : (i += 1) {
        const char = str[i];
        if (char >= '0' and char <= '9') {
            left = char - '0';
            break;
        }
    }

    i = str.len - 1;
    while (i >= 0) : (i -= 1) {
        const char = str[i];
        if (char >= '0' and char <= '9') {
            right = char - '0';
            break;
        }
    }
    return left * 10 + right;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();
    _ = allocator;

    var reader = try getReader();
    var buffer: [512:0]u8 = undefined;

    var fbs = std.io.fixedBufferStream(&buffer);
    var total: u64 = 0;

    loop: while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break :loop;
        };
        const line = fbs.getWritten();
        const num = readNumPart1(line);
        std.debug.print("[{s}] => {}\n", .{ line, num });
        total += num;
        fbs.reset();
    }
    std.debug.print("Total: {}\n", .{total});
}

test "num-part1" {
    try expect(readNumPart1("1abc2") == 12);
    try expect(readNumPart1("pqr3stu8vwx") == 38);
    try expect(readNumPart1("a1b2c3d4e5f") == 15);
    try expect(readNumPart1("treb7uchet") == 77);
}
