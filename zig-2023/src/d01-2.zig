const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn findDigit(str: []const u8) ?u8 {
    const litterals = .{ "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
    if (str[0] >= '0' and str[0] <= '9') {
        return str[0] - '0';
    }
    inline for (litterals, 1..) |name, val| {
        if (std.mem.startsWith(u8, str, name)) {
            return @intCast(val);
        }
    }
    return null;
}

fn readNum(str: []const u8) u8 {
    var i: usize = 0;

    const left = while (i < str.len) : (i += 1) {
        if (findDigit(str[i..str.len])) |val| {
            break val;
        }
    } else unreachable;

    i = str.len - 1;
    const right = while (i >= 0) : (i -= 1) {
        if (findDigit(str[i..str.len])) |val| {
            break val;
        }
    } else unreachable;
    return left * 10 + right;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();
    _ = allocator;

    var reader = try getReader();
    var buffer: [512:0]u8 = undefined;

    var fbs = std.io.fixedBufferStream(&buffer);
    var total: u64 = 0;

    while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        const line = fbs.getWritten();
        const num = readNum(line);
        std.debug.print("[{s}] => {}\n", .{ line, num });
        total += num;
        fbs.reset();
    }
    std.debug.print("Total: {}\n", .{total});
}

test "example" {
    try expect(readNum("two1nine") == 29);
    try expect(readNum("eightwothree") == 83);
    try expect(readNum("abcone2threexyz") == 13);
    try expect(readNum("xtwone3four") == 24);
    try expect(readNum("4nineeightseven2") == 42);
    try expect(readNum("zoneight234") == 14);
    try expect(readNum("7pqrstsixteen") == 76);
}
