const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn gamePower(fullLine: []const u8) u64 {
    var i: usize = undefined;
    var num: u8 = undefined;
    var minRed: u64 = 0;
    var minGreen: u64 = 0;
    var minBlue: u64 = 0;

    var line = fullLine;
    i = std.mem.indexOf(u8, line, ":") orelse unreachable;
    // skip header "Game X: "
    line = line[i + 2 ..];
    while (true) {
        // find the first space char & read the number of cubes
        // the next letter is the color (R, G, B)
        // find the next , or ; and start over

        i = std.mem.indexOf(u8, line, " ") orelse unreachable;
        num = std.fmt.parseInt(u8, line[0..i], 10) catch {
            unreachable;
        };
        const offset: usize = switch (line[i + 1]) {
            'r' => blk: {
                minRed = @max(minRed, num);
                break :blk 3;
            },
            'g' => blk: {
                minGreen = @max(minGreen, num);
                break :blk 5;
            },
            'b' => blk: {
                minBlue = @max(minBlue, num);
                break :blk 4;
            },
            else => {
                unreachable;
            },
        };
        if (i + offset + 1 == line.len) {
            break;
        }
        line = line[i + offset + 3 ..];
    }
    std.debug.print("[r: {}, g: {}, b: {}] => {}\n", .{ minRed, minGreen, minBlue, minRed * minGreen * minBlue });
    return minRed * minGreen * minBlue;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();
    _ = allocator;

    var reader = try getReader();
    var buffer: [512:0]u8 = undefined;

    var fbs = std.io.fixedBufferStream(&buffer);
    var totalPower: u64 = 0;

    while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        const line = fbs.getWritten();
        totalPower += gamePower(line);
        fbs.reset();
    }
    std.debug.print("Total: {}\n", .{totalPower});
}

test "example" {
    try expect(gamePower("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green") == 48);
    try expect(gamePower("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue") == 12);
    try expect(gamePower("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red") == 1560);
    try expect(gamePower("Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red") == 630);
    try expect(gamePower("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green") == 36);
}
