const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Coords = struct {
    x: u64,
    y: u64,

    pub fn format(self: *const Coords, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "(x={}, y={})", .{ self.x, self.y });
    }

    pub fn nextTo(self: *const Coords, other: *const Coords) bool {
        var closeX: bool = false;
        var closeY: bool = false;

        if (other.x == 0) {
            closeX = self.x <= 1;
        } else {
            closeX = other.x - 1 <= self.x and self.x <= other.x + 1;
        }

        if (other.y == 0) {
            closeY = self.y <= 1;
        } else {
            closeY = other.y - 1 <= self.y and self.y <= other.y + 1;
        }
        return closeX and closeY;
    }
};

test "coords-next-to" {
    const coords = [_][2]Coords{
        [2]Coords{ Coords{ .x = 0, .y = 0 }, Coords{ .x = 0, .y = 1 } },
        [2]Coords{ Coords{ .x = 0, .y = 0 }, Coords{ .x = 1, .y = 0 } },
        [2]Coords{ Coords{ .x = 0, .y = 0 }, Coords{ .x = 1, .y = 1 } },

        [2]Coords{ Coords{ .x = 2, .y = 4 }, Coords{ .x = 3, .y = 4 } },
        [2]Coords{ Coords{ .x = 2, .y = 4 }, Coords{ .x = 3, .y = 3 } },
        [2]Coords{ Coords{ .x = 2, .y = 4 }, Coords{ .x = 1, .y = 3 } },
    };

    for (coords) |pair| {
        // std.debug.print("Testing {} with {}\n", .{ pair[0], pair[1] });
        try expect(pair[0].nextTo(&pair[1]));
        try expect(pair[1].nextTo(&pair[0]));
    }
}

test "coords-not-next-to" {
    const coords = [_][2]Coords{
        [2]Coords{ Coords{ .x = 0, .y = 0 }, Coords{ .x = 0, .y = 2 } },
        [2]Coords{ Coords{ .x = 0, .y = 0 }, Coords{ .x = 2, .y = 0 } },
        [2]Coords{ Coords{ .x = 0, .y = 0 }, Coords{ .x = 2, .y = 1 } },

        [2]Coords{ Coords{ .x = 2, .y = 4 }, Coords{ .x = 5, .y = 3 } },
        [2]Coords{ Coords{ .x = 2, .y = 4 }, Coords{ .x = 2, .y = 7 } },
    };

    for (coords) |pair| {
        // std.debug.print("Testing {} with {}\n", .{ pair[0], pair[1] });
        try expect(pair[0].nextTo(&pair[1]) == false);
        try expect(pair[1].nextTo(&pair[0]) == false);
    }
}

const NumPart = struct {
    num: u64,
    length: u64,
    coords: Coords,

    pub fn nextTo(self: *const NumPart, point: *const Coords) bool {
        if (self.coords.nextTo(point)) {
            return true;
        }
        for (1..self.length) |i| {
            if ((Coords{ .x = self.coords.x + i, .y = self.coords.y }).nextTo(point)) {
                return true;
            }
        }
        return false;
    }

    pub fn format(self: *const NumPart, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "NumPart({} ({}) at {})", .{ self.num, self.length, self.coords });
    }
};

const Symbol = struct {
    value: u8,
    coords: Coords,

    pub fn format(self: *const Symbol, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "Symbol('{c}' at {})", .{ self.value, self.coords });
    }
};

const Map = struct {
    numParts: std.ArrayList(NumPart),
    symbols: std.ArrayList(Symbol),

    pub fn init(allocator: std.mem.Allocator) Map {
        const map = Map{
            .numParts = std.ArrayList(NumPart).init(allocator),
            .symbols = std.ArrayList(Symbol).init(allocator),
        };
        return map;
    }

    pub fn deinit(self: *Map) void {
        self.numParts.deinit();
        self.symbols.deinit();
    }
};

fn parseLine(line: []const u8, linum: usize, map: *Map) !void {
    var offset: usize = 0;
    while (offset < line.len) {
        // std.debug.print(">> offset {d:03} [{s}]\n", .{ offset, line[offset..line.len] });
        var pos = std.mem.indexOfNone(u8, line[offset..line.len], ".") orelse {
            break;
        };
        pos += offset;
        const char = line[pos];
        if (char >= '0' and char <= '9') {
            var num: u64 = undefined;
            var numLen: u64 = undefined;
            if (std.mem.indexOfNone(u8, line[pos..line.len], "0123456789")) |endPos| {
                num = try std.fmt.parseInt(u64, line[pos .. pos + endPos], 10);
                numLen = endPos;
                offset = pos + endPos;
            } else {
                num = try std.fmt.parseInt(u64, line[pos..line.len], 10);
                numLen = line.len - pos;
                offset = line.len;
            }
            const numPart = NumPart{ .num = num, .length = numLen, .coords = .{ .x = pos, .y = linum } };
            // std.debug.print("Found {}\n", .{numPart});
            try map.numParts.append(numPart);
        } else {
            const symbol = Symbol{ .value = char, .coords = .{ .x = pos, .y = linum } };
            try map.symbols.append(symbol);
            // std.debug.print("Found {}\n", .{symbol});
            offset = pos + 1;
        }
    }
}

test "parseline" {
    {
        var map = Map.init(std.testing.allocator);
        defer map.deinit();

        try parseLine("................................................965..583........389.................307.................512......................395.....387", 0, &map);
        try expect(map.numParts.items.len == 7);
        try expect(map.symbols.items.len == 0);
        for ([_]u64{ 965, 583, 389, 307, 512, 395, 387 }, 0..) |n, i| {
            try expect(map.numParts.items[i].num == n);
        }
    }

    {
        var map = Map.init(std.testing.allocator);
        defer map.deinit();

        try parseLine("........................#....374...382....250...*..........737*....*896.395...........*....................$.........................#......", 0, &map);
        try expect(map.numParts.items.len == 6);
        try expect(map.symbols.items.len == 7);
        for ([_]u64{ 374, 382, 250, 737, 896, 395 }, 0..) |n, i| {
            try expect(map.numParts.items[i].num == n);
        }
        for ([_]u8{ '#', '*', '*', '*', '*', '$', '#' }, 0..) |n, i| {
            try expect(map.symbols.items[i].value == n);
        }
    }
}

fn parseMap(reader: std.fs.File.Reader, map: *Map) !void {
    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);
    var linum: usize = 0;

    while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        try parseLine(fbs.getWritten(), linum, map);
        fbs.reset();
        linum += 1;
    }
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const reader = try getReader();

    var map = Map.init(allocator);
    defer map.deinit();
    var total: u64 = 0;

    try parseMap(reader, &map);

    for (map.symbols.items) |symbol| {
        if (symbol.value != '*') {
            continue;
        }

        var neighbors: usize = 0;
        var ratio: u64 = 1;
        for (map.numParts.items) |numPart| {
            if (numPart.nextTo(&symbol.coords)) {
                neighbors += 1;
                ratio *= numPart.num;
                if (neighbors > 2) {
                    break;
                }
            }
        } else {
            if (neighbors == 2) {
                std.debug.print("Found gear {} with ratio {}\n", .{ symbol, ratio });
                total += ratio;
            }
        }
    }

    std.debug.print("Total: {}\n", .{total});
}
