const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

const Card = std.AutoHashMap(u8, void);

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn parseCard(allocator: std.mem.Allocator, line: []const u8) !u64 {
    var card = Card.init(allocator);
    defer card.deinit();

    const column = std.mem.indexOf(u8, line, ":");
    const pipe = std.mem.indexOf(u8, line, "|");

    const left = line[column.? + 2 .. pipe.? - 1];
    const right = line[pipe.? + 2 .. line.len];

    var ite = std.mem.tokenizeScalar(u8, left, ' ');

    while (ite.next()) |buf| {
        const n = try std.fmt.parseInt(u8, buf, 10);
        try card.put(n, {});
    }

    var score: u64 = 0;
    ite = std.mem.tokenizeScalar(u8, right, ' ');
    while (ite.next()) |buf| {
        const n = try std.fmt.parseInt(u8, buf, 10);
        if (card.contains(n)) {
            if (score == 0) {
                score = 1;
            } else {
                score *= 2;
            }
        }
    }
    std.debug.print("Left [{s}] - Right [{s}] => {}\n", .{ left, right, score });
    return score;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);

    var linum: usize = 1;
    var total: u64 = 0;

    while (true) : (linum += 1) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        const score = try parseCard(allocator, fbs.getWritten());
        total += score;
        // std.debug.print("Card {} has score {}\n", .{ linum, score });

        fbs.reset();
    }

    std.debug.print("Total: {}\n", .{total});
}
