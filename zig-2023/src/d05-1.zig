const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn readSeeds(line: []const u8, seeds: *std.ArrayList(u64)) !void {
    var ite = std.mem.tokenizeScalar(u8, line[7..line.len], ' ');

    while (ite.next()) |token| {
        try seeds.append(try std.fmt.parseInt(u64, token, 10));
    }
}

const MapRange = struct {
    dst: u64,
    src: u64,
    len: u64,

    pub fn initFromLine(line: []const u8) !MapRange {
        var ite = std.mem.tokenizeScalar(u8, line, ' ');
        const dst = try std.fmt.parseInt(u64, ite.next() orelse unreachable, 10);
        const src = try std.fmt.parseInt(u64, ite.next() orelse unreachable, 10);
        const len = try std.fmt.parseInt(u64, ite.next() orelse unreachable, 10);

        return MapRange{
            .dst = dst,
            .src = src,
            .len = len,
        };
    }

    pub fn convert(self: *const MapRange, num: u64) ?u64 {
        if (self.src <= num and num < self.src + self.len) {
            return num - self.src + self.dst;
        }
        return null;
    }
};

const Map = std.ArrayList(MapRange);

test "MapRange.initFromLine" {
    try testing.expectEqual(MapRange{ .dst = 123, .src = 456, .len = 789 }, try MapRange.initFromLine("123 456 789"));
    try testing.expectEqual(MapRange{ .dst = 1, .src = 23, .len = 456 }, try MapRange.initFromLine("1 23 456"));
}

test "MapRange.convert" {
    {
        const range = MapRange{ .dst = 52, .src = 50, .len = 48 };

        try testing.expectEqual(@as(?u64, null), range.convert(0));
        try testing.expectEqual(@as(?u64, 52), range.convert(50));
        try testing.expectEqual(@as(?u64, 53), range.convert(51));
        try testing.expectEqual(@as(?u64, 99), range.convert(97));
        try testing.expectEqual(@as(?u64, null), range.convert(98));
        try testing.expectEqual(@as(?u64, null), range.convert(100));
    }

    {
        const range = MapRange{ .dst = 60, .src = 56, .len = 37 };
        try testing.expectEqual(@as(?u64, 82), range.convert(78));
    }
}

fn processMap(allocator: std.mem.Allocator, reader: *std.fs.File.Reader, seeds: *std.ArrayList(u64)) !void {
    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);

    var map = Map.init(allocator);
    defer map.deinit();

    try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);
    std.debug.print("Map: {s}\n", .{fbs.getWritten()});

    while (true) {
        fbs.reset();
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        if (fbs.getWritten().len == 0) {
            break;
        }
        try map.append(try MapRange.initFromLine(fbs.getWritten()));
        std.debug.print(">>> {}\n", .{map.items[map.items.len - 1]});
    }

    for (seeds.items, 0..) |seed, i| {
        for (map.items) |range| {
            if (range.convert(seed)) |converted| {
                std.debug.print("Range: {} => {} => {}\n", .{ range, seed, converted });
                seeds.items[i] = converted;
                break;
            }
        }
    }
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);

    var seeds = std.ArrayList(u64).init(allocator);
    defer seeds.deinit();

    reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
        unreachable;
    };
    try readSeeds(fbs.getWritten(), &seeds);
    std.debug.print("Seeds: {d}\n", .{seeds.items});
    reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
        unreachable;
    };

    while (true) {
        fbs.reset();
        processMap(allocator, &reader, &seeds) catch {
            std.debug.print("> seeds: {d}\n", .{seeds.items});
            break;
        };
        std.debug.print("> seeds: {d}\n", .{seeds.items});
    }
    std.debug.print("Final seeds: {d}\n", .{seeds.items});

    std.debug.print("Min: {}\n", .{std.mem.min(u64, seeds.items)});
}
