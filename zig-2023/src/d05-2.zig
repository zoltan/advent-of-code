const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Range = struct {
    start: u64,
    end: u64,

    pub fn contains(self: *Range, value: u64) bool {
        return (self.start <= value) and (value <= self.end);
    }

    pub fn format(self: *const Range, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "Range({d} - {d})", .{ self.start, self.end });
    }
};

test "Range.contains" {
    var r = Range{ .start = 2, .end = 10 };
    try testing.expectEqual(false, r.contains(0));
    try testing.expectEqual(false, r.contains(1));
    try testing.expectEqual(true, r.contains(2));
    try testing.expectEqual(true, r.contains(5));
    try testing.expectEqual(true, r.contains(10));
    try testing.expectEqual(false, r.contains(11));
}

const MapEntry = struct {
    dst: u64,
    src: u64,
    len: u64,
};

const MapEntryList = std.ArrayList(MapEntry);

const Map = struct {
    entries: MapEntryList,

    pub fn init(allocator: std.mem.Allocator) Map {
        return Map{ .entries = MapEntryList.init(allocator) };
    }

    pub fn deinit(self: *Map) void {
        self.entries.deinit();
    }
};

fn readSeeds(allocator: std.mem.Allocator, line: []const u8) !std.ArrayList(Range) {
    var ite = std.mem.tokenizeScalar(u8, line, ' ');
    var ranges = std.ArrayList(Range).init(allocator);

    while (ite.next()) |tok_s| {
        const start = try std.fmt.parseInt(u64, tok_s, 10);
        const len = try std.fmt.parseInt(u64, ite.next().?, 10);

        try ranges.append(Range{ .start = start, .end = start + len - 1 });
    }
    return ranges;
}

fn readMaps(allocator: std.mem.Allocator, reader: std.fs.File.Reader) !std.ArrayList(Map) {
    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);
    var maps = std.ArrayList(Map).init(allocator);

    while (true) {
        // skip empty line and map name
        fbs.reset();
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        var map = Map.init(allocator);

        while (true) {
            fbs.reset();

            reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
                break;
            };

            const line = fbs.getWritten();
            if (line.len == 0) break;

            var ite = std.mem.tokenizeScalar(u8, line, ' ');
            try map.entries.append(MapEntry{
                .dst = try std.fmt.parseInt(u64, ite.next().?, 10),
                .src = try std.fmt.parseInt(u64, ite.next().?, 10),
                .len = try std.fmt.parseInt(u64, ite.next().?, 10),
            });
        }
        try maps.append(map);
    }
    return maps;
}

fn run(range: Range, maps: []const Map, entryIndex: usize) u64 {
    std.debug.assert(range.start <= range.end);

    if (maps.len == 0) {
        return range.start;
    }

    const entries = maps[0].entries.items;
    for (entryIndex..entries.len) |i| {
        const entry = entries[i];
        if (range.end < entry.src or range.start > entry.src + entry.len) {
            // no overlap
            continue;
        } else if (range.start >= entry.src and range.end <= entry.src + entry.len) {
            // range is fully covered by entry
            return run(
                Range{
                    .start = range.start + entry.dst - entry.src,
                    .end = range.end + entry.dst - entry.src,
                },
                maps[1..maps.len],
                0,
            );
        } else if (range.start < entry.src) {
            const left = run(
                Range{ .start = range.start, .end = entry.src },
                maps,
                i + 1,
            );
            const right = run(
                Range{ .start = entry.src, .end = range.end },
                maps,
                i,
            );
            return @min(left, right);
        } else {
            const left = run(
                Range{
                    .start = range.start + entry.dst - entry.src,
                    .end = entry.dst + entry.len,
                },
                maps[1..maps.len],
                0,
            );
            const right = run(
                Range{
                    .start = entry.src + entry.len,
                    .end = range.end,
                },
                maps,
                i + 1,
            );
            return @min(left, right);
        }
    }
    return run(range, maps[1..maps.len], 0);
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);

    try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);
    var line = fbs.getWritten();
    var seeds = try readSeeds(allocator, line[6..line.len]);
    defer seeds.deinit();

    try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);
    var maps = try readMaps(allocator, reader);
    defer maps.deinit();

    var min = seeds.items[0].start;
    for (seeds.items) |seed| {
        std.debug.print("🌱 Seeds: {d}\n", .{seed});
        const ret = run(seed, maps.items, 0);
        min = @min(min, ret);
    }

    std.debug.print("Result: {d}\n", .{min});
}
