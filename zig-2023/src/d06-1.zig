const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn countOptions(time: f64, distance: f64) u64 {
    const delta = time * time - 4 * distance;

    const x1 = @as(u64, @intFromFloat(@floor((-time + @sqrt(delta)) / -2.0)));
    const x2 = @as(u64, @intFromFloat(@ceil((-time - @sqrt(delta)) / -2.0)));

    return x2 - x1 - 1;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);

    var times = std.ArrayList(u64).init(allocator);
    var distances = std.ArrayList(u64).init(allocator);

    try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);
    var line = fbs.getWritten();
    var ite = std.mem.tokenizeScalar(u8, line[5..line.len], ' ');
    while (ite.next()) |token| {
        try times.append(try std.fmt.parseInt(u64, token, 10));
    }

    fbs.reset();
    try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);
    line = fbs.getWritten();
    ite = std.mem.tokenizeScalar(u8, line[10..line.len], ' ');
    while (ite.next()) |token| {
        try distances.append(try std.fmt.parseInt(u64, token, 10));
    }

    var total: u64 = 1;
    for (times.items, distances.items) |time, distance| {
        const options = countOptions(@floatFromInt(time), @floatFromInt(distance));
        total *= options;
        std.debug.print("{d:6} {d:6} -> options: {}\n", .{ time, distance, options });
    }

    std.debug.print("total: {}\n", .{total});
}
