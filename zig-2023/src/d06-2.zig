const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn countOptions(time: f64, distance: f64) u64 {
    const delta = time * time - 4 * distance;

    const x1 = @as(u64, @intFromFloat(@floor((-time + @sqrt(delta)) / -2.0)));
    const x2 = @as(u64, @intFromFloat(@ceil((-time - @sqrt(delta)) / -2.0)));

    return x2 - x1 - 1;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();
    _ = allocator;

    var reader = try getReader();

    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);

    var time: u64 = 0;
    var distance: u64 = 0;

    try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);
    var line = fbs.getWritten();
    for (line[5..line.len]) |c| {
        if (c == ' ') {
            continue;
        }
        time *= 10;
        time += c - '0';
    }

    fbs.reset();
    try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);
    line = fbs.getWritten();
    for (line[10..line.len]) |c| {
        if (c == ' ') {
            continue;
        }
        distance *= 10;
        distance += c - '0';
    }

    const options = countOptions(@floatFromInt(time), @floatFromInt(distance));
    std.debug.print("{d:6} {d:6} -> options: {}\n", .{ time, distance, options });
}
