const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Card = enum {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,

    pub fn make(value: u8) Card {
        return switch (value) {
            '2' => return .Two,
            '3' => return .Three,
            '4' => return .Four,
            '5' => return .Five,
            '6' => return .Six,
            '7' => return .Seven,
            '8' => return .Eight,
            '9' => return .Nine,
            'T' => return .Ten,
            'J' => return .Jack,
            'Q' => return .Queen,
            'K' => return .King,
            'A' => return .Ace,
            else => unreachable,
        };
    }

    pub fn format(self: Card, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        const c: u8 = switch (self) {
            .Two => '2',
            .Three => '3',
            .Four => '4',
            .Five => '5',
            .Six => '6',
            .Seven => '7',
            .Eight => '8',
            .Nine => '9',
            .Ten => 'T',
            .Jack => 'J',
            .Queen => 'Q',
            .King => 'K',
            .Ace => 'A',
        };
        try std.fmt.format(writer, "{c}", .{c});
    }
};

const HandType = enum {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,

    pub fn parse(input: []const u8) HandType {
        var values = [_]u8{0} ** 13;

        for (input) |i| {
            switch (i) {
                '2'...'9' => values[i - '2'] += 1,
                'T' => values[8] += 1,
                'J' => values[9] += 1,
                'Q' => values[10] += 1,
                'K' => values[11] += 1,
                'A' => values[12] += 1,
                else => unreachable,
            }
        }

        const handType = switch (std.mem.max(u8, &values)) {
            5 => HandType.FiveOfAKind,
            4 => HandType.FourOfAKind,
            3 => blk: {
                if (std.mem.indexOfScalar(u8, &values, 2)) |_| {
                    break :blk HandType.FullHouse;
                } else {
                    break :blk HandType.ThreeOfAKind;
                }
            },
            2 => blk: {
                if (std.mem.count(u8, &values, &[_]u8{2}) == 2) {
                    break :blk HandType.TwoPair;
                } else {
                    break :blk HandType.OnePair;
                }
            },
            else => HandType.HighCard,
        };
        return handType;
    }
};

test "HandType.parse" {
    try testing.expectEqual(HandType.FiveOfAKind, HandType.parse("AAAAA"));
    try testing.expectEqual(HandType.FourOfAKind, HandType.parse("KKAKK"));
    try testing.expectEqual(HandType.FullHouse, HandType.parse("KKKQQ"));
    try testing.expectEqual(HandType.ThreeOfAKind, HandType.parse("KKKQJ"));
    try testing.expectEqual(HandType.TwoPair, HandType.parse("KKQQJ"));
    try testing.expectEqual(HandType.OnePair, HandType.parse("KKQJ9"));
    try testing.expectEqual(HandType.HighCard, HandType.parse("KQJ98"));
}

const Hand = struct {
    cards: [5]Card,
    handType: HandType,
    bid: u64,

    pub fn init(line: []const u8) !Hand {
        var hand = Hand{
            .cards = undefined,
            .handType = HandType.parse(line[0..5]),
            .bid = try std.fmt.parseInt(u64, line[6..line.len], 10),
        };

        inline for (line[0..5], 0..) |c, i| {
            hand.cards[i] = Card.make(c);
        }
        return hand;
    }

    fn lessThan(ctx: void, a: Hand, b: Hand) bool {
        _ = ctx;
        if (@intFromEnum(a.handType) == @intFromEnum(b.handType)) {
            for (a.cards, b.cards) |l, r| {
                if (@intFromEnum(l) < @intFromEnum(r)) {
                    return true;
                } else if (@intFromEnum(l) > @intFromEnum(r)) {
                    return false;
                }
            }
            return false;
        }
        return @intFromEnum(a.handType) < @intFromEnum(b.handType);
    }
};

test Hand {
    try testing.expectEqual(Hand{ .cards = [_]Card{ .Three, .Two, .Ten, .Three, .King }, .bid = 765, .handType = HandType.OnePair }, try Hand.init("32T3K 765"));
    try testing.expectEqual(Hand{ .cards = [_]Card{ .Ten, .Five, .Five, .Jack, .Five }, .bid = 684, .handType = HandType.ThreeOfAKind }, try Hand.init("T55J5 684"));
    try testing.expectEqual(Hand{ .cards = [_]Card{ .King, .King, .Six, .Seven, .Seven }, .bid = 28, .handType = HandType.TwoPair }, try Hand.init("KK677 28"));
    try testing.expectEqual(Hand{ .cards = [_]Card{ .King, .Ten, .Jack, .Jack, .Ten }, .bid = 220, .handType = HandType.TwoPair }, try Hand.init("KTJJT 220"));
    try testing.expectEqual(Hand{ .cards = [_]Card{ .Queen, .Queen, .Queen, .Jack, .Ace }, .bid = 483, .handType = HandType.ThreeOfAKind }, try Hand.init("QQQJA 483"));
}

test "Hand.lessThan" {
    const l = try Hand.init("6J25T 265");
    const r = try Hand.init("T72A4 657");

    try testing.expectEqual(true, Hand.lessThan({}, l, r));
    try testing.expectEqual(false, Hand.lessThan({}, r, l));
    try testing.expectEqual(false, Hand.lessThan({}, l, l));
    try testing.expectEqual(false, Hand.lessThan({}, r, r));
    try testing.expectEqual(false, Hand.lessThan({}, l, l));
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);

    var hands = std.ArrayList(Hand).init(allocator);
    defer hands.deinit();

    while (true) {
        fbs.reset();
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };

        const hand = try Hand.init(fbs.getWritten());
        try hands.append(hand);
    }

    std.debug.print("Found {} hands\nSorting...\n", .{hands.items.len});

    std.sort.pdq(Hand, hands.items, {}, Hand.lessThan);

    var total: u64 = 0;
    for (hands.items, 1..) |hand, rank| {
        total += rank * hand.bid;
        // std.debug.print("[{d:4} : {d:8} ]>> {}\n", .{ rank, total, hand });
    }
    std.debug.print("Total: {d}\n", .{total});
}
