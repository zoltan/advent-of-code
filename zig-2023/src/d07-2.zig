const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Card = enum {
    Jack,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Queen,
    King,
    Ace,

    pub fn make(value: u8) Card {
        return switch (value) {
            'J' => return .Jack,
            '2' => return .Two,
            '3' => return .Three,
            '4' => return .Four,
            '5' => return .Five,
            '6' => return .Six,
            '7' => return .Seven,
            '8' => return .Eight,
            '9' => return .Nine,
            'T' => return .Ten,
            'Q' => return .Queen,
            'K' => return .King,
            'A' => return .Ace,
            else => unreachable,
        };
    }

    pub fn format(self: Card, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        const c: u8 = switch (self) {
            .Two => '2',
            .Three => '3',
            .Four => '4',
            .Five => '5',
            .Six => '6',
            .Seven => '7',
            .Eight => '8',
            .Nine => '9',
            .Ten => 'T',
            .Jack => 'J',
            .Queen => 'Q',
            .King => 'K',
            .Ace => 'A',
        };
        try std.fmt.format(writer, "{c}", .{c});
    }
};

const HandType = enum {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,

    pub fn parse(cards: [5]Card) HandType {
        var values = [_]u8{0} ** @typeInfo(Card).Enum.fields.len;

        for (cards) |i| {
            values[@intFromEnum(i)] += 1;
        }

        const jokersCount = values[@intFromEnum(Card.Jack)];

        const handType: HandType = switch (std.mem.max(u8, &values)) {
            5 => .FiveOfAKind,
            4 => if (jokersCount == 1 or jokersCount == 4) .FiveOfAKind else .FourOfAKind,
            3 => blk: {
                const hasPair: bool = if (std.mem.indexOfScalar(u8, &values, 2)) |_| true else false;

                break :blk switch (jokersCount) {
                    // XXX JJ
                    2 => .FiveOfAKind,
                    // XXX J Y
                    1 => .FourOfAKind,
                    // XXX JJ or XXX J Y
                    3 => if (hasPair) .FiveOfAKind else .FourOfAKind,
                    // XXX YY or XXX Y Z
                    else => if (hasPair) .FullHouse else .ThreeOfAKind,
                };
            },
            2 => blk: {
                const hasTwoPairs: bool = std.mem.count(u8, &values, &[_]u8{2}) == 2;

                break :blk switch (jokersCount) {
                    // XX JJ Y or JJ XYZ
                    2 => if (hasTwoPairs) .FourOfAKind else .ThreeOfAKind,
                    // XX J YY or XX J YZ
                    1 => if (hasTwoPairs) .FullHouse else .ThreeOfAKind,
                    // XX YY Z or XX WYZ
                    else => if (hasTwoPairs) .TwoPair else .OnePair,
                };
            },
            else => if (jokersCount == 1) .OnePair else .HighCard,
        };
        return handType;
    }
};

test "HandType.parse" {
    {
        try testing.expectEqual(HandType.FiveOfAKind, HandType.parse([_]Card{ .Ace, .Ace, .Ace, .Ace, .Ace }));
        try testing.expectEqual(HandType.FourOfAKind, HandType.parse([_]Card{ .King, .King, .Ace, .King, .King }));
        try testing.expectEqual(HandType.FullHouse, HandType.parse([_]Card{ .King, .King, .King, .Queen, .Queen }));
        try testing.expectEqual(HandType.FourOfAKind, HandType.parse([_]Card{ .King, .King, .King, .Queen, .Jack }));
        try testing.expectEqual(HandType.FullHouse, HandType.parse([_]Card{ .King, .King, .Queen, .Queen, .Jack }));
        try testing.expectEqual(HandType.ThreeOfAKind, HandType.parse([_]Card{ .King, .King, .Queen, .Jack, .Nine }));
        try testing.expectEqual(HandType.OnePair, HandType.parse([_]Card{ .King, .Queen, .Jack, .Nine, .Eight }));
        try testing.expectEqual(HandType.ThreeOfAKind, HandType.parse([_]Card{ .Jack, .Jack, .Five, .Ace, .Ten }));
    }

    {
        try testing.expectEqual(HandType.OnePair, HandType.parse([_]Card{ .Three, .Two, .Ten, .Three, .King }));
        try testing.expectEqual(HandType.TwoPair, HandType.parse([_]Card{ .King, .King, .Six, .Seven, .Seven }));
        try testing.expectEqual(HandType.FourOfAKind, HandType.parse([_]Card{ .Ten, .Five, .Five, .Jack, .Five }));
        try testing.expectEqual(HandType.FourOfAKind, HandType.parse([_]Card{ .King, .Ten, .Jack, .Jack, .Ten }));
        try testing.expectEqual(HandType.FourOfAKind, HandType.parse([_]Card{ .Queen, .Queen, .Queen, .Jack, .Ace }));
    }
}

test "HandType.parse: max_card_count=4" {
    {
        // XXXX Y
        try testing.expectEqual(HandType.FourOfAKind, HandType.parse([_]Card{ .Queen, .Queen, .Queen, .Queen, .King }));
        // XXXX J
        try testing.expectEqual(HandType.FiveOfAKind, HandType.parse([_]Card{ .Queen, .Queen, .Queen, .Queen, .Jack }));
        // JJJJ X
        try testing.expectEqual(HandType.FiveOfAKind, HandType.parse([_]Card{ .Jack, .Jack, .Jack, .Jack, .King }));
    }
}
test "HandType.parse: max_card_count=3" {
    {
        // XXX JJ
        try testing.expectEqual(HandType.FiveOfAKind, HandType.parse([_]Card{ .Queen, .Queen, .Queen, .Jack, .Jack }));
        // XXX J Y
        try testing.expectEqual(HandType.FourOfAKind, HandType.parse([_]Card{ .Queen, .Queen, .Queen, .Jack, .King }));
        // XXX YY
        try testing.expectEqual(HandType.FullHouse, HandType.parse([_]Card{ .Queen, .Queen, .Queen, .King, .King }));
        // XXX YZ
        try testing.expectEqual(HandType.ThreeOfAKind, HandType.parse([_]Card{ .Queen, .Queen, .Queen, .King, .Ace }));

        // JJJ XX
        try testing.expectEqual(HandType.FiveOfAKind, HandType.parse([_]Card{ .Jack, .Jack, .Jack, .King, .King }));
        // JJJ X Y
        try testing.expectEqual(HandType.FourOfAKind, HandType.parse([_]Card{ .Jack, .Jack, .Jack, .King, .Queen }));
    }
}

test "HandType.parse: max_card_count=2" {
    {
        // XX JJ Y
        try testing.expectEqual(HandType.FourOfAKind, HandType.parse([_]Card{ .Queen, .Queen, .Jack, .Jack, .King }));
        // XX J YY
        try testing.expectEqual(HandType.FullHouse, HandType.parse([_]Card{ .Queen, .Queen, .Jack, .King, .King }));
        // XX YY Z
        try testing.expectEqual(HandType.TwoPair, HandType.parse([_]Card{ .Queen, .Queen, .King, .King, .Ace }));

        // XX J YZ
        try testing.expectEqual(HandType.ThreeOfAKind, HandType.parse([_]Card{ .Queen, .Queen, .Jack, .Ace, .King }));
        // X JJ YZ
        try testing.expectEqual(HandType.ThreeOfAKind, HandType.parse([_]Card{ .Queen, .Jack, .Jack, .Ace, .King }));

        // XX WYZ
        try testing.expectEqual(HandType.OnePair, HandType.parse([_]Card{ .Queen, .Queen, .Ten, .Ace, .King }));
    }
}

const Hand = struct {
    cards: [5]Card,
    handType: HandType,
    bid: u64,

    pub fn init(line: []const u8) !Hand {
        var hand = Hand{
            .cards = undefined,
            .handType = undefined,
            .bid = try std.fmt.parseInt(u64, line[6..line.len], 10),
        };

        inline for (line[0..5], 0..) |c, i| {
            hand.cards[i] = Card.make(c);
        }
        hand.handType = HandType.parse(hand.cards);
        return hand;
    }

    fn lessThan(ctx: void, a: Hand, b: Hand) bool {
        _ = ctx;
        if (@intFromEnum(a.handType) == @intFromEnum(b.handType)) {
            for (a.cards, b.cards) |l, r| {
                if (@intFromEnum(l) != @intFromEnum(r)) {
                    return @intFromEnum(l) < @intFromEnum(r);
                }
            }
            return false;
        }
        return @intFromEnum(a.handType) < @intFromEnum(b.handType);
    }
};

test Hand {
    try testing.expectEqual(Hand{ .cards = [_]Card{ .Three, .Two, .Ten, .Three, .King }, .bid = 765, .handType = .OnePair }, try Hand.init("32T3K 765"));
    try testing.expectEqual(Hand{ .cards = [_]Card{ .Ten, .Five, .Five, .Jack, .Five }, .bid = 684, .handType = .FourOfAKind }, try Hand.init("T55J5 684"));
    try testing.expectEqual(Hand{ .cards = [_]Card{ .King, .King, .Six, .Seven, .Seven }, .bid = 28, .handType = .TwoPair }, try Hand.init("KK677 28"));
    try testing.expectEqual(Hand{ .cards = [_]Card{ .King, .Ten, .Jack, .Jack, .Ten }, .bid = 220, .handType = .FourOfAKind }, try Hand.init("KTJJT 220"));
    try testing.expectEqual(Hand{ .cards = [_]Card{ .Queen, .Queen, .Queen, .Jack, .Ace }, .bid = 483, .handType = .FourOfAKind }, try Hand.init("QQQJA 483"));
}

test "Hand.lessThan" {
    {
        const l = try Hand.init("T55J5 265");
        const r = try Hand.init("KTJJT 657");

        try testing.expectEqual(true, Hand.lessThan({}, l, r));
        try testing.expectEqual(false, Hand.lessThan({}, r, l));
        try testing.expectEqual(false, Hand.lessThan({}, l, l));
        try testing.expectEqual(false, Hand.lessThan({}, r, r));
        try testing.expectEqual(false, Hand.lessThan({}, l, l));
    }
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);

    var hands = std.ArrayList(Hand).init(allocator);
    defer hands.deinit();

    while (true) {
        fbs.reset();
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };

        const hand = try Hand.init(fbs.getWritten());
        try hands.append(hand);
    }

    std.debug.print("Found {} hands\nSorting...\n", .{hands.items.len});

    std.sort.pdq(Hand, hands.items, {}, Hand.lessThan);

    var total: u64 = 0;
    for (hands.items, 1..) |hand, rank| {
        total += rank * hand.bid;
        std.debug.print("[{d:0>4} : {d:0>8}] {}\n", .{ rank, total, hand });
    }
    std.debug.print("Total: {d}\n", .{total});

    const knownErrors = [_]u64{
        250960730, // too low
        251008389, // don't know
    };

    if (std.mem.indexOfScalar(u64, &knownErrors, total)) |_| {
        std.debug.print("{d} is not the correct result\n", .{total});
    } else {
        std.debug.print("{d} was not tested before!\n", .{total});
    }
}
