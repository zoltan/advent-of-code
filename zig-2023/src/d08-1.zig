const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
    InvalidPlace,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Place = [3]u8;

pub fn makePlace(s: []const u8) Place {
    if (s.len != 3) {
        unreachable;
    }

    var place: Place = undefined;
    @memcpy(&place, s[0..3]);
    return place;
}

pub fn samePlace(a: Place, b: Place) bool {
    return a[0] == b[0] and a[1] == b[1] and a[2] == b[2];
}

const Destination = struct {
    Left: Place,
    Right: Place,

    pub fn fromString(s: []const u8) Destination {
        if (s.len < 8) {
            unreachable;
        }

        return Destination{
            .Left = makePlace(s[0..3]),
            .Right = makePlace(s[5..8]),
        };
    }

    pub fn format(self: *const Destination, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "(left={s}, rigth={s})", .{ self.Left, self.Right });
    }
};

test {
    const dest = Destination.fromString("ABC, DEF");

    try testing.expectEqual(dest.Left, [_]u8{ 'A', 'B', 'C' });
    try testing.expectEqual(dest.Right, [_]u8{ 'D', 'E', 'F' });
}

const CycleIterator = struct {
    allocator: std.mem.Allocator,
    buffer: []const u8 = undefined,
    current: usize = 0,

    pub fn init(allocator: std.mem.Allocator, line: []const u8) !CycleIterator {
        var ite = CycleIterator{ .allocator = allocator };
        ite.buffer = try allocator.dupe(u8, line);
        return ite;
    }

    pub fn next(self: *CycleIterator) u8 {
        const c = self.buffer[self.current];
        self.current += 1;
        if (self.current == self.buffer.len) {
            self.current = 0;
        }
        return c;
    }

    pub fn deinit(self: *CycleIterator) void {
        self.allocator.free(self.buffer);
    }
};

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);

    var map = std.AutoHashMap(Place, Destination).init(allocator);

    try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);
    var line = fbs.getWritten();
    var instructions = try CycleIterator.init(allocator, line);
    defer instructions.deinit();

    // skip empty line
    try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);

    while (true) {
        fbs.reset();
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        line = fbs.getWritten();
        const k = makePlace(line[0..3]);
        const v = Destination.fromString(line[7..line.len]);

        std.debug.print("k: {s}, v: {}\n", .{ k, v });
        try map.put(k, v);
    }

    var current = makePlace("AAA");
    const target = makePlace("ZZZ");
    var steps: usize = 0;

    while (!samePlace(current, target)) : (steps += 1) {
        const dest = map.get(current).?;
        const next = instructions.next();
        std.debug.print("[{d:4}]current: {s}, dest: {} => {c}\n", .{ steps, current, dest, next });
        current = switch (next) {
            'L' => dest.Left,
            'R' => dest.Right,
            else => unreachable,
        };
    }
    std.debug.print("Total steps: {d}\n", .{steps});
}
