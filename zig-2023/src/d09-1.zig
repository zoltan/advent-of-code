const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn findNext(allocator: std.mem.Allocator, nums: std.ArrayList(i64)) !i64 {
    var nextLine = std.ArrayList(i64).init(allocator);
    defer nextLine.deinit();

    var onlyZeros: bool = true;
    var previous: i64 = nums.items[0];
    for (1..nums.items.len) |i| {
        const diff: i64 = nums.items[i] - previous;

        try nextLine.append(diff);
        if (diff != 0) {
            onlyZeros = false;
        }
        previous = nums.items[i];
    }
    // std.debug.print(">> {d}\n>>>> {d}\n", .{ nums.items, nextLine.items });
    if (onlyZeros) {
        std.debug.print("next num is: {d}\n", .{nums.items[nums.items.len - 1] + nextLine.items[nextLine.items.len - 1]});
        return nums.items[nums.items.len - 1] + nextLine.items[nextLine.items.len - 1];
    }
    const next = try findNext(allocator, nextLine);
    // std.debug.print(">> [{d}] << {d}\n", .{ next, nums.items });
    return nums.items[nums.items.len - 1] + next;
}

test "findNext - all equal" {
    var nums = std.ArrayList(i64).init(std.testing.allocator);
    defer nums.deinit();

    try nums.append(3);
    try nums.append(3);
    try nums.append(3);
    try nums.append(3);
    try nums.append(3);

    try testing.expect(try findNext(std.testing.allocator, nums) == 3);
}

test "findNext - example 1" {
    var nums = std.ArrayList(i64).init(std.testing.allocator);
    defer nums.deinit();

    std.debug.print("Example line 1\n", .{});
    try nums.append(0);
    try nums.append(3);
    try nums.append(6);
    try nums.append(9);
    try nums.append(12);
    try nums.append(15);

    try testing.expect(try findNext(std.testing.allocator, nums) == 18);
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);
    var total: i64 = 0;

    while (true) {
        fbs.reset();
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        var nums = std.ArrayList(i64).init(allocator);
        defer nums.deinit();

        const line = fbs.getWritten();
        var ite = std.mem.tokenizeScalar(u8, line, ' ');
        while (ite.next()) |token| {
            const num = try std.fmt.parseInt(i64, token, 10);
            try nums.append(num);
        }
        const next = try findNext(allocator, nums);
        total += next;
        std.debug.print(">> {d} ++ {d} ----> {d}\n", .{ nums.items, next, total });
    }
    std.debug.print("Total steps: {d}\n", .{total});
}
