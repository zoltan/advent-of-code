const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
    InvalidCoords,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Direction = enum {
    North,
    East,
    West,
    South,

    pub fn opposite(self: Direction) Direction {
        return switch (self) {
            .North => .South,
            .South => .North,
            .East => .West,
            .West => .East,
        };
    }
};

const Tile = enum {
    Vertical,
    Horizontal,
    NorthEast,
    NorthWest,
    SouthWest,
    SouthEast,
    Start,
    Ground,

    pub fn fromChar(c: u8) Tile {
        return switch (c) {
            '|' => .Vertical,
            '-' => .Horizontal,
            'L' => .NorthEast,
            'J' => .NorthWest,
            '7' => .SouthWest,
            'F' => .SouthEast,
            'S' => .Start,
            else => .Ground,
        };
    }

    pub fn format(self: Tile, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        const char: u8 = switch (self) {
            .Vertical => '|',
            .Horizontal => '-',
            .NorthEast => 'L',
            .NorthWest => 'J',
            .SouthWest => '7',
            .SouthEast => 'F',
            .Start => 'S',
            else => '.',
        };
        try std.fmt.format(writer, "{c}", .{char});
    }

    pub fn canMoveTo(self: Tile, dir: Direction) bool {
        return switch (dir) {
            .North => switch (self) {
                .Vertical, .NorthEast, .NorthWest => true,
                else => false,
            },
            .South => switch (self) {
                .Vertical, .SouthEast, .SouthWest => true,
                else => false,
            },
            .East => switch (self) {
                .Horizontal, .NorthEast, .SouthEast => true,
                else => false,
            },
            .West => switch (self) {
                .Horizontal, .NorthWest, .SouthWest => true,
                else => false,
            },
        };
    }

    pub fn getOtherDirection(self: Tile, dir: Direction) Direction {
        return switch (dir) {
            .North => switch (self) {
                .Vertical => .South,
                .NorthEast => .East,
                .NorthWest => .West,
                else => unreachable,
            },
            .South => switch (self) {
                .Vertical => .North,
                .SouthEast => .East,
                .SouthWest => .West,
                else => unreachable,
            },
            .West => switch (self) {
                .Horizontal => .East,
                .NorthWest => .North,
                .SouthWest => .South,
                else => unreachable,
            },
            .East => switch (self) {
                .Horizontal => .West,
                .NorthEast => .North,
                .SouthEast => .South,
                else => unreachable,
            },
        };
    }

    pub fn fromDirections(dir1: Direction, dir2: Direction) Tile {
        return switch (dir1) {
            .North => switch (dir2) {
                .South => .Vertical,
                .East => .NorthEast,
                .West => .NorthWest,
                else => unreachable,
            },
            .South => switch (dir2) {
                .North => .Vertical,
                .East => .SouthEast,
                .West => .SouthWest,
                else => unreachable,
            },
            .West => switch (dir2) {
                .East => .Horizontal,
                .North => .NorthWest,
                .South => .SouthWest,
                else => unreachable,
            },
            .East => switch (dir2) {
                .West => .Horizontal,
                .North => .NorthEast,
                .South => .SouthEast,
                else => unreachable,
            },
        };
    }
};

const Coords = struct {
    x: u64,
    y: u64,

    pub fn format(self: *const Coords, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "(x={}, y={})", .{ self.x, self.y });
    }

    pub fn move(self: *const Coords, dir: Direction) !Coords {
        return switch (dir) {
            .North => if (self.y > 0) Coords{ .x = self.x, .y = self.y - 1 } else AOCError.InvalidCoords,
            .West => if (self.x > 0) Coords{ .x = self.x - 1, .y = self.y } else AOCError.InvalidCoords,
            .East => Coords{ .x = self.x + 1, .y = self.y },
            .South => Coords{ .x = self.x, .y = self.y + 1 },
        };
    }

    pub fn equal(self: *const Coords, other: *const Coords) bool {
        return self.x == other.x and self.y == other.y;
    }
};

const Map = struct {
    allocator: std.mem.Allocator,
    tiles: [][]Tile,
    start: Coords,
    size: u64,

    fn deinit(self: *Map) void {
        for (self.tiles) |row| {
            self.allocator.free(row);
        }
        self.allocator.free(self.tiles);
    }

    fn fromReader(allocator: std.mem.Allocator, reader: *std.fs.File.Reader) !Map {
        var map = Map{
            .allocator = allocator,
            .tiles = undefined,
            .start = undefined,
            .size = 0,
        };
        var buffer: [512:0]u8 = undefined;
        var fbs = std.io.fixedBufferStream(&buffer);

        try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);
        map.size = fbs.getWritten().len;
        map.tiles = try allocator.alloc([]Tile, map.size);

        var current: u64 = 0;
        while (true) : (current += 1) {
            const line = fbs.getWritten();
            var tiles = try allocator.alloc(Tile, map.size);
            for (line, 0..) |c, i| {
                const tile = Tile.fromChar(c);
                if (tile == .Start) {
                    map.start = Coords{ .x = i, .y = current };
                }
                tiles[i] = tile;
            }
            map.tiles[current] = tiles;

            fbs.reset();
            reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
                break;
            };
        }
        const s = map.inferStartTile();
        map.tiles[map.start.y][map.start.x] = s;
        return map;
    }

    fn inferStartTile(self: *const Map) Tile {
        var options: [2]Direction = undefined;
        var offset: u1 = 0;

        for ([_]Direction{ .North, .South, .West, .East }) |dir| {
            const c = self.start.move(dir) catch {
                continue;
            };
            const t = self.get(c) catch {
                continue;
            };

            if (t.canMoveTo(dir.opposite())) {
                options[offset] = dir;
                if (offset == 0) {
                    offset += 1;
                } else {
                    break;
                }
            }
        }

        return Tile.fromDirections(options[0], options[1]);
    }

    pub fn format(self: *const Map, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "Size: {} - start: {}", .{ self.size, self.start });
    }

    pub fn get(self: *const Map, coords: Coords) !Tile {
        if (coords.x < self.size and coords.y < self.size) {
            return self.tiles[coords.y][coords.x];
        }
        return AOCError.InvalidCoords;
    }
};

const Iterator = struct {
    map: *const Map,
    current: Coords,
    direction: Direction,

    pub fn initFromStart(map: *const Map, direction: Direction) Iterator {
        const ite = Iterator{
            .map = map,
            .current = map.start,
            .direction = direction,
        };

        return ite;
    }

    pub fn next(self: *Iterator) ?Coords {
        const nextCoord = self.current.move(self.direction) catch {
            unreachable;
        };

        const nextTile = self.map.get(nextCoord) catch {
            unreachable;
        };
        const nextDir = nextTile.getOtherDirection(self.direction.opposite());
        std.debug.print("Going {} from {} to {} - next direction {}.\n", .{ self.direction, self.current, nextCoord, nextDir });
        self.current = nextCoord;
        self.direction = nextDir;
        if (nextCoord.equal(&self.map.start)) {
            return null;
        }
        return nextCoord;
    }
};

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var map = try Map.fromReader(allocator, &reader);
    defer map.deinit();
    std.debug.print("{}\n", .{map});
    for (map.tiles) |tiles| {
        std.debug.print("{s}\n", .{tiles});
    }

    const startDir: Direction = blk: for ([_]Direction{ .North, .West, .East, .South }) |dir| {
        const startingTile = try map.get(map.start);
        if (startingTile.canMoveTo(dir)) {
            break :blk dir;
        }
    } else {
        unreachable;
    };

    std.debug.print("Start by going {}\n", .{startDir});

    var distance: u64 = 0;
    var ite = Iterator.initFromStart(&map, startDir);
    while (ite.next()) |_| {
        distance += 1;
    }

    std.debug.print("Loop length: {}\n", .{distance});
    std.debug.print("Result: {}\n", .{(distance + 1) / 2});
}
