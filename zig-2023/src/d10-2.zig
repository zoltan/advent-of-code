const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
    InvalidCoords,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Direction = enum {
    North,
    East,
    West,
    South,

    pub fn opposite(self: Direction) Direction {
        return switch (self) {
            .North => .South,
            .South => .North,
            .East => .West,
            .West => .East,
        };
    }
};

const Tile = enum {
    Vertical,
    Horizontal,
    NorthEast,
    NorthWest,
    SouthWest,
    SouthEast,
    Start,
    Ground,

    pub fn fromChar(c: u8) Tile {
        return switch (c) {
            '|' => .Vertical,
            '-' => .Horizontal,
            'L' => .NorthEast,
            'J' => .NorthWest,
            '7' => .SouthWest,
            'F' => .SouthEast,
            'S' => .Start,
            else => .Ground,
        };
    }

    pub fn format(self: Tile, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        const unicode: []const u8 = switch (self) {
            .Vertical => "│",
            .Horizontal => "─",
            .NorthEast => "└",
            .NorthWest => "┘",
            .SouthWest => "┐",
            .SouthEast => "┌",
            .Start => "S",
            else => ".",
        };
        try std.fmt.format(writer, "{s}", .{unicode});
    }

    pub fn canMoveTo(self: Tile, dir: Direction) bool {
        return switch (dir) {
            .North => switch (self) {
                .Vertical, .NorthEast, .NorthWest => true,
                else => false,
            },
            .South => switch (self) {
                .Vertical, .SouthEast, .SouthWest => true,
                else => false,
            },
            .East => switch (self) {
                .Horizontal, .NorthEast, .SouthEast => true,
                else => false,
            },
            .West => switch (self) {
                .Horizontal, .NorthWest, .SouthWest => true,
                else => false,
            },
        };
    }

    pub fn getOtherDirection(self: Tile, dir: Direction) Direction {
        return switch (dir) {
            .North => switch (self) {
                .Vertical => .South,
                .NorthEast => .East,
                .NorthWest => .West,
                else => unreachable,
            },
            .South => switch (self) {
                .Vertical => .North,
                .SouthEast => .East,
                .SouthWest => .West,
                else => unreachable,
            },
            .West => switch (self) {
                .Horizontal => .East,
                .NorthWest => .North,
                .SouthWest => .South,
                else => unreachable,
            },
            .East => switch (self) {
                .Horizontal => .West,
                .NorthEast => .North,
                .SouthEast => .South,
                else => unreachable,
            },
        };
    }

    pub fn fromDirections(dir1: Direction, dir2: Direction) Tile {
        return switch (dir1) {
            .North => switch (dir2) {
                .South => .Vertical,
                .East => .NorthEast,
                .West => .NorthWest,
                else => unreachable,
            },
            .South => switch (dir2) {
                .North => .Vertical,
                .East => .SouthEast,
                .West => .SouthWest,
                else => unreachable,
            },
            .West => switch (dir2) {
                .East => .Horizontal,
                .North => .NorthWest,
                .South => .SouthWest,
                else => unreachable,
            },
            .East => switch (dir2) {
                .West => .Horizontal,
                .North => .NorthEast,
                .South => .SouthEast,
                else => unreachable,
            },
        };
    }
};

const Coords = struct {
    x: u64,
    y: u64,

    pub fn format(self: *const Coords, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "(x={}, y={})", .{ self.x, self.y });
    }

    pub fn move(self: *const Coords, dir: Direction) !Coords {
        return switch (dir) {
            .North => if (self.y > 0) Coords{ .x = self.x, .y = self.y - 1 } else AOCError.InvalidCoords,
            .West => if (self.x > 0) Coords{ .x = self.x - 1, .y = self.y } else AOCError.InvalidCoords,
            .East => Coords{ .x = self.x + 1, .y = self.y },
            .South => Coords{ .x = self.x, .y = self.y + 1 },
        };
    }

    pub fn equal(self: *const Coords, other: *const Coords) bool {
        return self.x == other.x and self.y == other.y;
    }
};

const CoordsSet = std.AutoHashMap(Coords, void);

const Map = struct {
    allocator: std.mem.Allocator,
    tiles: [][]Tile,
    start: Coords,
    size: u64,

    fn deinit(self: *Map) void {
        for (self.tiles) |row| {
            self.allocator.free(row);
        }
        self.allocator.free(self.tiles);
    }

    fn fromReader(allocator: std.mem.Allocator, reader: *std.fs.File.Reader) !Map {
        var map = Map{
            .allocator = allocator,
            .tiles = undefined,
            .start = undefined,
            .size = 0,
        };
        var buffer: [512:0]u8 = undefined;
        var fbs = std.io.fixedBufferStream(&buffer);

        try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);
        // add one to the size so (0, 0) is always "outside"
        map.size = fbs.getWritten().len + 2;
        map.tiles = try allocator.alloc([]Tile, map.size);

        map.tiles[0] = try allocator.alloc(Tile, map.size);
        @memset(map.tiles[0], Tile.Ground);
        map.tiles[map.size - 1] = try allocator.alloc(Tile, map.size);
        @memset(map.tiles[map.size - 1], Tile.Ground);

        var current: u64 = 1;
        while (true) : (current += 1) {
            const line = fbs.getWritten();
            var tiles = try allocator.alloc(Tile, map.size);
            tiles[0] = .Ground;
            tiles[map.size - 1] = .Ground;
            for (line, 1..) |c, i| {
                const tile = Tile.fromChar(c);
                if (tile == .Start) {
                    map.start = Coords{ .x = i, .y = current };
                }
                tiles[i] = tile;
            }
            map.tiles[current] = tiles;

            fbs.reset();
            reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
                break;
            };
        }
        const s = map.inferStartTile();
        map.tiles[map.start.y][map.start.x] = s;
        return map;
    }

    fn inferStartTile(self: *const Map) Tile {
        var options: [2]Direction = undefined;
        var offset: u1 = 0;

        for ([_]Direction{ .North, .South, .West, .East }) |dir| {
            const c = self.start.move(dir) catch {
                continue;
            };
            const t = self.get(c) catch {
                continue;
            };

            if (t.canMoveTo(dir.opposite())) {
                options[offset] = dir;
                if (offset == 0) {
                    offset += 1;
                } else {
                    break;
                }
            }
        }

        return Tile.fromDirections(options[0], options[1]);
    }

    pub fn format(self: *const Map, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "Size: {} - start: {}", .{ self.size, self.start });
    }

    pub fn get(self: *const Map, coords: Coords) !Tile {
        if (coords.x < self.size and coords.y < self.size) {
            return self.tiles[coords.y][coords.x];
        }
        return AOCError.InvalidCoords;
    }
};

const Iterator = struct {
    map: *const Map,
    current: Coords,
    direction: Direction,

    pub fn initFromStart(map: *const Map, direction: Direction) Iterator {
        const ite = Iterator{
            .map = map,
            .current = map.start,
            .direction = direction,
        };

        return ite;
    }

    pub fn next(self: *Iterator) ?Coords {
        const nextCoord = self.current.move(self.direction) catch {
            unreachable;
        };

        const nextTile = self.map.get(nextCoord) catch {
            unreachable;
        };
        const nextDir = nextTile.getOtherDirection(self.direction.opposite());
        self.current = nextCoord;
        self.direction = nextDir;
        if (nextCoord.equal(&self.map.start)) {
            return null;
        }
        return nextCoord;
    }
};

fn countInnerTiles(map: *const Map, loop: *const CoordsSet) !usize {
    var total: usize = 0;

    for (0..map.size) |y| {
        var inside = false;
        var x: usize = 1;

        std.debug.print("{d:4}: ", .{y});
        while (x < map.size) : (x += 1) {
            const c = Coords{ .x = x, .y = y };

            if (loop.contains(c)) {
                const t = try map.get(c);

                if (t == .Vertical) {
                    inside = !inside;
                    std.debug.print("{}", .{t});
                } else {
                    std.debug.print("{}", .{t});
                    var u = try map.get(Coords{ .x = x + 1, .y = y });
                    x += 1;
                    while (u == .Horizontal) {
                        std.debug.print("{}", .{u});
                        x += 1;
                        u = try map.get(Coords{ .x = x, .y = y });
                    }
                    std.debug.print("{}", .{u});
                    if ((t == .NorthEast and u == .SouthWest) or (t == .SouthEast and u == .NorthWest)) {
                        inside = !inside;
                    }
                }
            } else if (inside) {
                total += 1;
                std.debug.print("x", .{});
            } else {
                std.debug.print(".", .{});
            }
        }
        std.debug.print("\n", .{});
    }
    return total;
}

pub fn main() !void {
    {}
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var map = try Map.fromReader(allocator, &reader);
    defer map.deinit();
    std.debug.print("{}\n", .{map});

    const startDir: Direction = blk: for ([_]Direction{ .North, .West, .East, .South }) |dir| {
        const startingTile = try map.get(map.start);
        if (startingTile.canMoveTo(dir)) {
            break :blk dir;
        }
    } else {
        unreachable;
    };

    std.debug.print("Start by going {}\n", .{startDir});

    var ite = Iterator.initFromStart(&map, startDir);
    var loopCoords = CoordsSet.init(allocator);
    defer loopCoords.deinit();

    try loopCoords.put(map.start, {});
    while (ite.next()) |c| {
        try loopCoords.put(c, {});
    }

    const inner = try countInnerTiles(&map, &loopCoords);

    const badAnswers = [_]u64{
        263, // too low
    };

    std.debug.print("Loop length: {}\n", .{loopCoords.count()});
    std.debug.print("Result: {}\n", .{(loopCoords.count() + 1) / 2});

    if (std.mem.indexOfScalar(u64, &badAnswers, inner)) |_| {
        std.debug.print("{} is not the correct answer :(\n", .{inner});
    }
    std.debug.print("Inside: {}\n", .{inner});
}
