const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Coords = struct {
    x: u64,
    y: u64,

    pub fn format(self: *const Coords, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "(x={d:3}, y={d:3})", .{ self.x, self.y });
    }

    pub fn distance(self: *const Coords, other: *const Coords) u64 {
        const xDiff = if (self.x > other.x) (self.x - other.x) else (other.x - self.x);
        const yDiff = if (self.y > other.y) (self.y - other.y) else (other.y - self.y);

        return xDiff + yDiff;
    }
};

const CoordsList = std.ArrayList(Coords);

fn readGalaxies(reader: std.fs.File.Reader, galaxies: *CoordsList) !void {
    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);
    var lineNum: u64 = 0;
    var lineLen: usize = 0;
    var colFlags = [_]bool{false} ** 512;
    var colOffsets = [_]u64{0} ** 512;

    while (true) : (lineNum += 1) {
        fbs.reset();
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        const line = fbs.getWritten();
        var hasGalaxies = false;

        lineLen = line.len;
        for (line, 0..) |c, i| {
            if (c == '#') {
                const g = Coords{ .x = i, .y = lineNum };
                // std.debug.print("{}\n", .{g});
                try galaxies.append(g);
                colFlags[i] = true;
                hasGalaxies = true;
            }
        }

        if (!hasGalaxies) {
            lineNum += 1;
        }
    }

    var acc: u64 = 0;
    for (0..lineLen) |i| {
        if (!colFlags[i]) {
            acc += 1;
        }
        // std.debug.print("col {} has offset {}\n", .{ i, acc });
        colOffsets[i] = acc;
    }

    for (galaxies.items, 0..) |g, i| {
        const newG = Coords{ .x = g.x + colOffsets[g.x], .y = g.y };
        // std.debug.print("Expanding  {s} to {s}\n", .{ g, newG });
        galaxies.items[i] = newG;
    }
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const reader = try getReader();

    var galaxies = CoordsList.init(allocator);
    defer galaxies.deinit();

    try readGalaxies(reader, &galaxies);
    // std.debug.print("Expanded galaxies: {s}\n", .{galaxies.items});

    var total: u64 = 0;
    for (galaxies.items[0..(galaxies.items.len - 1)], 0..) |g, i| {
        std.debug.print("Distance from {s} ({}):\n", .{ g, i + 1 });

        for (galaxies.items[(i + 1)..galaxies.items.len], (i + 2)..) |h, j| {
            const d = g.distance(&h);
            std.debug.print("    {}. {s} => {d:4}\n", .{ j, h, d });
            total += g.distance(&h);
        }
    }
    std.debug.print("Total distance: {}\n", .{total});
}
