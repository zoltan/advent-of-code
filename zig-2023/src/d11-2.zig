const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Coords = struct {
    x: u64,
    y: u64,

    pub fn format(self: *const Coords, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "(x={d:3}, y={d:3})", .{ self.x, self.y });
    }

    pub fn distance(self: *const Coords, other: *const Coords) u64 {
        const xDiff = if (self.x > other.x) (self.x - other.x) else (other.x - self.x);
        const yDiff = if (self.y > other.y) (self.y - other.y) else (other.y - self.y);

        return xDiff + yDiff;
    }
};

const CoordsList = std.ArrayList(Coords);

fn readGalaxies(reader: std.fs.File.Reader, galaxies: *CoordsList, expandSize: u64) !void {
    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);
    var lineNum: u64 = 0;
    var lineLen: usize = 0;
    // input has 140 lines and 140 columns
    var colFlags = [_]bool{false} ** 256;
    var colOffsets = [_]u64{0} ** 256;

    while (true) : (lineNum += 1) {
        fbs.reset();
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        const line = fbs.getWritten();
        var hasGalaxies = false;

        lineLen = line.len;
        for (line, 0..) |c, i| {
            if (c == '#') {
                const g = Coords{ .x = i, .y = lineNum };
                try galaxies.append(g);
                colFlags[i] = true;
                hasGalaxies = true;
            }
        }

        if (!hasGalaxies) {
            lineNum += expandSize;
        }
    }

    var acc: u64 = 0;
    for (0..lineLen) |i| {
        if (!colFlags[i]) {
            acc += expandSize;
        }
        colOffsets[i] = acc;
    }

    for (galaxies.items, 0..) |g, i| {
        const newG = Coords{ .x = g.x + colOffsets[g.x], .y = g.y };
        galaxies.items[i] = newG;
    }
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const reader = try getReader();

    var galaxies = CoordsList.init(allocator);
    defer galaxies.deinit();

    try readGalaxies(reader, &galaxies, 1000000 - 1);

    var total: u64 = 0;
    for (galaxies.items[0..(galaxies.items.len - 1)], 0..) |g, i| {
        for (galaxies.items[(i + 1)..galaxies.items.len]) |h| {
            total += g.distance(&h);
        }
    }
    std.debug.print("Total distance: {}\n", .{total});
}
