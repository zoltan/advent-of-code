const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn isValid(symbols: []const u8, numbers: []const u8) bool {
    if (std.mem.indexOfScalar(u8, symbols, '?')) |_| {
        return false;
    }

    var ite = std.mem.tokenizeScalar(u8, symbols, '.');
    var i: usize = 0;
    while (ite.next()) |token| : (i += 1) {
        if (numbers.len <= i or token.len != numbers[i]) {
            return false;
        }
    }
    return (i == numbers.len);
}

test isValid {
    try testing.expect(isValid("...", &[_]u8{}));
    try testing.expect(isValid("#..", &[_]u8{1}));
    try testing.expect(isValid(".#.", &[_]u8{1}));
    try testing.expect(isValid("..#", &[_]u8{1}));
    try testing.expect(isValid("#.#", &[_]u8{ 1, 1 }));
    try testing.expect(isValid(".#.#.", &[_]u8{ 1, 1 }));
    try testing.expect(isValid(".##.", &[_]u8{2}));
    try testing.expect(isValid("##..", &[_]u8{2}));
    try testing.expect(isValid("##.#.", &[_]u8{ 2, 1 }));
    try testing.expect(isValid("#.##.", &[_]u8{ 1, 2 }));
    try testing.expect(isValid("#.#.###", &[_]u8{ 1, 1, 3 }));

    // examples
    try testing.expect(isValid("#.#.###", &[_]u8{ 1, 1, 3 }));
    try testing.expect(isValid(".#...#....###.", &[_]u8{ 1, 1, 3 }));
    try testing.expect(isValid(".#.###.#.######", &[_]u8{ 1, 3, 1, 6 }));
    try testing.expect(isValid("####.#...#...", &[_]u8{ 4, 1, 1 }));
    try testing.expect(isValid("#....######..#####.", &[_]u8{ 1, 6, 5 }));
    try testing.expect(isValid(".###.##....#", &[_]u8{ 3, 2, 1 }));
}

fn backtrack(symbols: []u8, numbers: []const u8, start: usize) u64 {
    if (start == symbols.len) {
        return if (isValid(symbols, numbers)) 1 else 0;
    }

    // std.debug.print(">> backtrack {s} {d} [{s}]\n", .{ symbols, start, symbols[start..symbols.len] });
    if (std.mem.indexOfScalar(u8, symbols[start..symbols.len], '?')) |pos| {
        var total: u64 = 0;
        symbols[start + pos] = '#';
        total += backtrack(symbols, numbers, start + pos + 1);
        symbols[start + pos] = '.';
        total += backtrack(symbols, numbers, start + pos + 1);
        symbols[start + pos] = '?';
        return total;
    } else {
        return if (isValid(symbols, numbers)) 1 else 0;
    }
}

test backtrack {
    var allocator = std.testing.allocator;

    const defs: [8]struct { []const u8, []const u8, u64 } = .{
        .{ "?.?", &[_]u8{1}, 2 },
        .{ "???.###", &[_]u8{ 1, 1, 3 }, 1 },
        .{ "???.###", &[_]u8{ 1, 1, 3 }, 1 },
        .{ ".??..??...?##.", &[_]u8{ 1, 1, 3 }, 4 },
        .{ "?#?#?#?#?#?#?#?", &[_]u8{ 1, 3, 1, 6 }, 1 },
        .{ "????.#...#...", &[_]u8{ 4, 1, 1 }, 1 },
        .{ "????.######..#####.", &[_]u8{ 1, 6, 5 }, 4 },
        .{ "?###????????", &[_]u8{ 3, 2, 1 }, 10 },
    };

    for (defs) |def| {
        const buffer = try allocator.dupe(u8, def[0]);
        defer allocator.free(buffer);

        try testing.expectEqual(def[2], backtrack(buffer, def[1], 0));
    }
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();
    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);
    var total: u64 = 0;
    while (true) {
        fbs.reset();
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        var numbers = std.ArrayList(u8).init(allocator);
        defer numbers.deinit();

        var line = fbs.getWritten();

        const space = std.mem.indexOfScalar(u8, line, ' ').?;
        var ite = std.mem.tokenizeScalar(u8, line[space + 1 .. line.len], ',');
        while (ite.next()) |n| {
            try numbers.append(try std.fmt.parseInt(u8, n, 10));
        }

        const symbols = try allocator.dupe(u8, line[0..space]);
        defer allocator.free(symbols);
        const options = backtrack(symbols, numbers.items, 0);
        // std.debug.print(">>> line {s} has {} options\n", .{ line, options });
        total += options;
    }
    std.debug.print("Total: {}\n", .{total});
}
