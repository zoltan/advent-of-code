const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Map = struct {
    allocator: std.mem.Allocator,
    sLines: std.ArrayList([]const u8),
    nLines: std.ArrayList(u64),
    nColumns: std.ArrayList(u64),

    pub fn init(allocator: std.mem.Allocator) Map {
        return Map{
            .allocator = allocator,
            .sLines = std.ArrayList([]const u8).init(allocator),
            .nLines = std.ArrayList(u64).init(allocator),
            .nColumns = std.ArrayList(u64).init(allocator),
        };
    }

    pub fn deinit(self: *Map) void {
        self.sLines.deinit();
        self.nLines.deinit();
        self.nColumns.deinit();
    }

    pub fn addLine(self: *Map, line: []const u8) !void {
        try self.sLines.append(line);
        try self.nLines.append(hashPatern(line));

        if (self.nColumns.items.len == 0) {
            for (0..line.len) |_| {
                try self.nColumns.append(0);
            }
        }

        for (line, 0..) |c, i| {
            var n = self.nColumns.items[i];
            n = @shlExact(n, 1);
            if (c == '#') {
                n = n | 1;
            }
            self.nColumns.items[i] = n;
        }
    }

    pub fn getTotal(self: *const Map) u64 {
        if (findSymetry(self.nLines.items)) |i| {
            // std.debug.print("Found symetry in lines: {d}\n", .{i});
            return i * 100;
        } else if (findSymetry(self.nColumns.items)) |i| {
            // std.debug.print("Found symetry in columns: {d}\n", .{i});
            return i;
        } else {
            unreachable;
        }
    }
};

fn hashPatern(s: []const u8) u64 {
    var n: u64 = 0;
    for (s) |c| {
        n = @shlExact(n, 1);
        if (c == '#') {
            n = n | 1;
        }
    }
    return n;
}

test hashPatern {
    try testing.expectEqual(@as(u64, 0), hashPatern("..."));
    try testing.expectEqual(@as(u64, 1), hashPatern("..#"));
    try testing.expectEqual(@as(u64, 2), hashPatern(".#."));
    try testing.expectEqual(@as(u64, 3), hashPatern(".##"));
    try testing.expectEqual(@as(u64, 4), hashPatern("#.."));
    try testing.expectEqual(@as(u64, 5), hashPatern("#.#"));
}

fn isOneBitAway(a: u64, b: u64) bool {
    return @popCount(a ^ b) == 1;
}

fn isSymetric(items: []const u64, start: u64) bool {
    var fixed = false;

    if (start <= items.len / 2) {
        for (0..start) |i| {
            const l = items[start - i - 1];
            const r = items[start + i];

            if (fixed and l != r) {
                return false;
            } else if (l != r and isOneBitAway(l, r)) {
                fixed = true;
            }
        }
    } else {
        for (0..(items.len - start)) |i| {
            const l = items[start - i - 1];
            const r = items[start + i];

            if (fixed and l != r) {
                return false;
            } else if (l != r and isOneBitAway(l, r)) {
                fixed = true;
            }
        }
    }
    return fixed;
}

test isSymetric {
    try testing.expect(!isSymetric(&[_]u64{ 1, 2, 3, 3, 2, 1 }, 3));
    try testing.expect(!isSymetric(&[_]u64{ 1, 2, 3, 3, 2, 1, 0, 0, 0 }, 3));

    try testing.expect(!isSymetric(&[_]u64{ 1, 2, 3, 3, 2, 1, 0, 0, 0 }, 1));
    try testing.expect(!isSymetric(&[_]u64{ 1, 2, 3, 3, 2, 1, 0, 0, 0 }, 2));
    try testing.expect(isSymetric(&[_]u64{ 3, 2, 3, 3, 2, 1, 0, 0, 0 }, 3));

    // 1st example grid
    try testing.expect(isSymetric(&[_]u64{ 358, 90, 385, 385, 90, 102, 346 }, 3));

    // 2nd example grid
    try testing.expect(!isSymetric(&[_]u64{ 281, 265, 103, 502, 502, 103, 265 }, 4));
}

fn findSymetry(items: []const u64) ?usize {
    if (items.len == 0) {
        return 0;
    }

    for (1..items.len) |i| {
        const l = items[i - 1];
        const r = items[i];
        if ((l == r or isOneBitAway(l, r)) and isSymetric(items, i)) {
            return i;
        }
    }
    return null;
}

test findSymetry {

    // 1st example grid
    try testing.expectEqual(@as(?u64, 3), findSymetry(&[_]u64{ 358, 90, 385, 385, 90, 102, 346 }));
    try testing.expectEqual(@as(?u64, null), findSymetry(&[_]u64{ 89, 24, 103, 66, 37, 37, 66, 103, 24 }));

    // // 2nd example grid
    try testing.expectEqual(@as(?u64, 1), findSymetry(&[_]u64{ 281, 265, 103, 502, 502, 103, 265 }));
    try testing.expectEqual(@as(?u64, null), findSymetry(&[_]u64{ 109, 12, 30, 30, 76, 97, 30, 30, 115 }));
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);
    var map = Map.init(allocator);
    defer map.deinit();
    var total: u64 = 0;

    while (true) {
        fbs.reset();
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        const line = fbs.getWritten();

        if (line.len > 0) {
            try map.addLine(line);
        } else {
            total += map.getTotal();
            map.deinit();
            map = Map.init(allocator);
        }
    }
    total += map.getTotal();
    std.debug.print("Total: {}\n", .{total});
}
