const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Map = struct {
    size: usize,
    data: [100][100]u8,

    pub fn fromFile(reader: std.fs.File.Reader) Map {
        var map = Map{ .size = 0, .data = undefined };

        var readBuffer: [512:0]u8 = undefined;
        var fbs = std.io.fixedBufferStream(&readBuffer);
        while (true) : (map.size += 1) {
            fbs.reset();
            reader.streamUntilDelimiter(fbs.writer(), '\n', readBuffer.len) catch {
                break;
            };
            std.mem.copyForwards(u8, &map.data[map.size], fbs.getWritten());
        }

        return map;
    }

    pub fn T(self: *Map) void {
        for (0..self.size) |i| {
            for (i..self.size) |j| {
                const t = self.data[i][j];
                self.data[i][j] = self.data[j][i];
                self.data[j][i] = t;
            }
        }
    }
};

fn moveToFront(row: []u8) void {
    const end = std.mem.indexOfScalar(u8, row, '#') orelse row.len;
    const rocks = std.mem.count(u8, row[0..end], "O");

    for (0..rocks) |i| {
        row[i] = 'O';
    }
    for (rocks..end) |i| {
        row[i] = '.';
    }

    if (end < row.len) {
        moveToFront(row[end + 1 .. row.len]);
    }
}

test moveToFront {
    var allocator = std.testing.allocator;
    const results = [_]struct { []const u8, []const u8 }{
        .{ "OO.O.O..##", "OOOO....##" },
        .{ "...OO....O", "OOO......." },
        .{ "#.#..O#.##", "#.#O..#.##" },
        .{ "..O..#O..O", "O....#OO.." },
    };

    for (results) |r| {
        const buf = try allocator.dupe(u8, r[0]);
        defer allocator.free(buf);

        moveToFront(buf);
        std.debug.print("Testing: [{s}] => [{s}]\n", .{ r[0], buf });
        try std.testing.expect(std.mem.eql(u8, r[1], buf));
    }
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();
    _ = allocator;

    const reader = try getReader();

    var map = Map.fromFile(reader);

    for (0..map.size) |i| {
        std.debug.print("{d:3} >> {s}\n", .{ i, map.data[i] });
    }

    map.T();
    for (0..map.size) |i| {
        moveToFront(map.data[i][0..map.size]);
    }
    map.T();

    std.debug.print("--------------------------------------------------------------------------------\n", .{});

    var total: usize = 0;
    for (0..map.size) |i| {
        total += (map.size - i) * std.mem.count(u8, map.data[i][0..map.size], "O");
        std.debug.print("{d:3} total: {d:4} >> {s}\n", .{ i, total, map.data[i] });
    }
    std.debug.print("Total: {d}\n", .{total});
}
