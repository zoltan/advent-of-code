const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Direction = enum {
    North,
    East,
    South,
    West,
};

const Map = struct {
    size: usize,
    data: [100][100]u8,
    transposed: bool = false,

    pub fn fromFile(reader: std.fs.File.Reader) Map {
        var map = Map{ .size = 0, .data = undefined };

        var readBuffer: [512:0]u8 = undefined;
        var fbs = std.io.fixedBufferStream(&readBuffer);
        while (true) : (map.size += 1) {
            fbs.reset();
            reader.streamUntilDelimiter(fbs.writer(), '\n', readBuffer.len) catch {
                break;
            };
            std.mem.copyForwards(u8, &map.data[map.size], fbs.getWritten());
        }

        return map;
    }

    pub fn dupe(self: *const Map, allocator: std.mem.Allocator) !*Map {
        var map: *Map = try allocator.create(Map);

        map.size = self.size;
        map.transposed = self.transposed;
        for (self.data, 0..) |r, i| {
            @memcpy(&map.data[i], &r);
        }
        return map;
    }

    pub fn eql(self: *const Map, other: *const Map) bool {
        if (self.size != other.size) {
            return false;
        } else if (self.transposed != other.transposed) {
            return false;
        }

        for (self.data, other.data) |r1, r2| {
            if (!std.mem.eql(u8, &r1, &r2)) {
                return false;
            }
        }
        return true;
    }

    pub fn tilt(self: *Map, direction: Direction) void {
        _ = switch (direction) {
            .North => blk: {
                if (!self.transposed) {
                    self.T();
                }
                for (0..self.size) |i| {
                    moveToFront(self.data[i][0..self.size]);
                }
                break :blk {};
            },
            .South => blk: {
                if (!self.transposed) {
                    self.T();
                }
                for (0..self.size) |i| {
                    moveToBack(self.data[i][0..self.size]);
                }
                break :blk {};
            },
            .West => blk: {
                if (self.transposed) {
                    self.T();
                }
                for (0..self.size) |i| {
                    moveToFront(self.data[i][0..self.size]);
                }
                break :blk {};
            },
            .East => blk: {
                if (self.transposed) {
                    self.T();
                }
                for (0..self.size) |i| {
                    moveToBack(self.data[i][0..self.size]);
                }
                break :blk {};
            },
        };
    }

    fn T(self: *Map) void {
        self.transposed = !self.transposed;
        for (0..self.size) |i| {
            for (i..self.size) |j| {
                const t = self.data[i][j];
                self.data[i][j] = self.data[j][i];
                self.data[j][i] = t;
            }
        }
    }
};

fn moveToFront(row: []u8) void {
    const end = std.mem.indexOfScalar(u8, row, '#') orelse row.len;
    const rocks = std.mem.count(u8, row[0..end], "O");

    for (0..rocks) |i| {
        row[i] = 'O';
    }
    for (rocks..end) |i| {
        row[i] = '.';
    }

    if (end < row.len) {
        moveToFront(row[end + 1 .. row.len]);
    }
}

test moveToFront {
    var allocator = std.testing.allocator;
    const results = [_]struct { []const u8, []const u8 }{
        .{ "OO.O.O..##", "OOOO....##" },
        .{ "...OO....O", "OOO......." },
        .{ "#.#..O#.##", "#.#O..#.##" },
        .{ "..O..#O..O", "O....#OO.." },
    };

    for (results) |r| {
        const buf = try allocator.dupe(u8, r[0]);
        defer allocator.free(buf);

        moveToFront(buf);
        std.debug.print("Testing: [{s}] => [{s}]\n", .{ r[0], buf });
        try std.testing.expect(std.mem.eql(u8, r[1], buf));
    }
}

pub fn moveToBack(row: []u8) void {
    const end = std.mem.indexOfScalar(u8, row, '#') orelse row.len;
    const rocks = std.mem.count(u8, row[0..end], "O");

    for (0..end - rocks) |i| {
        row[i] = '.';
    }
    for (end - rocks..end) |i| {
        row[i] = 'O';
    }

    if (end < row.len) {
        moveToBack(row[end + 1 .. row.len]);
    }
}

test moveToBack {
    var allocator = std.testing.allocator;
    const results = [_]struct { []const u8, []const u8 }{
        .{ "OO.O.O..##", "....OOOO##" },
        .{ "...OO....O", ".......OOO" },
        .{ "#.#..O#.##", "#.#..O#.##" },
        .{ "..O..#O..O", "....O#..OO" },
    };

    for (results) |r| {
        const buf = try allocator.dupe(u8, r[0]);
        defer allocator.free(buf);

        moveToBack(buf);
        std.debug.print("Testing: [{s}] => [{s}]\n", .{ r[0], buf });
        try std.testing.expect(std.mem.eql(u8, r[1], buf));
    }
}

fn cycle(comptime T: type) type {
    return struct {
        const Self = @This();

        values: []const T,
        index: usize,

        pub fn init(values: []const T) Self {
            return Self{
                .values = values,
                .index = 0,
            };
        }

        pub fn next(self: *@This()) Direction {
            const ret = self.values[self.index];
            self.index += 1;
            if (self.index == self.values.len) {
                self.index = 0;
            }
            return ret;
        }
    };
}

fn lastIndexOf(maps: []*const Map, m: *const Map) ?usize {
    const len = maps.len;
    if (maps.len == 0) {
        return null;
    }
    for (1..maps.len) |i| {
        if (m.eql(maps[len - i])) {
            return i;
        }
    }
    return null;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const reader = try getReader();

    var map = Map.fromFile(reader);
    var mapList = std.ArrayList(*const Map).init(allocator);

    for (0..map.size) |i| {
        std.debug.print("{d:3} >> {s}\n", .{ i, map.data[i] });
    }

    std.debug.print("--------------------------------------------------------------------------------\n", .{});

    var ite = cycle(Direction).init(&[_]Direction{ .North, .West, .South, .East });

    var n: usize = 0;
    var loopLen: usize = 0;
    while (n < 1_000_000_000) : (n += 1) {
        if (n % 10 == 0) {
            std.debug.print("On iteration {d:0>10} ({d:.3}%)\n", .{ n, @as(f64, @bitCast(n)) / 1000000000.0 });
        }

        map.tilt(ite.next());
        map.tilt(ite.next());
        map.tilt(ite.next());
        map.tilt(ite.next());
        if (lastIndexOf(mapList.items, &map)) |pos| {
            std.debug.print("n={d:10}: found duplicate at position {d}\n", .{ n, pos });
            loopLen = pos;
            break;
        }
        var copy = try map.dupe(allocator);
        if (copy.transposed) {
            copy.T();
        }
        try mapList.append(copy);
    }

    for (1..(1_000_000_000 - n) % loopLen) |_| {
        map.tilt(ite.next());
        map.tilt(ite.next());
        map.tilt(ite.next());
        map.tilt(ite.next());
    }

    var total: usize = 0;
    for (0..map.size) |i| {
        total += (map.size - i) * std.mem.count(u8, map.data[i][0..map.size], "O");
        std.debug.print("{d:3} total: {d:4} >> {s}\n", .{ i, total, map.data[i] });
    }
    std.debug.print("{d} Total: {d}\n", .{ n, total });
}
