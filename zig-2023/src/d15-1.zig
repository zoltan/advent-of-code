const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn hash(s: []const u8) u64 {
    var total: u64 = 0;
    for (s) |c| {
        total += c;
        total = (total * 17) % 256;
    }
    return total;
}

test hash {
    try testing.expectEqual(@as(u64, 30), hash("rn=1"));
    try testing.expectEqual(@as(u64, 253), hash("cm-"));
    try testing.expectEqual(@as(u64, 97), hash("qp=3"));
    try testing.expectEqual(@as(u64, 47), hash("cm=2"));
    try testing.expectEqual(@as(u64, 14), hash("qp-"));
    try testing.expectEqual(@as(u64, 180), hash("pc=4"));
    try testing.expectEqual(@as(u64, 9), hash("ot=9"));
    try testing.expectEqual(@as(u64, 197), hash("ab=5"));
    try testing.expectEqual(@as(u64, 48), hash("pc-"));
    try testing.expectEqual(@as(u64, 214), hash("pc=6"));
    try testing.expectEqual(@as(u64, 231), hash("ot=7"));
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();
    _ = allocator;

    var reader = try getReader();
    var readBuffer: [25000:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&readBuffer);
    fbs.reset();
    try reader.streamUntilDelimiter(fbs.writer(), '\n', readBuffer.len);
    const line = fbs.getWritten();
    var ite = std.mem.tokenizeScalar(u8, line, ',');
    var total: u64 = 0;
    while (ite.next()) |tok| {
        total += hash(tok);
    }
    std.debug.print("Total: {d}\n", .{total});
}
