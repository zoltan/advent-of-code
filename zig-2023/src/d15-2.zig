const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn hash(s: []const u8) u64 {
    var total: u64 = 0;
    for (s) |c| {
        total += c;
        total = (total * 17) % 256;
    }
    return total;
}

test hash {
    try testing.expectEqual(@as(u64, 30), hash("rn=1"));
    try testing.expectEqual(@as(u64, 253), hash("cm-"));
    try testing.expectEqual(@as(u64, 97), hash("qp=3"));
    try testing.expectEqual(@as(u64, 47), hash("cm=2"));
    try testing.expectEqual(@as(u64, 14), hash("qp-"));
    try testing.expectEqual(@as(u64, 180), hash("pc=4"));
    try testing.expectEqual(@as(u64, 9), hash("ot=9"));
    try testing.expectEqual(@as(u64, 197), hash("ab=5"));
    try testing.expectEqual(@as(u64, 48), hash("pc-"));
    try testing.expectEqual(@as(u64, 214), hash("pc=6"));
    try testing.expectEqual(@as(u64, 231), hash("ot=7"));
}

const Lens = struct {
    label: []const u8,
    focalLength: u8,

    pub fn format(self: *const Lens, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "({s}/{d})", .{ self.label, self.focalLength });
    }
};

const Box = struct {
    lenses: std.ArrayList(Lens),

    pub fn init(allocator: std.mem.Allocator) Box {
        return Box{ .lenses = std.ArrayList(Lens).init(allocator) };
    }

    fn findLens(self: *const Box, label: []const u8) ?usize {
        for (self.lenses.items, 0..) |lens, i| {
            if (std.mem.eql(u8, lens.label, label)) {
                return i;
            }
        }
        return null;
    }

    pub fn removeLens(self: *Box, label: []const u8) void {
        if (self.findLens(label)) |pos| {
            _ = self.lenses.orderedRemove(pos);
        }
    }

    pub fn addLens(self: *Box, label: []const u8, focalLength: u8) !void {
        if (self.findLens(label)) |pos| {
            self.lenses.items[pos].focalLength = focalLength;
        } else {
            try self.lenses.append(Lens{ .label = label, .focalLength = focalLength });
        }
    }
};

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();
    var readBuffer: [25000:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&readBuffer);
    fbs.reset();
    try reader.streamUntilDelimiter(fbs.writer(), '\n', readBuffer.len);
    const line = fbs.getWritten();
    var ite = std.mem.tokenizeScalar(u8, line, ',');

    var boxes: [256]Box = undefined;

    for (0..boxes.len) |i| {
        boxes[i] = Box.init(allocator);
    }

    while (ite.next()) |tok| {
        const sep = std.mem.indexOfAny(u8, tok, "-=").?;
        const label = tok[0..sep];
        const boxIndex = hash(label);
        if (tok[sep] == '-') {
            boxes[boxIndex].removeLens(label);
        } else {
            const focalLen = try std.fmt.parseInt(u8, tok[sep + 1 .. tok.len], 10);
            try boxes[boxIndex].addLens(label, focalLen);
        }
    }

    var total: u64 = 0;
    for (boxes, 1..) |box, i| {
        // std.debug.print("{d:3} >>> {s}\n", .{ i, box.lenses.items });
        for (box.lenses.items, 1..) |lens, j| {
            total += i * j * lens.focalLength;
        }
    }
    std.debug.print("Total: {d}\n", .{total});
}
