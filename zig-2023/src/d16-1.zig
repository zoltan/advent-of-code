const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn readMap(allocator: std.mem.Allocator, reader: std.fs.File.Reader) ![][]u8 {
    var readBuffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&readBuffer);

    try reader.streamUntilDelimiter(fbs.writer(), '\n', readBuffer.len);
    const line = fbs.getWritten();
    var map = try allocator.alloc([]u8, line.len);
    map[0] = try allocator.dupe(u8, line);
    for (1..map.len) |i| {
        fbs.reset();
        try reader.streamUntilDelimiter(fbs.writer(), '\n', readBuffer.len);
        map[i] = try allocator.dupe(u8, fbs.getWritten());
    }
    return map;
}

const Direction = enum {
    North,
    East,
    South,
    West,

    pub fn opposite(self: *const Direction) Direction {
        return switch (self.*) {
            .North => .South,
            .South => .North,
            .East => .West,
            .West => .East,
        };
    }
};

const Point = struct {
    x: usize,
    y: usize,
};

const Ray = struct {
    direction: Direction,
    origin: Point,
};

const RayList = std.ArrayList(Ray);

fn makeRays(rays: *RayList, p: Point, symbol: u8, d: Direction) !void {
    // std.debug.print("  making rays at {} ({c}) coming from {}\n", .{ p, symbol, d });
    switch (symbol) {
        '-' => {
            if (d == .West or d == .East) unreachable;
            try rays.append(Ray{
                .direction = .West,
                .origin = p,
            });
            // std.debug.print("    - add West\n", .{});
            try rays.append(Ray{
                .direction = .East,
                .origin = p,
            });
            // std.debug.print("    - add East\n", .{});
        },
        '|' => {
            if (d == .North or d == .South) unreachable;
            try rays.append(Ray{
                .direction = .North,
                .origin = p,
            });
            // std.debug.print("    - add North\n", .{});
            try rays.append(Ray{
                .direction = .South,
                .origin = p,
            });
            // std.debug.print("    - add South\n", .{});
        },
        '/' => {
            try rays.append(Ray{
                .direction = switch (d) {
                    .North => .West,
                    .West => .North,
                    .South => .East,
                    .East => .South,
                },
                .origin = p,
            });
            // std.debug.print("    - add {s}\n", .{switch (d) {
            //     .North => "West",
            //     .West => "North",
            //     .South => "East",
            //     .East => "South",
            // }});
        },
        '\\' => {
            try rays.append(Ray{
                .direction = switch (d) {
                    .North => .East,
                    .East => .North,
                    .South => .West,
                    .West => .South,
                },
                .origin = p,
            });
            // std.debug.print("    - add {s}\n", .{switch (d) {
            //     .North => "East",
            //     .East => "North",
            //     .South => "West",
            //     .West => "South",
            // }});
        },
        else => unreachable,
    }
}

fn raycast(map: [][]const u8, visited: [][]bool, rays: *RayList, ray: Ray) !void {
    switch (ray.direction) {
        .North => {
            if (ray.origin.y == 0) return;
            var i = ray.origin.y - 1;
            while (i > 0) : (i -= 1) {
                visited[i][ray.origin.x] = true;
                const p = map[i][ray.origin.x];
                // std.debug.print("  👁️ looking at {d}:{d} => {c}\n", .{ ray.origin.x, i, p });
                if (p != '.' and p != '|') {
                    try makeRays(rays, Point{ .x = ray.origin.x, .y = i }, p, ray.direction.opposite());
                    break;
                }
            } else {
                visited[i][ray.origin.x] = true;
                const p = map[i][ray.origin.x];
                if (p != '.' and p != '|') {
                    try makeRays(rays, Point{ .x = ray.origin.x, .y = i }, p, ray.direction.opposite());
                }
            }
        },
        .South => {
            var i = ray.origin.y + 1;
            while (i < map.len) : (i += 1) {
                visited[i][ray.origin.x] = true;
                const p = map[i][ray.origin.x];
                // std.debug.print("  👁️ looking at {d}:{d} => {c}\n", .{ ray.origin.x, i, p });
                if (p != '.' and p != '|') {
                    try makeRays(rays, Point{ .x = ray.origin.x, .y = i }, p, ray.direction.opposite());
                    break;
                }
            }
        },
        .East => {
            for (ray.origin.x + 1..map[ray.origin.y].len) |i| {
                visited[ray.origin.y][i] = true;
                const p = map[ray.origin.y][i];
                // std.debug.print("  👁️ looking at {d}:{d} => {c}\n", .{ i, ray.origin.y, p });
                if (p != '.' and p != '-') {
                    try makeRays(rays, Point{ .x = i, .y = ray.origin.y }, p, ray.direction.opposite());
                    break;
                }
            }
        },
        .West => {
            if (ray.origin.x == 0) return;
            var i = ray.origin.x - 1;
            while (i > 0) : (i -= 1) {
                visited[ray.origin.y][i] = true;
                const p = map[ray.origin.y][i];
                // std.debug.print("  👁️ looking at {d}:{d} => {c}\n", .{ i, ray.origin.y, p });
                if (p != '.' and p != '-') {
                    try makeRays(rays, Point{ .x = i, .y = ray.origin.y }, p, ray.direction.opposite());
                    break;
                }
            } else {
                visited[ray.origin.y][i] = true;
                const p = map[ray.origin.y][i];
                if (p != '.' and p != '-') {
                    try makeRays(rays, Point{ .x = i, .y = ray.origin.y }, p, ray.direction.opposite());
                }
            }
        },
    }
}

fn printMap(map: [][]const u8, p: Point) void {
    for (map, 0..) |row, i| {
        if (i == p.y) {
            std.debug.print("{d:4}: ", .{i});
            for (row, 0..) |c, j| {
                std.debug.print("{c}", .{if (j == p.x) '#' else c});
            }
            std.debug.print("\n", .{});
        } else {
            std.debug.print("{d:4}: {s}\n", .{ i, row });
        }
    }
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const reader = try getReader();
    const map = try readMap(allocator, reader);
    var visited = try allocator.alloc([]bool, map.len);
    for (0..visited.len) |i| {
        visited[i] = try allocator.alloc(bool, map[i].len);
        for (0..visited[i].len) |j| {
            visited[i][j] = false;
        }
    }

    var rays = RayList.init(allocator);
    if (map[0][0] != '.' and map[0][0] != '-') {
        try makeRays(&rays, Point{ .x = 0, .y = 0 }, map[0][0], .West);
    } else {
        try rays.append(Ray{
            .direction = .East,
            .origin = Point{ .x = 0, .y = 0 },
        });
    }
    visited[0][0] = true;
    var knownRays = std.AutoHashMap(Ray, void).init(allocator);

    while (rays.popOrNull()) |ray| {
        // std.debug.print("Ray: {}\n", .{ray});
        // printMap(map, ray.origin);
        // if (rays.items.len > 0) {
        //     for (0..rays.items.len) |i| {
        //         std.debug.print("  {}\n", .{rays.items[i]});
        //     }
        // } else {
        //     std.debug.print("  no other ray\n", .{});
        // }
        if (knownRays.get(ray)) |_| {} else {
            try knownRays.put(ray, {});
            try raycast(map, visited, &rays, ray);
        }
    }

    var total: usize = 0;
    for (visited, 0..) |row, i| {
        _ = i;
        // std.debug.print("{d:4}: ", .{i});
        for (row) |c| {
            if (c) {
                // std.debug.print("#", .{});
                total += 1;
            } else {
                // std.debug.print(".", .{});
            }
        }
        // std.debug.print("\n", .{});
    }
    std.debug.print("Total: {d}\n", .{total});

    const wrong = [_]usize{
        7913, // too low
    };

    if (std.mem.indexOfScalar(usize, &wrong, total)) |_| {
        std.debug.print("{d} is not the right answer\n", .{total});
    }
}
