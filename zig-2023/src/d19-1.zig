const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
    NotATransition,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const xMas = struct {
    x: u64,
    m: u64,
    a: u64,
    s: u64,

    pub fn fromLine(str: []const u8) !xMas {
        var ite = std.mem.tokenizeScalar(u8, str[1 .. str.len - 1], ',');

        const x = ite.next().?;
        const m = ite.next().?;
        const a = ite.next().?;
        const s = ite.next().?;

        return xMas{
            .x = try std.fmt.parseInt(u64, x[2..x.len], 10),
            .m = try std.fmt.parseInt(u64, m[2..m.len], 10),
            .a = try std.fmt.parseInt(u64, a[2..a.len], 10),
            .s = try std.fmt.parseInt(u64, s[2..s.len], 10),
        };
    }

    pub fn get(self: *const xMas, c: Category) u64 {
        return switch (c) {
            .X => self.x,
            .M => self.m,
            .A => self.a,
            .S => self.s,
        };
    }

    pub fn total(self: *const xMas) u64 {
        return self.x + self.m + self.a + self.s;
    }
};

const xMasList = std.ArrayList(xMas);

const Category = enum {
    X,
    M,
    A,
    S,

    pub fn fromChar(c: u8) Category {
        return switch (c) {
            'x' => .X,
            'm' => .M,
            'a' => .A,
            's' => .S,
            else => unreachable,
        };
    }
};

const CmpOperator = enum {
    LessThan,
    GreaterThan,

    pub fn fromChar(c: u8) CmpOperator {
        return switch (c) {
            '<' => .LessThan,
            '>' => .GreaterThan,
            else => unreachable,
        };
    }
};

const Transition = struct {
    category: Category,
    op: CmpOperator,
    value: u64,
    dest: []const u8,

    pub fn fromString(allocator: std.mem.Allocator, str: []const u8) !Transition {
        const colon = std.mem.indexOfScalar(u8, str, ':') orelse {
            return AOCError.NotATransition;
        };
        return Transition{
            .category = Category.fromChar(str[0]),
            .op = CmpOperator.fromChar(str[1]),
            .value = try std.fmt.parseInt(u64, str[2..colon], 10),
            .dest = try allocator.dupe(u8, str[colon + 1 .. str.len]),
        };
    }

    pub fn deinit(self: *const Transition, allocator: std.mem.Allocator) void {
        allocator.free(self.dest);
    }

    pub fn match(self: *const Transition, xmas: *const xMas) bool {
        const v = xmas.get(self.category);
        return switch (self.op) {
            .LessThan => v < self.value,
            .GreaterThan => v > self.value,
        };
    }
};

test "Transition.fromString" {
    const allocator = std.testing.allocator;

    {
        var t = try Transition.fromString(allocator, "a<2006:qkq");
        defer t.deinit(allocator);

        try std.testing.expectEqual(Category.A, t.category);
        try std.testing.expectEqual(CmpOperator.LessThan, t.op);
        try std.testing.expectEqual(@as(u64, 2006), t.value);
        try std.testing.expect(std.mem.eql(u8, "qkq", t.dest));
    }
    {
        try std.testing.expectError(
            AOCError.NotATransition,
            Transition.fromString(allocator, "A"),
        );
    }
}

const TransitionList = std.ArrayList(Transition);

const State = struct {
    name: []const u8,
    transitions: TransitionList,
    default: []const u8,

    pub fn fromLine(allocator: std.mem.Allocator, str: []const u8) !State {
        const bracket = std.mem.indexOfScalar(u8, str, '{').?;
        var s = State{
            .name = try allocator.dupe(u8, str[0..bracket]),
            .transitions = TransitionList.init(allocator),
            .default = undefined,
        };

        var ite = std.mem.tokenizeScalar(u8, str[bracket + 1 .. str.len], ',');
        while (ite.next()) |tok| {
            if (Transition.fromString(allocator, tok)) |t| {
                try s.transitions.append(t);
            } else |err| {
                switch (err) {
                    AOCError.NotATransition => s.default = try allocator.dupe(u8, tok[0 .. tok.len - 1]),
                    else => return err,
                }
            }
        }
        return s;
    }

    pub fn deinit(self: *const State, allocator: std.mem.Allocator) void {
        for (self.transitions.items) |t| {
            t.deinit(allocator);
        }
        self.transitions.deinit();
        allocator.free(self.name);
        allocator.free(self.default);
    }

    pub fn findNext(self: *const State, xmas: *const xMas) []const u8 {
        for (self.transitions.items) |t| {
            if (t.match(xmas)) {
                return t.dest;
            }
        }
        return self.default;
    }
};

const StateHash = std.StringHashMap(State);

test "State.fromLine" {
    const allocator = std.testing.allocator;

    {
        var s = try State.fromLine(allocator, "px{a<2006:qkq,m>2090:A,rfg}");
        defer s.deinit(allocator);

        try std.testing.expect(std.mem.eql(u8, "px", s.name));
        try std.testing.expect(std.mem.eql(u8, "rfg", s.default));
        try std.testing.expectEqual(@as(usize, 2), s.transitions.items.len);
    }
}

pub fn isAccepted(states: *const StateHash, xmas: *const xMas) bool {
    var current: []const u8 = "in";

    while (true) {
        const s = states.get(current).?;

        const c = s.findNext(xmas);
        std.debug.print("  [{s:4}] >> [{s:4}]\n", .{ current, c });
        current = c;
        if (std.mem.eql(u8, current, "A")) {
            return true;
        } else if (std.mem.eql(u8, current, "R")) {
            return false;
        }
    }
}

pub fn run(states: *const StateHash, xmass: *const xMasList) void {
    var total: u64 = 0;
    for (xmass.items) |x| {
        std.debug.print(">> {}\n", .{x});
        if (isAccepted(states, &x)) {
            total += x.total();
            std.debug.print("Accepted: {}\n", .{x});
        }
    }
    std.debug.print("Total: {}\n", .{total});
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var states = StateHash.init(allocator);
    var xmass = xMasList.init(allocator);

    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);

    while (true) {
        fbs.reset();
        try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);
        const line = fbs.getWritten();
        if (line.len == 0) break;
        const s = try State.fromLine(allocator, line);
        try states.put(s.name, s);
    }

    while (true) {
        fbs.reset();
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        const line = fbs.getWritten();
        try xmass.append(try xMas.fromLine(line[0..line.len]));
    }

    run(&states, &xmass);
}
