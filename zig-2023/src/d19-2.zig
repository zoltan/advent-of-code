const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
    NotATransition,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Category = enum {
    X,
    M,
    A,
    S,

    pub fn fromChar(c: u8) Category {
        return switch (c) {
            'x' => .X,
            'm' => .M,
            'a' => .A,
            's' => .S,
            else => unreachable,
        };
    }

    pub fn format(self: Category, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "{c}", .{@as(u8, switch (self) {
            .X => 'x',
            .M => 'm',
            .A => 'a',
            .S => 's',
        })});
    }
};

const CmpOperator = enum {
    LessThan,
    GreaterThan,

    pub fn fromChar(c: u8) CmpOperator {
        return switch (c) {
            '<' => .LessThan,
            '>' => .GreaterThan,
            else => unreachable,
        };
    }

    pub fn format(self: CmpOperator, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        const c: u8 = switch (self) {
            .LessThan => '<',
            .GreaterThan => '>',
        };
        try std.fmt.format(writer, "{c}", .{c});
    }
};

const Transition = struct {
    category: Category,
    op: CmpOperator,
    value: u64,
    dest: []const u8,

    pub fn fromString(allocator: std.mem.Allocator, str: []const u8) !Transition {
        const colon = std.mem.indexOfScalar(u8, str, ':') orelse {
            return AOCError.NotATransition;
        };
        return Transition{
            .category = Category.fromChar(str[0]),
            .op = CmpOperator.fromChar(str[1]),
            .value = try std.fmt.parseInt(u64, str[2..colon], 10),
            .dest = try allocator.dupe(u8, str[colon + 1 .. str.len]),
        };
    }

    pub fn deinit(self: *const Transition, allocator: std.mem.Allocator) void {
        allocator.free(self.dest);
    }

    pub fn format(self: *const Transition, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "Transition: dest {s} => {} {} {}", .{
            self.dest,
            self.category,
            self.op,
            self.value,
        });
    }
};

test "Transition.fromString" {
    const allocator = std.testing.allocator;

    {
        var t = try Transition.fromString(allocator, "a<2006:qkq");
        defer t.deinit(allocator);

        try std.testing.expectEqual(Category.A, t.category);
        try std.testing.expectEqual(CmpOperator.LessThan, t.op);
        try std.testing.expectEqual(@as(u64, 2006), t.value);
        try std.testing.expect(std.mem.eql(u8, "qkq", t.dest));
    }
    {
        try std.testing.expectError(
            AOCError.NotATransition,
            Transition.fromString(allocator, "A"),
        );
    }
}

const TransitionList = std.ArrayList(Transition);

const State = struct {
    name: []const u8,
    transitions: TransitionList,
    default: []const u8,

    pub fn fromLine(allocator: std.mem.Allocator, str: []const u8) !State {
        const bracket = std.mem.indexOfScalar(u8, str, '{').?;
        var s = State{
            .name = try allocator.dupe(u8, str[0..bracket]),
            .transitions = TransitionList.init(allocator),
            .default = undefined,
        };

        var ite = std.mem.tokenizeScalar(u8, str[bracket + 1 .. str.len], ',');
        while (ite.next()) |tok| {
            if (Transition.fromString(allocator, tok)) |t| {
                try s.transitions.append(t);
            } else |err| {
                switch (err) {
                    AOCError.NotATransition => s.default = try allocator.dupe(u8, tok[0 .. tok.len - 1]),
                    else => return err,
                }
            }
        }
        return s;
    }

    pub fn reduce(self: *State) bool {
        var changed = false;
        while (self.transitions.items.len > 0) {
            const lastDest = self.transitions.items[self.transitions.items.len - 1].dest;
            if (std.mem.eql(u8, lastDest, self.default)) {
                _ = self.transitions.pop();
                changed = true;
            } else {
                break;
            }
        }
        return changed;
    }

    pub fn deinit(self: *const State, allocator: std.mem.Allocator) void {
        for (self.transitions.items) |t| {
            t.deinit(allocator);
        }
        self.transitions.deinit();
        allocator.free(self.name);
        allocator.free(self.default);
    }
};

const StateHash = std.StringHashMap(State);

test "State.fromLine" {
    const allocator = std.testing.allocator;

    {
        var s = try State.fromLine(allocator, "px{a<2006:qkq,m>2090:A,rfg}");
        defer s.deinit(allocator);

        try std.testing.expect(std.mem.eql(u8, "px", s.name));
        try std.testing.expect(std.mem.eql(u8, "rfg", s.default));
        try std.testing.expectEqual(@as(usize, 2), s.transitions.items.len);
    }
}

fn minMaxTotal(m: [4][2]u64) u64 {
    var total: u64 = 1;
    for (m) |r| {
        total *= r[1] - r[0] + 1;
    }
    return total;
}

fn run(states: *const StateHash, name: []const u8, minMax: [4][2]u64) u64 {
    if (std.mem.eql(u8, name, "A")) {
        return minMaxTotal(minMax);
    } else if (std.mem.eql(u8, name, "R")) {
        return 0;
    }

    const s = states.get(name).?;
    var total: u64 = 0;

    var mmCum = minMax;
    for (s.transitions.items) |t| {
        var mm = mmCum;
        switch (t.op) {
            .LessThan => {
                const v = mm[@intFromEnum(t.category)][1];
                mm[@intFromEnum(t.category)][1] = @min(t.value - 1, v);

                const vCum = mmCum[@intFromEnum(t.category)][0];
                mmCum[@intFromEnum(t.category)][0] = @max(t.value, vCum);
            },
            .GreaterThan => {
                const v = mm[@intFromEnum(t.category)][0];
                mm[@intFromEnum(t.category)][0] = @max(t.value + 1, v);

                const vCum = mmCum[@intFromEnum(t.category)][1];
                mmCum[@intFromEnum(t.category)][1] = @min(t.value, vCum);
            },
        }
        total += run(states, t.dest, mm);
    }
    total += run(states, s.default, mmCum);
    return total;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();

    var states = StateHash.init(allocator);

    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);

    while (true) {
        fbs.reset();
        try reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len);
        const line = fbs.getWritten();
        if (line.len == 0) break;
        const s = try State.fromLine(allocator, line);
        try states.put(s.name, s);
    }

    var ite = states.valueIterator();
    while (ite.next()) |s| {
        for (s.transitions.items) |t| {
            std.debug.print("  {s} -->|{} {} {d}| {s}\n", .{ s.name, t.category, t.op, t.value, t.dest });
        }
        std.debug.print("  {s} -->|default| {s}\n", .{ s.name, s.default });
    }
    std.debug.print("--------------------------------------------------------------------------------\n", .{});

    const total = run(
        &states,
        "in",
        [4][2]u64{ [2]u64{ 1, 4000 }, [2]u64{ 1, 4000 }, [2]u64{ 1, 4000 }, [2]u64{ 1, 4000 } },
    );
    std.debug.print("Total: {}\n", .{total});
}
