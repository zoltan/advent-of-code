const std = @import("std");
const testing = std.testing;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const PulseValue = enum {
    Low,
    High,

    pub fn format(self: PulseValue, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "{s}", .{switch (self) {
            .Low => "low",
            .High => "high",
        }});
    }
};

const Broadcast = struct {
    pub fn sendPulse(_: *Broadcast, p: PulseValue, _: []const u8) ?PulseValue {
        return p;
    }
};
const Output = struct {
    pub fn sendPulse(_: *Output, p: PulseValue, _: []const u8) ?PulseValue {
        std.debug.print("Ouput: {}\n", .{p});
        return null;
    }
};

const FlipFlop = struct {
    state: bool = false,

    pub fn format(self: *const FlipFlop, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "{}", .{self.state});
    }

    pub fn sendPulse(self: *FlipFlop, p: PulseValue, _: []const u8) ?PulseValue {
        return switch (p) {
            .High => null,
            .Low => blk: {
                self.state = !self.state;
                break :blk if (self.state) .High else .Low;
            },
        };
    }

    pub fn reset(self: *FlipFlop) void {
        self.state = false;
    }
};

test "FlipFlop.sendPulse" {
    {
        var f = FlipFlop{};
        try std.testing.expectEqual(@as(?PulseValue, null), f.sendPulse(.High, ""));
        try std.testing.expect(!f.state);
    }

    {
        var f = FlipFlop{};
        try std.testing.expectEqual(@as(?PulseValue, .High), f.sendPulse(.Low, ""));
        try std.testing.expectEqual(@as(?PulseValue, .Low), f.sendPulse(.Low, ""));
        try std.testing.expectEqual(@as(?PulseValue, .High), f.sendPulse(.Low, ""));
        try std.testing.expectEqual(@as(?PulseValue, .Low), f.sendPulse(.Low, ""));
        try std.testing.expectEqual(@as(?PulseValue, null), f.sendPulse(.High, ""));
        try std.testing.expectEqual(@as(?PulseValue, .High), f.sendPulse(.Low, ""));
        try std.testing.expectEqual(@as(?PulseValue, .Low), f.sendPulse(.Low, ""));
    }
}

const Conjunction = struct {
    const Input = struct {
        src: []const u8,
        state: PulseValue = .Low,

        pub fn format(self: *const Input, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
            try std.fmt.format(writer, "{s:3}: {s}", .{ self.src, self.state });
        }
    };

    inputs: std.ArrayList(Input),

    pub fn init(allocator: std.mem.Allocator) Conjunction {
        return Conjunction{ .inputs = std.ArrayList(Input).init(allocator) };
    }

    pub fn deinit(self: *Conjunction) void {
        self.inputs.deinit();
    }

    pub fn addInput(self: *Conjunction, name: []const u8) !void {
        try self.inputs.append(Input{ .src = name });
    }

    pub fn format(self: *const Conjunction, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "{s}", .{self.inputs.items});
    }

    pub fn sendPulse(self: *Conjunction, p: PulseValue, from: []const u8) ?PulseValue {
        var ret: ?PulseValue = .Low;
        for (0..self.inputs.items.len) |i| {
            if (std.mem.eql(u8, from, self.inputs.items[i].src)) {
                self.inputs.items[i].state = p;
            }
            if (self.inputs.items[i].state == .Low) {
                ret = .High;
            }
        }
        return ret;
    }

    pub fn reset(self: *Conjunction) void {
        for (0..self.inputs.items.len) |i| {
            self.inputs.items[i].state = .Low;
        }
    }
};

const ModuleType = enum {
    Broadcast,
    Conjunction,
    FlipFlop,
    Output,
};

const Module = union(ModuleType) {
    Broadcast: Broadcast,
    Conjunction: Conjunction,
    FlipFlop: FlipFlop,
    Output: Output,
};

const Record = struct {
    module: *Module,
    targets: std.ArrayList([]const u8),

    pub fn init(allocator: std.mem.Allocator, mod: *Module) Record {
        return Record{
            .module = mod,
            .targets = std.ArrayList([]const u8).init(allocator),
        };
    }

    pub fn format(self: *const Record, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "{} -> {s}", .{ self.module, self.targets.items });
    }

    pub fn hasTarget(self: *const Record, name: []const u8) bool {
        for (self.targets.items) |t| {
            if (std.mem.eql(u8, name, t)) return true;
        }
        return false;
    }
};

const ModuleMap = std.StringHashMap(Record);

fn readInput(allocator: std.mem.Allocator, reader: std.fs.File.Reader, map: *ModuleMap) !void {
    var buffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buffer);

    while (true) {
        fbs.reset();
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break;
        };
        var line = fbs.getWritten();
        const sep = std.mem.indexOfScalar(u8, line, ' ').?;
        var name: []const u8 = undefined;
        if (line[0] == '%' or line[0] == '&') {
            name = try allocator.dupe(u8, line[1..sep]);
        } else {
            name = try allocator.dupe(u8, line[0..sep]);
        }

        const mod: *Module = try allocator.create(Module);
        if (std.mem.eql(u8, name, "broadcaster")) {
            mod.* = Module{ .Broadcast = Broadcast{} };
        } else if (std.mem.eql(u8, name, "output")) {
            mod.* = Module{ .Output = Output{} };
        } else if (line[0] == '%') {
            mod.* = Module{ .FlipFlop = FlipFlop{} };
        } else if (line[0] == '&') {
            mod.* = Module{ .Conjunction = Conjunction.init(allocator) };
        } else {
            unreachable;
        }

        var rec = Record.init(allocator, mod);
        var ite = std.mem.tokenizeSequence(u8, line[sep + 4 .. line.len], ", ");
        while (ite.next()) |tok| {
            try rec.targets.append(try allocator.dupe(u8, tok));
        }
        try map.put(name, rec);
    }
}

fn setConjunctions(map: *ModuleMap) !void {
    var ite = map.iterator();
    while (ite.next()) |entry| {
        switch (entry.value_ptr.*.module.*) {
            .Conjunction => |*c| {
                var kvIte = map.iterator();
                while (kvIte.next()) |kv| {
                    if (kv.value_ptr.*.hasTarget(entry.key_ptr.*)) {
                        try c.addInput(kv.key_ptr.*);
                    }
                }
            },
            else => {},
        }
    }
}

fn resetMap(map: *ModuleMap) void {
    var ite = map.iterator();
    while (ite.next()) |entry| {
        switch (entry.value_ptr.*.module.*) {
            .FlipFlop => |*f| f.reset(),
            .Conjunction => |*c| c.reset(),
            else => {},
        }
    }
}

const Pulse = struct {
    src: []const u8,
    dst: []const u8,
    p: PulseValue,

    pub fn format(self: *const Pulse, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        try std.fmt.format(writer, "{s} -{s}{}-> {s}", .{ self.src, if (self.p == .Low) "-" else "", self.p, self.dst });
    }
};

fn runCycle(allocator: std.mem.Allocator, map: *ModuleMap, stop: []const u8) !bool {
    var pulses = std.ArrayList(Pulse).init(allocator);
    defer pulses.deinit();

    try pulses.append(Pulse{
        .src = "button",
        .dst = "broadcaster",
        .p = .Low,
    });

    while (pulses.items.len > 0) {
        const p = pulses.orderedRemove(0);

        if (std.mem.eql(u8, p.dst, stop) and p.p == .Low) {
            std.debug.print("{}\n", .{p});
            return true;
        }

        if (map.get(p.dst)) |record| {
            const signal = switch (record.module.*) {
                .Output => |*m| m.sendPulse(p.p, p.src),
                .Broadcast => |*m| m.sendPulse(p.p, p.src),
                .FlipFlop => |*m| m.sendPulse(p.p, p.src),
                .Conjunction => |*m| m.sendPulse(p.p, p.src),
            };

            if (signal) |s| {
                for (record.targets.items) |t| {
                    try pulses.append(Pulse{
                        .src = p.dst,
                        .dst = t,
                        .p = s,
                    });
                }
            }
        }
    }
    return false;
}

fn lcm(a: u64, b: u64) u64 {
    const gcd = std.math.gcd(a, b);
    return (a / gcd) * b;
}

fn run(allocator: std.mem.Allocator, map: *ModuleMap, stops: [][]const u8) !void {
    var nums = std.ArrayList(usize).init(allocator);

    var ret: usize = 1;
    for (stops) |stop| {
        resetMap(map);
        var i: usize = 1;
        while (true) : (i += 1) {
            if (i % 100000 == 0) {
                std.debug.print("##### Cycle {}\n", .{i});
            }
            if (try runCycle(allocator, map, stop)) {
                try nums.append(i);
                ret = lcm(ret, i);
                break;
            }
        }
    }
    std.debug.print("Cycle {d} -> {d}\n", .{ nums.items, ret });
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const reader = try getReader();

    var map = ModuleMap.init(allocator);

    try readInput(allocator, reader, &map);
    try setConjunctions(&map);

    var ite = map.iterator();
    const parent = while (ite.next()) |e| {
        if (e.value_ptr.*.hasTarget("rx")) {
            std.debug.print("{s} => {}\n", .{ e.key_ptr.*, e.value_ptr.* });
            break e.key_ptr.*;
        }
    } else {
        unreachable;
    };
    std.debug.print("Parent: {s}\n", .{parent});
    var parents = std.ArrayList([]const u8).init(allocator);
    ite = map.iterator();
    while (ite.next()) |e| {
        if (e.value_ptr.*.hasTarget(parent)) {
            try parents.append(e.key_ptr.*);
        }
    }

    std.debug.print("Parents: {s}\n", .{parents.items});

    std.debug.print("--------------------------------------------------------------------------------\n", .{});

    try run(allocator, &map, parents.items);
}
