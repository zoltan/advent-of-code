const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn getPair(line: []const u8) struct {u64, u64} {
    var ite = std.mem.splitSequence(u8, line, "   ");
    const left = std.fmt.parseUnsigned(u64, ite.first(), 10) catch { unreachable; };
    const right = std.fmt.parseUnsigned(u64, ite.peek().?, 10) catch { unreachable; };
    return .{left, right};
}

test "getPair" {
    const pair = getPair("123   456");
    try expect(pair[0] == 123);
    try expect(pair[1] == 456);
}


fn abs(comptime T: type, left: T, right: T) T {
    return switch (left > right) {
        true => left - right,
        false => right - left,
    };
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();
    var buffer: [512:0]u8 = undefined;

    var fbs = std.io.fixedBufferStream(&buffer);
    var total: u64 = 0;
    var left = std.ArrayList(u64).init(allocator);
    var right = std.ArrayList(u64).init(allocator);

    loop: while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break :loop;
        };
        const line = fbs.getWritten();
        const pair = getPair(line);
        try left.append(pair[0]);
        try right.append(pair[1]);
        fbs.reset();
    }

    std.mem.sort(u64, left.items, {}, std.sort.asc(u64));
    std.mem.sort(u64, right.items, {}, std.sort.asc(u64));

    for (0..left.items.len) |i| {
        total += abs(u64, left.items[i], right.items[i]);
    }

    std.debug.print("Total: {}\n", .{total});
}
