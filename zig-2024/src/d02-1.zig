const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}


fn absDiff(comptime T: type, left: T, right: T) T {
    return switch (left > right) {
        true => left - right,
        false => right - left,
    };
}

const Report = struct {
    levels: std.ArrayList(u64),

    fn init(allocator: std.mem.Allocator, buffer: []const u8) !Report {
        var levels = std.ArrayList(u64).init(allocator);
        var ite = std.mem.splitScalar(u8, buffer, ' ');

        var num = try std.fmt.parseUnsigned(u64, ite.first(), 10);
        try levels.append(num);

        while (ite.next()) |s| {
            num = try std.fmt.parseUnsigned(u64, s, 10);
            try levels.append(num);
        }

        return .{.levels = levels};
    }

    fn deinit(self: *const Report) void {
        self.levels.deinit();
    }

    fn isSafe(self: *const Report) bool {
        if(self.levels.items[0] == self.levels.items[1]) {return false;}

        const isIncreasing = (self.levels.items[0] < self.levels.items[1]);
        for (1..self.levels.items.len) |i| {
            const cur = self.levels.items[i];
            const prev = self.levels.items[i - 1];
            if (cur == prev) {
                return false;
            } else if (isIncreasing and prev > cur) {
                return false;
            } else if (!isIncreasing and cur > prev) {
                return false;
            } else if (absDiff(u64, cur, prev) > 3) {
                return false;
            }
        }
        return true;
    }
};

test "report" {

    const cases = [_]struct { b: []const u8, r: bool }{
        .{ .b = "7 6 4 2 1", .r = true },
        .{ .b = "1 2 7 8 9", .r = false },
        .{ .b = "9 7 6 2 1", .r = false },
        .{ .b = "1 3 2 4 5", .r = false },
        .{ .b = "8 6 4 4 1", .r = false },
        .{ .b = "1 3 6 7 9", .r = true },
    };

    for (cases) |c| {
        const r = try Report.init(std.testing.allocator, c.b);
        try std.testing.expect(r.isSafe() == c.r);
        defer r.deinit();
    }

}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();
    var buffer: [512:0]u8 = undefined;

    var fbs = std.io.fixedBufferStream(&buffer);
    var total: u64 = 0;

    loop: while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break :loop;
        };
        const line = fbs.getWritten();
        const r = try Report.init(allocator, line);
        defer r.deinit();
        if (r.isSafe()) {
            total += 1;
        }

        fbs.reset();
    }

    std.debug.print("Total: {}\n", .{total});

}
