const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}


fn absDiff(comptime T: type, left: T, right: T) T {
    return switch (left > right) {
        true => left - right,
        false => right - left,
    };
}

const Report = struct {
    levels: std.ArrayList(u64),

    fn init(allocator: std.mem.Allocator, buffer: []const u8) !Report {
        var levels = std.ArrayList(u64).init(allocator);
        var ite = std.mem.splitScalar(u8, buffer, ' ');

        var num = try std.fmt.parseUnsigned(u64, ite.first(), 10);
        try levels.append(num);

        while (ite.next()) |s| {
            num = try std.fmt.parseUnsigned(u64, s, 10);
            try levels.append(num);
        }

        return .{.levels = levels};
    }

    fn deinit(self: *const Report) void {
        self.levels.deinit();
    }

    fn isSafe(self: *const Report, toleration: u8) !bool {
        var violations: u64 = 0;

        // std.debug.print("Checking >>> {d}\n", .{self.levels.items});

        if(self.levels.items[0] == self.levels.items[1]) { violations += 1; }

        const isIncreasing = (self.levels.items[0] < self.levels.items[1]);
        for (1..self.levels.items.len) |i| {
            const cur = self.levels.items[i];
            const prev = self.levels.items[i - 1];
            if (cur == prev) {
                violations +=1;
            } else if (isIncreasing and prev > cur) {
                violations +=1;
            } else if (!isIncreasing and cur > prev) {
                violations +=1;
            } else if (absDiff(u64, cur, prev) > 3) {
                violations +=1;
            }
        }

        if (violations == 0) {
            return true;
        }
        if (toleration == 0) {
            return false;
        }

        for (0..self.levels.items.len) |pos| {
            var subReport: Report = .{ .levels = try self.levels.clone()} ;
            _ = subReport.levels.orderedRemove(pos);
            defer subReport.deinit();

            if (try subReport.isSafe(toleration - 1)) {
                return true;
            }
        }

        return false;
    }
};

test "report" {

    const cases = [_]struct { b: []const u8, r: bool }{
        .{ .b = "7 6 4 2 1", .r = true },
        .{ .b = "1 2 7 8 9", .r = false },
        .{ .b = "9 7 6 2 1", .r = false },
        .{ .b = "1 3 2 4 5", .r = true },
        .{ .b = "8 6 4 4 1", .r = true },
        .{ .b = "1 3 6 7 9", .r = true },
    };

    for (cases) |c| {
        const r = try Report.init(std.testing.allocator, c.b);
        try std.testing.expect(try r.isSafe(1) == c.r);
        defer r.deinit();
    }

}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();
    var buffer: [512:0]u8 = undefined;

    var fbs = std.io.fixedBufferStream(&buffer);
    var total: u64 = 0;

    loop: while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break :loop;
        };
        const line = fbs.getWritten();
        const r = try Report.init(allocator, line);
        defer r.deinit();
        if (try r.isSafe(1)) {
            total += 1;
        }
        fbs.reset();
    }

    std.debug.print("Total: {}\n", .{total});

}
