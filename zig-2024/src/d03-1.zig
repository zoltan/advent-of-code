const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn getValue(s: []const u8) ?u64 {
    const comaPos = std.mem.indexOf(u8, s, ",") orelse return null;

    const left = std.fmt.parseUnsigned(u64, s[0..comaPos], 10) catch return null;
    if (left >= 1000) return null;

    const right = std.fmt.parseUnsigned(u64, s[comaPos + 1 .. s.len], 10) catch return null;
    if (right >= 1000) return null;

    return left * right;
}

test "mul()" {
    try std.testing.expectEqual(6, getValue("2,3").?);
    try std.testing.expectEqual(88, getValue("11,8").?);
    try std.testing.expectEqual(369, getValue("123,3").?);
    try std.testing.expectEqual(369, getValue("3,123").?);
    try std.testing.expect(getValue("3123)") == null);
    try std.testing.expect(getValue("12,3123") == null);
    try std.testing.expect(getValue("[32,64]then(mul(11,8] 19") == null);
    try std.testing.expect(getValue("32,64]then(mul(11,8") == null);
}

test "step_1" {
    try std.testing.expectEqual(
        128,
        processLine("mul(11,8)mul(8,5)"),
    );
    try std.testing.expectEqual(
        128,
        processLine("mul(32,64]then(mul(11,8)mul(8,5))"),
    );

    try std.testing.expectEqual(
        161,
        processLine("xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))"),
    );
}

fn processLine(line: []const u8) u64 {
    var total: u64 = 0;
    var base: usize = 0;

    while (std.mem.indexOfPos(u8, line, base, "mul(")) |pos| {
        if (std.mem.indexOfPos(u8, line, pos + 4, ")")) |paren| {
            const s = line[pos + 4 ..  paren];
            if (getValue(s)) |val| {
                total += val;
                base = paren;
            } else base = pos + 4;
        } else {
            base = pos;
        }
    }
    return total;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    // const allocator = arena.allocator();

    var reader = try getReader();
    var buffer: [4096:0]u8 = undefined;

    var fbs = std.io.fixedBufferStream(&buffer);
    var total: u64 = 0;

    loop: while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break :loop;
        };
        const line = fbs.getWritten();
        total += processLine(line);

        fbs.reset();
    }

    std.debug.print("Total: {d}\n", .{total});
}
