const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn getValue(s: []const u8) ?u64 {
    const comaPos = std.mem.indexOf(u8, s, ",") orelse return null;

    const left = std.fmt.parseUnsigned(u64, s[0..comaPos], 10) catch return null;
    if (left >= 1000) return null;

    const right = std.fmt.parseUnsigned(u64, s[comaPos + 1 .. s.len], 10) catch return null;
    if (right >= 1000) return null;

    return left * right;
}

test "mul()" {
    try std.testing.expectEqual(6, getValue("2,3").?);
    try std.testing.expectEqual(88, getValue("11,8").?);
    try std.testing.expectEqual(369, getValue("123,3").?);
    try std.testing.expectEqual(369, getValue("3,123").?);
    try std.testing.expect(getValue("3123)") == null);
    try std.testing.expect(getValue("12,3123") == null);
    try std.testing.expect(getValue("[32,64]then(mul(11,8] 19") == null);
    try std.testing.expect(getValue("32,64]then(mul(11,8") == null);
}

test "step_1" {
    var active = true;
    try std.testing.expectEqual(
        128,
        processLine("mul(11,8)mul(8,5)", &active),
    );
    try std.testing.expectEqual(
        128,
        processLine("mul(32,64]then(mul(11,8)mul(8,5))", &active),
    );

    try std.testing.expectEqual(
        48,
        processLine("xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))", &active),
    );
}

fn processLine(line: []const u8, active: *bool) u64 {
    var total: u64 = 0;
    var base: usize = 0;

    while (true) {

        const mulPos = std.mem.indexOfPos(u8, line, base, "mul(") orelse break;
        const doPos = std.mem.indexOfPos(u8, line, base, "do()");
        const dontPos = std.mem.indexOfPos(u8, line, base, "don't()");

        if (doPos != null and doPos.? < mulPos) {
            active.* = true;
            base = doPos.? + 4;
        } else if (dontPos != null and dontPos.? < mulPos) {
            active.* = false;
            base = dontPos.? + 6;
        } else {
            if (std.mem.indexOfPos(u8, line, mulPos + 4, ")")) |paren| {
                const s = line[mulPos + 4 ..  paren];
                if (getValue(s)) |val| {
                    if (active.*)
                        total += val;
                    base = paren;
                } else base = mulPos + 4;
            } else {
                base = mulPos;
            }
        }

    }

    return total;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    // const allocator = arena.allocator();

    var reader = try getReader();
    var buffer: [4096:0]u8 = undefined;

    var fbs = std.io.fixedBufferStream(&buffer);
    var total: u64 = 0;
    var active = true;

    loop: while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break :loop;
        };
        const line = fbs.getWritten();
        total += processLine(line, &active);

        fbs.reset();
    }

    std.debug.print("Total: {d}\n", .{total});
}
