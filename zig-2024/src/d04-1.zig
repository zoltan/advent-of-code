const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn readMap(allocator: std.mem.Allocator, reader: std.fs.File.Reader) ![][]u8 {
    var readBuffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&readBuffer);

    try reader.streamUntilDelimiter(fbs.writer(), '\n', readBuffer.len);
    const line = fbs.getWritten();
    var map = try allocator.alloc([]u8, line.len);
    map[0] = try allocator.dupe(u8, line);
    for (1..map.len) |i| {
        fbs.reset();
        try reader.streamUntilDelimiter(fbs.writer(), '\n', readBuffer.len);
        map[i] = try allocator.dupe(u8, fbs.getWritten());
    }
    return map;
}

const Point = struct {
    x: u64,
    y: u64,
};

const Direction = enum {
    Up,
    Down,
    Left,
    Right,
    DiagonalUpLeft,
    DiagonalUpRight,
    DiagonalDownLeft,
    DiagonalDownRight,
};

const Iterator = struct {
    start: Point,
    width: u64,
    height: u64,
    direction: Direction,
    _current: Point,

    pub fn create(start: Point, width: u64, height: u64, direction: Direction) Iterator {
        return .{
            .start = start,
            .width = width,
            .height = height,
            .direction = direction,
            ._current = start,
        };
    }

    pub fn next(self: *Iterator) ?Point {
        switch (self.direction) {
            Direction.Up => {
                if (self._current.y > 0) {
                    self._current.y -= 1;
                    return self._current;
                } else {
                    return null;
                }
            },
            Direction.Down => {
                if (self._current.y + 1 < self.height) {
                    self._current.y += 1;
                    return self._current;
                } else {
                    return null;
                }
            },
            Direction.Left => {
                if (self._current.x > 0) {
                    self._current.x -= 1;
                    return self._current;
                } else {
                    return null;
                }
            },
            Direction.Right => {
                if (self._current.x + 1 < self.width) {
                    self._current.x += 1;
                    return self._current;
                } else {
                    return null;
                }
            },
            Direction.DiagonalUpLeft => {
                if (self._current.y > 0 and self._current.x > 0) {
                    self._current.y -= 1;
                    self._current.x -= 1;
                    return self._current;
                } else {
                    return null;
                }
            },
            Direction.DiagonalUpRight => {
                if (self._current.y > 0 and self._current.x + 1 < self.width) {
                    self._current.y -= 1;
                    self._current.x += 1;
                    return self._current;
                } else {
                    return null;
                }
            },
            Direction.DiagonalDownLeft => {
                if (self._current.y + 1 < self.height and self._current.x > 0) {
                    self._current.y += 1;
                    self._current.x -= 1;
                    return self._current;
                } else {
                    return null;
                }
            },
            Direction.DiagonalDownRight => {
                if (self._current.y + 1 < self.height and self._current.x + 1 < self.width) {
                    self._current.y += 1;
                    self._current.x += 1;
                    return self._current;
                } else {
                    return null;
                }
            },
        }
    }
};

const dirs = [_]Direction{
    Direction.Up,
    Direction.Down,
    Direction.Left,
    Direction.Right,
    Direction.DiagonalUpLeft,
    Direction.DiagonalUpRight,
    Direction.DiagonalDownLeft,
    Direction.DiagonalDownRight,
};

fn search(map: [][]u8, width: usize, height: usize, start: Point) usize {
    const expected = "MAS";
    var ret: usize = 0;

    blk: for (dirs) |d| {
        var remaining: []const u8 = expected[0..];
        var ite = Iterator.create(start, width, height, d);
        while (ite.next()) |c| {
            if (map[c.y][c.x] == remaining[0]) {
                remaining = remaining[1..];
                if (remaining.len == 0) {
                    ret += 1;
                    continue :blk;
                }
            } else continue :blk;
        }
    }
    return ret;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const reader = try getReader();
    const map = try readMap(arena.allocator(), reader);
    const width = map[0].len;
    const height = map.len;
    var total: usize = 0;
    for (0..height) |i| {
        var offset: usize = 0;
        while (std.mem.indexOfScalarPos(u8, map[i], offset, 'X')) |pos| {
            total += search(map, width, height, .{ .x = pos, .y = i });
            offset = pos + 1;
        }
    }
    std.debug.print("total: {}\n", .{total});

}
