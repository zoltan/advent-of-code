const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

fn readMap(allocator: std.mem.Allocator, reader: std.fs.File.Reader) ![][]u8 {
    var readBuffer: [512:0]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&readBuffer);

    try reader.streamUntilDelimiter(fbs.writer(), '\n', readBuffer.len);
    const line = fbs.getWritten();
    var map = try allocator.alloc([]u8, line.len);
    map[0] = try allocator.dupe(u8, line);
    for (1..map.len) |i| {
        fbs.reset();
        try reader.streamUntilDelimiter(fbs.writer(), '\n', readBuffer.len);
        map[i] = try allocator.dupe(u8, fbs.getWritten());
    }
    return map;
}

fn isCross(map: []const []const u8, x: u64, y: u64) bool {
    return (
        (
            (map[y-1][x-1] == 'M' and map[y+1][x+1] == 'S')
             or (map[y-1][x-1] == 'S' and map[y+1][x+1] == 'M')
        )
        and (
            (map[y-1][x+1] == 'M' and map[y+1][x-1] == 'S')
            or (map[y-1][x+1] == 'S' and map[y+1][x-1] == 'M')
        )
    );
}

test "isCross" {
    const map = [_][]const u8 {
        ".M.S.",
        "..A..",
        ".M.S."
    };
    try std.testing.expect(isCross(&map, 2, 1));
}

fn foo(values: []const u8) usize {
    var sum: usize = 0;
    for (values) |v| sum += v;
    return sum;
}

test "slices" {
    const array = [_]u8{ 1, 2, 3, 4, 5 };
    const slice = array[0..];
    try expect(foo(slice) == 15);
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const reader = try getReader();
    const map = try readMap(arena.allocator(), reader);
    const width = map[0].len - 1;
    const height = map.len;
    var total: u64 = 0;
    for (1..height - 1) |i| {
        var offset: usize = 1;
        while (std.mem.indexOfScalarPos(u8, map[i], offset, 'A')) |pos| {
            if (pos == width)
                break;
            if (isCross(map, pos, i)) {
                total += 1;
                std.debug.print("✅ A at ({d}, {d})\n", .{pos, i});
            } else {
                std.debug.print("❌ A at ({d}, {d})\n", .{pos, i});
            }
            offset = pos + 1;
        }
    }

    std.debug.print("total: {}\n", .{total});
}
