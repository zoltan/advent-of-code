const std = @import("std");
const expect = std.testing.expect;

const AOCError = error{
    MissingInfileArg,
};

fn getReader() !std.fs.File.Reader {
    var args = std.process.args();
    _ = args.skip();
    const infile_path = args.next() orelse return AOCError.MissingInfileArg;

    std.debug.print("Reading input from {s}\n", .{infile_path});

    var dir = std.fs.cwd();
    var infile = try dir.openFile(infile_path, .{ .mode = .read_only });
    return infile.reader();
}

const Pair = struct {
    left: u8,
    right: u8,
};

fn updateIsValid(update: []const u8, pairs: []const Pair) bool {
    for (pairs) |pair| {

        const lPos = std.mem.indexOfScalar(u8, update , pair.left) orelse continue;
        const rPos = std.mem.indexOfScalar(u8, update , pair.right) orelse continue;

        if (lPos > rPos) {
            std.debug.print("  ⚠️ {d}|{d}\n", .{pair.left, pair.right});
            return false;
        }
    }
    return true;
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var reader = try getReader();
    var buffer: [512:0]u8 = undefined;

    var fbs = std.io.fixedBufferStream(&buffer);
    var total: u64 = 0;
    var pairs = std.ArrayList(Pair).init(allocator);

    loop: while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break :loop;
        };
        const line = fbs.getWritten();
        if (line.len == 0) {
            break :loop;
        }
        try pairs.append(.{
            .left = try std.fmt.parseUnsigned(u8, line[0..2], 10),
            .right = try std.fmt.parseUnsigned(u8, line[3..5], 10),
        });
        fbs.reset();
    }
    loop: while (true) {
        reader.streamUntilDelimiter(fbs.writer(), '\n', buffer.len) catch {
            break :loop;
        };
        const line = fbs.getWritten();

        var update = std.ArrayList(u8).init(allocator);

        var ite = std.mem.tokenizeScalar(u8, line, ',');
        while (ite.next()) |s| {
            const num = try std.fmt.parseUnsigned(u8, s, 10);
            try update.append(num);
        }
        if (updateIsValid(update.items , pairs.items )) {
            const mid = update.items[update.items.len / 2];
            std.debug.print("✅ {d} (mid: {d})\n", .{update.items, mid});
            total += mid;
        } else {
            std.debug.print("❌ {d}\n", .{update.items});
        }
        fbs.reset();
    }
    std.debug.print("Total: {}\n", .{total});
}
